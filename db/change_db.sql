/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100414
 Source Host           : localhost:3306
 Source Schema         : english

 Target Server Type    : MySQL
 Target Server Version : 100414
 File Encoding         : 65001

 Date: 12/05/2021 10:56:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for english_phrase
-- ----------------------------
DROP TABLE IF EXISTS `english_phrase`;
CREATE TABLE `english_phrase`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 265 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of english_phrase
-- ----------------------------
INSERT INTO `english_phrase` VALUES (1, '2021-03-30 05:38:14.000000', 'admin', NULL, NULL, 'have lunch', NULL);
INSERT INTO `english_phrase` VALUES (2, NULL, 'admin', '2021-03-31 08:29:17.000000', NULL, 'have a party', NULL);
INSERT INTO `english_phrase` VALUES (3, '2021-03-30 05:59:50.000000', 'admin', NULL, NULL, 'have a lesson', NULL);
INSERT INTO `english_phrase` VALUES (4, NULL, 'admin', '2021-03-30 06:01:42.000000', NULL, 'have a cup of tea', NULL);
INSERT INTO `english_phrase` VALUES (5, '2021-03-30 06:04:00.000000', 'admin', NULL, NULL, 'have a cup of coffee', NULL);
INSERT INTO `english_phrase` VALUES (6, NULL, 'admin', '2021-04-06 04:08:51.000000', NULL, 'have a shower', '');
INSERT INTO `english_phrase` VALUES (7, '2021-03-30 06:11:51.000000', 'admin', NULL, NULL, 'have dinner', NULL);
INSERT INTO `english_phrase` VALUES (8, '2021-03-30 06:15:15.000000', 'admin', NULL, NULL, 'have breakfast', NULL);
INSERT INTO `english_phrase` VALUES (9, '2021-03-30 06:25:02.000000', 'admin', NULL, NULL, 'have a meal', NULL);
INSERT INTO `english_phrase` VALUES (10, '2021-03-30 06:29:51.000000', 'admin', NULL, NULL, 'have something to eat', NULL);
INSERT INTO `english_phrase` VALUES (11, '2021-03-31 02:20:22.000000', 'admin', NULL, NULL, 'have a competition', '');
INSERT INTO `english_phrase` VALUES (12, '2021-03-31 02:33:28.000000', 'admin', NULL, NULL, 'have a look', '');
INSERT INTO `english_phrase` VALUES (13, NULL, 'admin', '2021-03-31 02:38:12.000000', NULL, 'have a go', '');
INSERT INTO `english_phrase` VALUES (14, '2021-03-31 02:48:35.000000', 'admin', NULL, NULL, 'have a good journey', 'Hành trình đi từ nơi này tới nơi khác, đặc biệt là một chuyến hành trình đi xa, chưa biết khi nào quay trở về');
INSERT INTO `english_phrase` VALUES (15, NULL, 'admin', '2021-04-06 04:09:29.000000', NULL, 'have a moment', '');
INSERT INTO `english_phrase` VALUES (16, '2021-03-31 04:15:52.000000', 'admin', NULL, NULL, 'have a good time', '');
INSERT INTO `english_phrase` VALUES (17, '2021-03-31 05:03:45.000000', 'admin', NULL, NULL, 'have my hair cut', '');
INSERT INTO `english_phrase` VALUES (18, '2021-03-31 05:06:30.000000', 'admin', NULL, NULL, 'don’t have the time', '');
INSERT INTO `english_phrase` VALUES (19, '2021-03-31 05:11:23.000000', 'admin', NULL, NULL, 'have got', 'Dùng trong văn nói - không thân mật, không trang trọng');
INSERT INTO `english_phrase` VALUES (20, NULL, 'admin', '2021-04-01 03:48:10.000000', NULL, '\'ve got', 'Dùng trong văn nói - không thân mật, không trang trọng.\nViết tắt của have got.');
INSERT INTO `english_phrase` VALUES (21, '2021-03-31 05:14:38.000000', 'admin', NULL, NULL, '\'s got', '');
INSERT INTO `english_phrase` VALUES (22, '2021-03-31 05:16:03.000000', 'admin', NULL, NULL, 'have to', 'Có nghĩa bạn phải làm gì đó');
INSERT INTO `english_phrase` VALUES (23, '2021-03-31 05:21:11.000000', 'admin', NULL, NULL, 'have a meeting', '');
INSERT INTO `english_phrase` VALUES (24, '2021-03-31 05:30:59.000000', 'admin', NULL, NULL, 'have an appointment', '');
INSERT INTO `english_phrase` VALUES (25, '2021-03-31 08:12:41.000000', 'admin', NULL, NULL, 'have a drink', '');
INSERT INTO `english_phrase` VALUES (26, '2021-03-31 08:14:17.000000', 'admin', NULL, NULL, 'have a swim', '');
INSERT INTO `english_phrase` VALUES (27, NULL, 'admin', '2021-03-31 08:19:12.000000', NULL, 'have a bath', 'Tắm bồn');
INSERT INTO `english_phrase` VALUES (28, '2021-03-31 08:23:45.000000', 'admin', NULL, NULL, 'have a game of chess', '');
INSERT INTO `english_phrase` VALUES (29, '2021-03-31 08:24:05.000000', 'admin', NULL, NULL, 'have a game of football', '');
INSERT INTO `english_phrase` VALUES (30, '2021-03-31 08:25:36.000000', 'admin', NULL, NULL, 'have a game of cards', '');
INSERT INTO `english_phrase` VALUES (31, '2021-03-31 08:32:18.000000', 'admin', NULL, NULL, 'have a wash', '');
INSERT INTO `english_phrase` VALUES (32, NULL, 'admin', '2021-04-01 02:16:38.000000', NULL, 'went in', '');
INSERT INTO `english_phrase` VALUES (33, NULL, 'admin', '2021-04-01 02:20:05.000000', NULL, 'went into', '');
INSERT INTO `english_phrase` VALUES (34, '2021-04-01 02:13:54.000000', 'admin', NULL, NULL, 'went up', '');
INSERT INTO `english_phrase` VALUES (35, '2021-04-01 02:14:50.000000', 'admin', NULL, NULL, 'go up', '');
INSERT INTO `english_phrase` VALUES (36, '2021-04-01 02:17:18.000000', 'admin', NULL, NULL, 'go in', '');
INSERT INTO `english_phrase` VALUES (37, NULL, 'admin', '2021-04-01 02:20:13.000000', NULL, 'go into', '');
INSERT INTO `english_phrase` VALUES (38, '2021-04-01 02:27:22.000000', 'admin', NULL, NULL, 'go away', '');
INSERT INTO `english_phrase` VALUES (39, '2021-04-01 02:30:40.000000', 'admin', NULL, NULL, 'went away', '');
INSERT INTO `english_phrase` VALUES (40, '2021-04-01 07:33:58.000000', 'admin', NULL, NULL, 'get-together', '');
INSERT INTO `english_phrase` VALUES (41, '2021-04-01 07:36:11.000000', 'admin', NULL, NULL, 'get together with friends', '');
INSERT INTO `english_phrase` VALUES (42, NULL, 'admin', '2021-04-01 10:00:14.000000', NULL, 'get on with', '');
INSERT INTO `english_phrase` VALUES (43, '2021-04-01 09:56:53.000000', 'admin', NULL, NULL, 'get on with the lesson', '');
INSERT INTO `english_phrase` VALUES (44, '2021-04-01 10:02:56.000000', 'admin', NULL, NULL, 'get on with this video', '');
INSERT INTO `english_phrase` VALUES (45, NULL, 'admin', '2021-04-01 10:34:39.000000', NULL, 'get on with things', '');
INSERT INTO `english_phrase` VALUES (46, NULL, 'admin', '2021-04-01 10:35:46.000000', NULL, 'get on with it', '');
INSERT INTO `english_phrase` VALUES (47, '2021-04-01 10:52:32.000000', 'admin', NULL, NULL, 'get behind with', '');
INSERT INTO `english_phrase` VALUES (48, '2021-04-01 13:53:04.000000', 'admin', NULL, NULL, 'look up the answer', '');
INSERT INTO `english_phrase` VALUES (49, NULL, 'admin', '2021-04-01 14:32:39.000000', NULL, 'look up', 'Things are looking up (cải thiện)');
INSERT INTO `english_phrase` VALUES (50, '2021-04-01 14:04:42.000000', 'admin', NULL, NULL, 'get through the ice', '');
INSERT INTO `english_phrase` VALUES (51, '2021-04-01 14:07:48.000000', 'admin', NULL, NULL, 'get through the door', '');
INSERT INTO `english_phrase` VALUES (52, '2021-04-01 14:14:24.000000', 'admin', NULL, NULL, 'get through the challenge', '');
INSERT INTO `english_phrase` VALUES (53, NULL, 'admin', '2021-04-01 14:22:57.000000', NULL, 'get through', 'Kết nối điện thoại');
INSERT INTO `english_phrase` VALUES (54, '2021-04-01 14:23:50.000000', 'admin', NULL, NULL, 'get through a horrible situation', '');
INSERT INTO `english_phrase` VALUES (55, '2021-04-01 14:32:58.000000', 'admin', NULL, NULL, 'look sb up', '');
INSERT INTO `english_phrase` VALUES (56, '2021-04-01 14:34:14.000000', 'admin', NULL, NULL, 'eat out', '');
INSERT INTO `english_phrase` VALUES (57, '2021-04-01 14:41:26.000000', 'admin', NULL, NULL, 'bring sth back', '');
INSERT INTO `english_phrase` VALUES (58, NULL, 'admin', '2021-04-01 14:43:08.000000', NULL, 'bring back results', '');
INSERT INTO `english_phrase` VALUES (59, '2021-04-01 14:43:24.000000', 'admin', NULL, NULL, 'bring back some fairness', '');
INSERT INTO `english_phrase` VALUES (60, NULL, 'admin', '2021-04-01 14:45:08.000000', NULL, 'ask sb out', 'He asked me out at a party ');
INSERT INTO `english_phrase` VALUES (61, '2021-04-01 15:01:46.000000', 'admin', NULL, NULL, 'look after myself', '');
INSERT INTO `english_phrase` VALUES (62, '2021-04-01 15:02:52.000000', 'admin', NULL, NULL, 'look after', '');
INSERT INTO `english_phrase` VALUES (63, '2021-04-01 15:04:24.000000', 'admin', NULL, NULL, 'ring sb back', 'ring you back');
INSERT INTO `english_phrase` VALUES (64, '2021-04-01 15:14:26.000000', 'admin', NULL, NULL, 'drop off the package', '');
INSERT INTO `english_phrase` VALUES (65, '2021-04-01 15:15:20.000000', 'admin', NULL, NULL, 'drop the package off', '');
INSERT INTO `english_phrase` VALUES (66, '2021-04-01 15:58:13.000000', 'admin', NULL, NULL, 'dig sth out', '');
INSERT INTO `english_phrase` VALUES (67, '2021-04-01 16:01:27.000000', 'admin', NULL, NULL, 'dig out shelters', '');
INSERT INTO `english_phrase` VALUES (68, '2021-04-01 16:04:16.000000', 'admin', NULL, NULL, 'dig out that information', '');
INSERT INTO `english_phrase` VALUES (69, '2021-04-01 16:05:51.000000', 'admin', NULL, NULL, 'dig out of a crisis', '');
INSERT INTO `english_phrase` VALUES (70, '2021-04-01 16:08:03.000000', 'admin', NULL, NULL, 'dig out of these circumstances', '');
INSERT INTO `english_phrase` VALUES (71, NULL, 'admin', '2021-04-01 16:11:16.000000', NULL, 'dig out of the ground', '');
INSERT INTO `english_phrase` VALUES (72, '2021-04-01 16:09:12.000000', 'admin', NULL, NULL, 'dig out of pocket', '');
INSERT INTO `english_phrase` VALUES (73, '2021-04-01 16:10:20.000000', 'admin', NULL, NULL, 'dig out of this', '');
INSERT INTO `english_phrase` VALUES (74, NULL, 'admin', '2021-04-01 16:18:03.000000', NULL, 'dig out of', '');
INSERT INTO `english_phrase` VALUES (75, '2021-04-01 16:14:07.000000', 'admin', NULL, NULL, 'dig out of the financial crisis', '');
INSERT INTO `english_phrase` VALUES (76, NULL, 'admin', '2021-04-02 04:12:28.000000', NULL, 'heard about', '');
INSERT INTO `english_phrase` VALUES (77, '2021-04-02 04:09:15.000000', 'admin', NULL, NULL, 'recycle', '');
INSERT INTO `english_phrase` VALUES (78, '2021-04-02 04:15:59.000000', 'admin', NULL, NULL, 'on the news', '');
INSERT INTO `english_phrase` VALUES (79, '2021-04-02 04:16:39.000000', 'admin', NULL, NULL, 'last night', '');
INSERT INTO `english_phrase` VALUES (80, '2021-04-02 04:22:20.000000', 'admin', NULL, NULL, 'keep the environment clean', '');
INSERT INTO `english_phrase` VALUES (81, '2021-04-02 04:22:54.000000', 'admin', NULL, NULL, 'keep sth clean', '');
INSERT INTO `english_phrase` VALUES (82, '2021-04-02 04:24:14.000000', 'admin', NULL, NULL, 'keep the streets and public places clean', '');
INSERT INTO `english_phrase` VALUES (83, '2021-04-02 04:28:37.000000', 'admin', NULL, NULL, 'recycling paper', '');
INSERT INTO `english_phrase` VALUES (84, '2021-04-02 04:32:13.000000', 'admin', NULL, NULL, 'instead of', '');
INSERT INTO `english_phrase` VALUES (85, '2021-04-02 08:11:43.000000', 'admin', NULL, NULL, 'throw sth away', '');
INSERT INTO `english_phrase` VALUES (86, '2021-04-02 10:27:14.000000', 'admin', NULL, NULL, 'look back on your life', '');
INSERT INTO `english_phrase` VALUES (87, '2021-04-02 10:27:23.000000', 'admin', NULL, NULL, 'look back', '');
INSERT INTO `english_phrase` VALUES (88, NULL, 'admin', '2021-04-08 08:08:00.000000', NULL, 'look back on', 'Nghĩa trừu tượng là nhìn lại những gì trong quá khứ,...');
INSERT INTO `english_phrase` VALUES (89, '2021-04-02 10:30:36.000000', 'admin', NULL, NULL, 'look back on my day', '');
INSERT INTO `english_phrase` VALUES (90, '2021-04-02 10:33:22.000000', 'admin', NULL, NULL, 'look back on history', '');
INSERT INTO `english_phrase` VALUES (91, NULL, 'admin', '2021-04-06 04:11:50.000000', NULL, 'look back at', 'Nghĩa đen là nhìn lại');
INSERT INTO `english_phrase` VALUES (92, NULL, 'admin', '2021-04-09 04:23:54.000000', NULL, 'get on', 'Nghĩa đen là lên.\nNghĩa trừu tượng là thân với');
INSERT INTO `english_phrase` VALUES (93, '2021-04-02 10:42:15.000000', 'admin', NULL, NULL, 'get on the bus', '');
INSERT INTO `english_phrase` VALUES (94, '2021-04-02 10:46:58.000000', 'admin', NULL, NULL, 'gave in', 'Nghĩa trừu tượng');
INSERT INTO `english_phrase` VALUES (95, NULL, 'admin', '2021-04-02 10:54:12.000000', NULL, 'came round', '');
INSERT INTO `english_phrase` VALUES (96, '2021-04-02 10:54:29.000000', 'admin', NULL, NULL, 'come round', '');
INSERT INTO `english_phrase` VALUES (97, '2021-04-02 10:57:23.000000', 'admin', NULL, NULL, 'come round to', '');
INSERT INTO `english_phrase` VALUES (98, '2021-04-07 03:01:38.000000', 'admin', NULL, NULL, 'turn up the radio', '');
INSERT INTO `english_phrase` VALUES (99, '2021-04-07 03:02:07.000000', 'admin', NULL, NULL, 'turn up the sound', '');
INSERT INTO `english_phrase` VALUES (100, '2021-04-07 03:03:15.000000', 'admin', NULL, NULL, 'turn up its sensitivity', '');
INSERT INTO `english_phrase` VALUES (101, NULL, 'admin', '2021-04-07 03:05:20.000000', NULL, 'turn up at', '');
INSERT INTO `english_phrase` VALUES (102, NULL, 'admin', '2021-04-07 03:05:12.000000', NULL, 'turned up at', '');
INSERT INTO `english_phrase` VALUES (103, '2021-04-07 03:06:33.000000', 'admin', NULL, NULL, 'turn up in', '');
INSERT INTO `english_phrase` VALUES (104, '2021-04-07 03:28:36.000000', 'admin', NULL, NULL, 'sent off', '');
INSERT INTO `english_phrase` VALUES (105, NULL, 'admin', '2021-04-08 07:57:38.000000', NULL, 'come across', '');
INSERT INTO `english_phrase` VALUES (106, '2021-04-08 07:14:43.000000', 'admin', NULL, NULL, 'take down the title', '');
INSERT INTO `english_phrase` VALUES (107, NULL, 'admin', '2021-04-08 07:20:05.000000', NULL, 'ask sb around', '');
INSERT INTO `english_phrase` VALUES (108, '2021-04-08 07:19:12.000000', 'admin', NULL, NULL, 'asked some friends around', '');
INSERT INTO `english_phrase` VALUES (109, '2021-04-08 07:22:11.000000', 'admin', NULL, NULL, 'ask around', '');
INSERT INTO `english_phrase` VALUES (110, '2021-04-08 07:26:02.000000', 'admin', NULL, NULL, 'play up your eyes', '');
INSERT INTO `english_phrase` VALUES (111, '2021-04-08 07:27:45.000000', 'admin', NULL, NULL, 'play up the good bits', '');
INSERT INTO `english_phrase` VALUES (112, '2021-04-08 07:29:43.000000', 'admin', NULL, NULL, 'play up on this concept', '');
INSERT INTO `english_phrase` VALUES (113, NULL, 'admin', '2021-04-08 08:02:17.000000', NULL, 'play up', '');
INSERT INTO `english_phrase` VALUES (114, '2021-04-08 07:39:39.000000', 'admin', NULL, NULL, 'break down this wall', '');
INSERT INTO `english_phrase` VALUES (115, '2021-04-08 07:41:17.000000', 'admin', NULL, NULL, 'break down this formula', '');
INSERT INTO `english_phrase` VALUES (116, NULL, 'admin', '2021-04-12 08:32:52.000000', NULL, 'break down', '');
INSERT INTO `english_phrase` VALUES (117, NULL, 'admin', '2021-04-08 08:03:45.000000', NULL, 'bring up issues', '');
INSERT INTO `english_phrase` VALUES (118, NULL, 'admin', '2021-04-08 08:03:36.000000', NULL, 'bring up', '');
INSERT INTO `english_phrase` VALUES (119, '2021-04-08 07:45:55.000000', 'admin', NULL, NULL, 'brought up', '');
INSERT INTO `english_phrase` VALUES (120, '2021-04-08 07:47:13.000000', 'admin', NULL, NULL, 'sort out', '');
INSERT INTO `english_phrase` VALUES (121, '2021-04-08 07:49:41.000000', 'admin', NULL, NULL, 'mess sb about', '');
INSERT INTO `english_phrase` VALUES (122, '2021-04-08 07:50:02.000000', 'admin', NULL, NULL, 'mess about', '');
INSERT INTO `english_phrase` VALUES (123, '2021-04-08 07:52:31.000000', 'admin', NULL, NULL, 'call it off', '');
INSERT INTO `english_phrase` VALUES (124, NULL, 'admin', '2021-04-08 07:54:37.000000', NULL, 'put it off', '');
INSERT INTO `english_phrase` VALUES (125, NULL, 'admin', '2021-04-08 07:54:55.000000', NULL, 'put sth off', '');
INSERT INTO `english_phrase` VALUES (126, '2021-04-08 08:04:46.000000', 'admin', NULL, NULL, 'deal with', '');
INSERT INTO `english_phrase` VALUES (127, '2021-04-09 03:48:17.000000', 'admin', NULL, NULL, 'asked sb out', '');
INSERT INTO `english_phrase` VALUES (128, '2021-04-09 03:48:55.000000', 'admin', NULL, NULL, 'ate out', '');
INSERT INTO `english_phrase` VALUES (129, '2021-04-09 03:51:06.000000', 'admin', NULL, NULL, 'look forward to', '');
INSERT INTO `english_phrase` VALUES (130, '2021-04-09 03:51:10.000000', 'admin', NULL, NULL, 'looking forward to', '');
INSERT INTO `english_phrase` VALUES (131, '2021-04-09 03:51:39.000000', 'admin', NULL, NULL, 'looked forward to', '');
INSERT INTO `english_phrase` VALUES (132, NULL, 'admin', '2021-04-09 03:54:06.000000', NULL, 'go out with', '');
INSERT INTO `english_phrase` VALUES (133, '2021-04-09 03:53:36.000000', 'admin', NULL, NULL, 'going out with', '');
INSERT INTO `english_phrase` VALUES (134, '2021-04-09 03:53:57.000000', 'admin', NULL, NULL, 'went out with', '');
INSERT INTO `english_phrase` VALUES (135, NULL, 'admin', '2021-04-09 03:55:34.000000', NULL, 'look down on', '');
INSERT INTO `english_phrase` VALUES (136, '2021-04-09 03:55:46.000000', 'admin', NULL, NULL, 'looked down on', '');
INSERT INTO `english_phrase` VALUES (137, '2021-04-09 03:56:05.000000', 'admin', NULL, NULL, 'looking down on', '');
INSERT INTO `english_phrase` VALUES (138, '2021-04-09 03:56:50.000000', 'admin', NULL, NULL, 'do away with', '');
INSERT INTO `english_phrase` VALUES (139, '2021-04-09 04:07:35.000000', 'admin', NULL, NULL, 'did away with', '');
INSERT INTO `english_phrase` VALUES (140, '2021-04-09 04:07:56.000000', 'admin', NULL, NULL, 'does away with', '');
INSERT INTO `english_phrase` VALUES (141, '2021-04-09 04:08:08.000000', 'admin', NULL, NULL, 'doing away with', '');
INSERT INTO `english_phrase` VALUES (142, '2021-04-09 04:10:05.000000', 'admin', NULL, NULL, 'come up with a plan', '');
INSERT INTO `english_phrase` VALUES (143, NULL, 'admin', '2021-04-09 04:15:06.000000', NULL, 'come up with an idea', '');
INSERT INTO `english_phrase` VALUES (144, '2021-04-09 04:13:02.000000', 'admin', NULL, NULL, 'come up with solutions', '');
INSERT INTO `english_phrase` VALUES (145, '2021-04-09 04:13:50.000000', 'admin', NULL, NULL, 'came up with solutions', '');
INSERT INTO `english_phrase` VALUES (146, '2021-04-09 04:14:43.000000', 'admin', NULL, NULL, 'came up with an idea', '');
INSERT INTO `english_phrase` VALUES (147, NULL, 'admin', '2021-04-09 04:21:05.000000', NULL, 'get away with', '');
INSERT INTO `english_phrase` VALUES (148, '2021-04-09 04:24:25.000000', 'admin', NULL, NULL, 'give in', '');
INSERT INTO `english_phrase` VALUES (149, '2021-04-09 04:24:44.000000', 'admin', NULL, NULL, 'give sth in', '');
INSERT INTO `english_phrase` VALUES (150, '2021-04-09 04:25:54.000000', 'admin', NULL, NULL, 'put off', '');
INSERT INTO `english_phrase` VALUES (151, '2021-04-09 04:27:34.000000', 'admin', NULL, NULL, 'take this coat off', '');
INSERT INTO `english_phrase` VALUES (152, '2021-04-09 04:28:18.000000', 'admin', NULL, NULL, 'take off', '');
INSERT INTO `english_phrase` VALUES (153, '2021-04-09 04:30:02.000000', 'admin', NULL, NULL, 'take off shoes', '');
INSERT INTO `english_phrase` VALUES (154, '2021-04-09 04:31:21.000000', 'admin', NULL, NULL, 'turn up', '');
INSERT INTO `english_phrase` VALUES (155, '2021-04-09 04:31:57.000000', 'admin', NULL, NULL, 'turn up on time', '');
INSERT INTO `english_phrase` VALUES (156, '2021-04-09 06:41:52.000000', 'admin', NULL, NULL, 'cut off', '');
INSERT INTO `english_phrase` VALUES (157, '2021-04-09 06:47:21.000000', 'admin', NULL, NULL, 'turned off the light', '');
INSERT INTO `english_phrase` VALUES (158, NULL, 'admin', '2021-04-12 07:07:41.000000', NULL, 'turn the tv off', '');
INSERT INTO `english_phrase` VALUES (159, '2021-04-09 06:48:20.000000', 'admin', NULL, NULL, 'turn off the light', '');
INSERT INTO `english_phrase` VALUES (160, '2021-04-12 04:34:48.000000', 'admin', NULL, NULL, 'get over fear', '');
INSERT INTO `english_phrase` VALUES (161, '2021-04-12 04:35:59.000000', 'admin', NULL, NULL, 'get over myself', '');
INSERT INTO `english_phrase` VALUES (162, NULL, 'admin', '2021-04-12 06:58:24.000000', NULL, 'keep on', '');
INSERT INTO `english_phrase` VALUES (163, '2021-04-12 06:55:30.000000', 'admin', NULL, NULL, 'keep on trying', '');
INSERT INTO `english_phrase` VALUES (164, '2021-04-12 06:58:14.000000', 'admin', NULL, NULL, 'keep on going', '');
INSERT INTO `english_phrase` VALUES (165, '2021-04-12 07:00:21.000000', 'admin', NULL, NULL, 'kept on', '');
INSERT INTO `english_phrase` VALUES (166, NULL, 'admin', '2021-04-12 07:05:25.000000', NULL, 'pick sb up at', '');
INSERT INTO `english_phrase` VALUES (167, NULL, 'admin', '2021-04-12 07:07:05.000000', NULL, 'pick sb up', '');
INSERT INTO `english_phrase` VALUES (168, '2021-04-12 07:06:55.000000', 'admin', NULL, NULL, 'pick sth up', '');
INSERT INTO `english_phrase` VALUES (169, '2021-04-12 07:11:03.000000', 'admin', NULL, NULL, 'go against', '');
INSERT INTO `english_phrase` VALUES (170, '2021-04-12 07:21:05.000000', 'admin', NULL, NULL, 'go against the grain', '');
INSERT INTO `english_phrase` VALUES (171, '2021-04-12 08:21:55.000000', 'admin', NULL, NULL, 'cut out of decision-making meetings', '');
INSERT INTO `english_phrase` VALUES (172, '2021-04-12 08:25:01.000000', 'admin', NULL, NULL, 'cut out any words', '');
INSERT INTO `english_phrase` VALUES (173, NULL, 'admin', '2021-04-12 08:31:19.000000', NULL, 'cut out', '');
INSERT INTO `english_phrase` VALUES (174, '2021-04-12 08:34:24.000000', 'admin', NULL, NULL, 'broke down', '');
INSERT INTO `english_phrase` VALUES (175, '2021-04-12 08:37:33.000000', 'admin', NULL, NULL, 'sb tried as best as sb could', '');
INSERT INTO `english_phrase` VALUES (176, NULL, 'admin', '2021-04-12 08:48:41.000000', NULL, 'go through', '');
INSERT INTO `english_phrase` VALUES (177, NULL, 'admin', '2021-04-12 08:48:31.000000', NULL, 'went through', '');
INSERT INTO `english_phrase` VALUES (178, NULL, 'admin', '2021-04-12 09:07:12.000000', NULL, 'brush aside sth', '');
INSERT INTO `english_phrase` VALUES (179, NULL, 'admin', '2021-04-12 09:07:07.000000', NULL, 'brush sth aside', '');
INSERT INTO `english_phrase` VALUES (180, NULL, 'admin', '2021-04-12 09:07:03.000000', NULL, 'brushed aside sth', '');
INSERT INTO `english_phrase` VALUES (181, NULL, 'admin', '2021-04-12 09:06:30.000000', NULL, 'brushed sth aside', '');
INSERT INTO `english_phrase` VALUES (182, '2021-04-12 09:12:41.000000', 'admin', NULL, NULL, 'fell for', '');
INSERT INTO `english_phrase` VALUES (183, '2021-04-12 09:13:25.000000', 'admin', NULL, NULL, 'fall for', '');
INSERT INTO `english_phrase` VALUES (184, NULL, 'admin', '2021-04-12 09:15:28.000000', NULL, 'make out what', '');
INSERT INTO `english_phrase` VALUES (185, NULL, 'admin', '2021-04-12 09:15:40.000000', NULL, 'make out what exactly', '');
INSERT INTO `english_phrase` VALUES (186, '2021-04-12 09:20:56.000000', 'admin', NULL, NULL, 'make out that', '');
INSERT INTO `english_phrase` VALUES (187, '2021-04-12 09:20:59.000000', 'admin', NULL, NULL, 'made out that', '');
INSERT INTO `english_phrase` VALUES (188, '2021-04-12 09:30:39.000000', 'admin', NULL, NULL, 'chase up', '');
INSERT INTO `english_phrase` VALUES (189, '2021-04-12 09:32:26.000000', 'admin', NULL, NULL, 'chased up', '');
INSERT INTO `english_phrase` VALUES (190, '2021-04-12 09:33:46.000000', 'admin', NULL, NULL, 'leave out', '');
INSERT INTO `english_phrase` VALUES (191, '2021-04-12 09:34:30.000000', 'admin', NULL, NULL, 'leave sth out', '');
INSERT INTO `english_phrase` VALUES (192, NULL, 'admin', '2021-04-12 09:37:31.000000', NULL, 'left out', '');
INSERT INTO `english_phrase` VALUES (193, '2021-04-12 09:42:36.000000', 'admin', NULL, NULL, 'put out a report', '');
INSERT INTO `english_phrase` VALUES (194, '2021-04-12 09:43:55.000000', 'admin', NULL, NULL, 'put out a statement', '');
INSERT INTO `english_phrase` VALUES (195, '2021-04-12 09:47:51.000000', 'admin', NULL, NULL, 'call off the search', '');
INSERT INTO `english_phrase` VALUES (196, '2021-04-12 09:48:22.000000', 'admin', NULL, NULL, 'call off the wedding', '');
INSERT INTO `english_phrase` VALUES (197, '2021-04-12 09:50:26.000000', 'admin', NULL, NULL, 'call off', '');
INSERT INTO `english_phrase` VALUES (198, NULL, 'admin', '2021-04-12 09:56:23.000000', NULL, 'called off', '');
INSERT INTO `english_phrase` VALUES (199, '2021-04-12 09:55:25.000000', 'admin', NULL, NULL, 'called off after', '');
INSERT INTO `english_phrase` VALUES (200, '2021-04-12 09:59:46.000000', 'admin', NULL, NULL, 'go down', '');
INSERT INTO `english_phrase` VALUES (201, '2021-04-12 10:02:44.000000', 'admin', NULL, NULL, 'went down this path', '');
INSERT INTO `english_phrase` VALUES (202, '2021-04-12 10:02:49.000000', 'admin', NULL, NULL, 'go down this path', '');
INSERT INTO `english_phrase` VALUES (203, '2021-04-12 10:03:03.000000', 'admin', NULL, NULL, 'go down this road', '');
INSERT INTO `english_phrase` VALUES (204, '2021-04-12 10:03:52.000000', 'admin', NULL, NULL, 'went down this road', '');
INSERT INTO `english_phrase` VALUES (205, '2021-04-12 10:05:21.000000', 'admin', NULL, NULL, 'get away', '');
INSERT INTO `english_phrase` VALUES (206, '2021-04-12 10:10:22.000000', 'admin', NULL, NULL, 'get away from', '');
INSERT INTO `english_phrase` VALUES (207, '2021-04-13 08:08:08.000000', 'admin', NULL, NULL, 'stored at', '');
INSERT INTO `english_phrase` VALUES (208, '2021-04-15 07:10:13.000000', 'admin', NULL, NULL, 'the main difference', '');
INSERT INTO `english_phrase` VALUES (209, '2021-04-15 07:10:19.000000', 'admin', NULL, NULL, 'the main differences', '');
INSERT INTO `english_phrase` VALUES (210, '2021-04-15 07:12:07.000000', 'admin', NULL, NULL, 'heap memory', '');
INSERT INTO `english_phrase` VALUES (211, '2021-04-15 07:13:26.000000', 'admin', NULL, NULL, 'abstract data type', '');
INSERT INTO `english_phrase` VALUES (212, '2021-04-15 07:13:36.000000', 'admin', NULL, NULL, 'data type', '');
INSERT INTO `english_phrase` VALUES (213, '2021-04-15 07:15:09.000000', 'admin', NULL, NULL, 'special region', '');
INSERT INTO `english_phrase` VALUES (214, '2021-04-15 07:16:15.000000', 'admin', NULL, NULL, 'random access memory', '');
INSERT INTO `english_phrase` VALUES (215, '2021-04-15 07:18:13.000000', 'admin', NULL, NULL, 'so-called', '');
INSERT INTO `english_phrase` VALUES (216, '2021-04-15 07:20:16.000000', 'admin', NULL, NULL, 'call stack', '');
INSERT INTO `english_phrase` VALUES (217, '2021-04-15 07:22:26.000000', 'admin', NULL, NULL, 'post a question', '');
INSERT INTO `english_phrase` VALUES (218, '2021-04-15 07:27:12.000000', 'admin', NULL, NULL, 'it keeps track of', '');
INSERT INTO `english_phrase` VALUES (219, '2021-04-15 07:31:47.000000', 'admin', NULL, NULL, 'the point to', '');
INSERT INTO `english_phrase` VALUES (220, '2021-04-15 07:40:10.000000', 'admin', NULL, NULL, 'get rid of', '');
INSERT INTO `english_phrase` VALUES (221, '2021-04-15 07:45:53.000000', 'admin', NULL, NULL, 'everytime', '');
INSERT INTO `english_phrase` VALUES (222, '2021-04-15 08:26:50.000000', 'admin', NULL, NULL, 'under the table', '');
INSERT INTO `english_phrase` VALUES (223, '2021-04-15 08:30:31.000000', 'admin', NULL, NULL, 'under the hood', '');
INSERT INTO `english_phrase` VALUES (224, '2021-04-15 08:36:33.000000', 'admin', NULL, NULL, 'in term of', '');
INSERT INTO `english_phrase` VALUES (225, '2021-04-15 09:00:27.000000', 'admin', NULL, NULL, 'would like to', '');
INSERT INTO `english_phrase` VALUES (226, '2021-04-15 10:01:51.000000', 'admin', NULL, NULL, 'several situations', '');
INSERT INTO `english_phrase` VALUES (227, '2021-04-15 10:02:58.000000', 'admin', NULL, NULL, 'quite handy', '');
INSERT INTO `english_phrase` VALUES (228, '2021-04-15 10:04:27.000000', 'admin', NULL, NULL, 'pose the question', '');
INSERT INTO `english_phrase` VALUES (229, '2021-04-15 10:11:00.000000', 'admin', NULL, NULL, 'in a nutshell', '');
INSERT INTO `english_phrase` VALUES (230, NULL, 'admin', '2021-04-15 10:17:32.000000', NULL, 'in the background', '');
INSERT INTO `english_phrase` VALUES (231, '2021-04-15 10:16:27.000000', 'admin', NULL, NULL, 'transformed into', '');
INSERT INTO `english_phrase` VALUES (232, '2021-04-15 10:28:10.000000', 'admin', NULL, NULL, 'it\'s important that', '');
INSERT INTO `english_phrase` VALUES (233, '2021-04-15 10:28:26.000000', 'admin', NULL, NULL, 'it\'s important', '');
INSERT INTO `english_phrase` VALUES (234, '2021-04-15 10:29:01.000000', 'admin', NULL, NULL, 'it\'s important to', '');
INSERT INTO `english_phrase` VALUES (235, NULL, 'admin', '2021-05-06 03:34:00.000000', NULL, 'no matter what', '');
INSERT INTO `english_phrase` VALUES (236, '2021-04-15 10:55:56.000000', 'admin', NULL, NULL, 'central processing unit', '');
INSERT INTO `english_phrase` VALUES (237, '2021-04-15 10:56:12.000000', 'admin', NULL, NULL, 'as about as', '');
INSERT INTO `english_phrase` VALUES (238, NULL, 'admin', '2021-05-04 03:32:49.000000', NULL, 'ok', '');
INSERT INTO `english_phrase` VALUES (239, '2021-05-04 08:26:49.000000', 'admin', NULL, NULL, 'warm', '');
INSERT INTO `english_phrase` VALUES (240, '2021-05-06 03:32:22.000000', 'admin', NULL, NULL, 'responsibility', '');
INSERT INTO `english_phrase` VALUES (241, '2021-05-06 03:33:00.000000', 'admin', NULL, NULL, 'thread scheduler', '');
INSERT INTO `english_phrase` VALUES (242, '2021-05-06 03:33:09.000000', 'admin', NULL, NULL, 'thread', '');
INSERT INTO `english_phrase` VALUES (243, '2021-05-06 03:33:31.000000', 'admin', NULL, NULL, 'time', '');
INSERT INTO `english_phrase` VALUES (244, '2021-05-06 03:37:54.000000', 'admin', NULL, NULL, 'runnable state', '');
INSERT INTO `english_phrase` VALUES (245, '2021-05-06 03:38:04.000000', 'admin', NULL, NULL, 'new thread', '');
INSERT INTO `english_phrase` VALUES (246, '2021-05-06 03:39:56.000000', 'admin', NULL, NULL, 'leave litter around', '');
INSERT INTO `english_phrase` VALUES (247, '2021-05-06 03:40:09.000000', 'admin', NULL, NULL, 'leave sth around', '');
INSERT INTO `english_phrase` VALUES (248, '2021-05-06 03:41:16.000000', 'admin', NULL, NULL, 'spoils', '');
INSERT INTO `english_phrase` VALUES (249, '2021-05-06 03:48:11.000000', 'admin', NULL, NULL, 'individual', '');
INSERT INTO `english_phrase` VALUES (250, '2021-05-06 03:48:26.000000', 'admin', NULL, NULL, 'individual thread', '');
INSERT INTO `english_phrase` VALUES (251, '2021-05-06 03:49:03.000000', 'admin', NULL, NULL, 'allocate', '');
INSERT INTO `english_phrase` VALUES (252, '2021-05-06 03:49:10.000000', 'admin', NULL, NULL, 'allocates', '');
INSERT INTO `english_phrase` VALUES (253, '2021-05-06 03:49:28.000000', 'admin', NULL, NULL, 'amount of time', '');
INSERT INTO `english_phrase` VALUES (254, '2021-05-06 03:52:24.000000', 'admin', NULL, NULL, 'blocked state', '');
INSERT INTO `english_phrase` VALUES (255, '2021-05-06 03:52:40.000000', 'admin', NULL, NULL, 'waiting state', '');
INSERT INTO `english_phrase` VALUES (256, '2021-05-06 03:52:51.000000', 'admin', NULL, NULL, 'inactive', '');
INSERT INTO `english_phrase` VALUES (257, '2021-05-06 03:53:04.000000', 'admin', NULL, NULL, 'temporarily', '');
INSERT INTO `english_phrase` VALUES (258, '2021-05-06 03:54:29.000000', 'admin', NULL, NULL, 'following states', '');
INSERT INTO `english_phrase` VALUES (259, '2021-05-06 03:54:35.000000', 'admin', NULL, NULL, 'the following states', '');
INSERT INTO `english_phrase` VALUES (260, '2021-05-06 04:06:54.000000', 'admin', NULL, NULL, 'timed waiting', '');
INSERT INTO `english_phrase` VALUES (261, '2021-05-06 04:20:25.000000', 'admin', NULL, NULL, 'timeout', '');
INSERT INTO `english_phrase` VALUES (262, '2021-05-06 06:59:47.000000', 'admin', NULL, NULL, 'keyword', '');
INSERT INTO `english_phrase` VALUES (263, '2021-05-06 07:00:19.000000', 'admin', NULL, NULL, 'the traditional way', '');
INSERT INTO `english_phrase` VALUES (264, '2021-05-06 07:01:33.000000', 'admin', NULL, NULL, 'achieve', '');

-- ----------------------------
-- Table structure for english_phrase_vietnamese_phrase
-- ----------------------------
DROP TABLE IF EXISTS `english_phrase_vietnamese_phrase`;
CREATE TABLE `english_phrase_vietnamese_phrase`  (
  `english_phrase_id` bigint NOT NULL,
  `vietnamese_phrase_id` bigint NOT NULL,
  PRIMARY KEY (`english_phrase_id`, `vietnamese_phrase_id`) USING BTREE,
  INDEX `FKhe7ryv9ikgfmpu3y2w5x0vdis`(`vietnamese_phrase_id`) USING BTREE,
  CONSTRAINT `FKhe7ryv9ikgfmpu3y2w5x0vdis` FOREIGN KEY (`vietnamese_phrase_id`) REFERENCES `vietnamese_phrase` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKlmjvkux33ixnyd9v75o8w5lod` FOREIGN KEY (`english_phrase_id`) REFERENCES `english_phrase` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of english_phrase_vietnamese_phrase
-- ----------------------------
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (1, 1);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (2, 2);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (2, 3);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (2, 4);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (2, 57);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (2, 58);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (3, 5);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (3, 6);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (4, 7);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (4, 8);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (5, 9);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (5, 10);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (6, 11);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (6, 12);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (6, 13);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (7, 14);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (8, 15);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (9, 16);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (9, 17);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (10, 18);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (11, 19);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (11, 20);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (11, 21);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (12, 22);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (12, 23);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (12, 24);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (13, 25);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (13, 26);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (13, 27);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (14, 28);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (14, 29);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (15, 30);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (15, 31);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (15, 32);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (16, 33);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (16, 34);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (17, 35);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (18, 36);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (18, 37);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (18, 38);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (19, 39);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (20, 39);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (21, 39);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (22, 40);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (23, 41);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (23, 42);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (23, 43);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (23, 44);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (23, 45);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (24, 46);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (24, 47);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (24, 48);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (24, 49);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (25, 50);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (25, 51);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (25, 52);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (26, 53);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (27, 11);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (28, 54);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (29, 55);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (30, 56);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (31, 11);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (31, 59);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (32, 60);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (32, 61);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (32, 62);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (32, 63);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (32, 64);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (32, 68);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (33, 60);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (33, 61);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (33, 62);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (33, 63);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (33, 64);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (33, 69);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (33, 70);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (34, 65);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (34, 66);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (34, 67);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (35, 65);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (35, 66);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (35, 67);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (36, 60);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (36, 61);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (36, 62);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (36, 63);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (36, 64);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (36, 68);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (37, 60);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (37, 61);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (37, 62);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (37, 63);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (37, 64);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (37, 69);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (37, 70);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (38, 71);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (38, 72);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (38, 73);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (38, 74);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (38, 75);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (39, 71);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (39, 72);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (39, 73);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (39, 74);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (39, 75);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (40, 76);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (40, 77);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (41, 78);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (42, 79);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (42, 81);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (42, 82);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (43, 80);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (44, 83);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (45, 79);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (45, 84);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (45, 85);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (45, 86);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (46, 79);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (46, 86);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (46, 87);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (47, 88);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (47, 89);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (48, 90);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (49, 91);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (49, 92);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (49, 93);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (49, 94);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (49, 103);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (50, 95);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (51, 96);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (52, 97);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (53, 98);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (53, 99);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (53, 100);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (53, 101);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (54, 102);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (55, 104);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (56, 105);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (56, 106);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (56, 107);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (57, 108);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (57, 109);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (58, 110);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (58, 111);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (59, 112);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (59, 113);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (60, 114);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (61, 115);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (62, 116);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (63, 117);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (64, 118);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (65, 118);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (66, 119);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (66, 120);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (67, 121);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (68, 122);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (69, 123);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (69, 124);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (70, 125);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (70, 126);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (71, 127);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (71, 130);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (72, 128);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (73, 129);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (74, 119);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (74, 131);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (74, 133);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (75, 132);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (76, 134);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (76, 135);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (76, 137);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (77, 136);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (78, 138);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (78, 139);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (79, 140);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (79, 141);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (80, 142);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (80, 143);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (81, 144);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (81, 145);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (82, 146);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (83, 147);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (83, 148);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (84, 149);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (85, 150);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (86, 151);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (87, 152);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (88, 152);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (88, 209);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (89, 153);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (90, 154);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (91, 152);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (92, 67);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (92, 156);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (92, 226);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (93, 155);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (94, 157);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (94, 158);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (95, 159);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (95, 160);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (95, 161);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (96, 159);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (96, 160);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (96, 161);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (97, 162);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (97, 163);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (98, 164);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (98, 165);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (99, 166);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (99, 167);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (100, 168);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (101, 162);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (101, 163);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (101, 169);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (102, 162);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (102, 163);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (102, 169);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (103, 170);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (104, 171);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (105, 172);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (105, 173);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (105, 203);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (106, 174);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (107, 114);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (108, 175);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (109, 176);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (109, 177);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (110, 178);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (111, 179);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (112, 180);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (113, 181);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (113, 182);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (113, 183);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (113, 204);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (114, 184);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (115, 185);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (116, 186);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (116, 187);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (116, 188);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (116, 189);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (116, 204);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (116, 205);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (116, 263);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (117, 190);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (117, 207);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (118, 191);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (118, 206);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (119, 191);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (120, 192);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (120, 193);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (121, 194);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (122, 195);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (122, 196);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (123, 197);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (124, 88);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (124, 198);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (124, 200);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (125, 199);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (125, 201);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (125, 202);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (126, 192);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (126, 193);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (126, 208);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (127, 114);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (128, 105);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (128, 106);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (128, 107);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (129, 210);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (129, 211);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (130, 210);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (130, 211);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (131, 210);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (131, 211);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (132, 212);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (132, 213);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (133, 212);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (133, 213);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (134, 212);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (134, 213);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (135, 214);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (135, 215);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (136, 214);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (136, 215);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (137, 214);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (137, 215);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (138, 216);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (138, 217);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (139, 216);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (139, 217);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (140, 216);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (140, 217);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (141, 216);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (141, 217);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (142, 218);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (143, 219);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (143, 220);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (143, 222);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (143, 223);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (144, 221);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (145, 221);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (146, 219);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (146, 220);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (146, 222);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (146, 223);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (147, 133);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (147, 224);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (147, 225);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (148, 157);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (148, 158);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (149, 227);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (150, 198);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (150, 200);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (151, 228);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (152, 229);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (152, 230);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (152, 231);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (152, 232);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (153, 233);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (154, 162);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (154, 163);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (155, 234);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (155, 235);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (156, 236);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (156, 237);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (157, 238);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (158, 239);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (159, 238);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (160, 240);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (160, 241);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (161, 242);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (162, 79);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (162, 86);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (162, 244);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (163, 243);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (164, 79);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (164, 86);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (165, 79);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (165, 86);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (165, 244);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (165, 245);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (166, 246);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (167, 247);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (167, 250);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (168, 248);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (168, 249);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (169, 251);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (169, 252);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (169, 253);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (170, 254);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (170, 255);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (171, 256);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (172, 257);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (172, 258);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (173, 259);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (173, 260);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (173, 261);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (173, 262);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (174, 186);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (174, 187);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (174, 188);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (174, 189);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (174, 204);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (174, 205);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (174, 263);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (175, 264);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (176, 100);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (176, 265);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (176, 266);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (176, 267);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (176, 268);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (176, 269);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (176, 270);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (177, 100);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (177, 265);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (177, 266);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (177, 267);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (177, 268);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (177, 269);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (177, 270);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (178, 271);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (178, 272);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (178, 273);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (179, 271);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (179, 272);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (179, 273);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (180, 271);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (180, 272);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (180, 273);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (181, 271);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (181, 272);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (181, 273);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (182, 274);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (182, 275);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (182, 276);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (183, 274);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (183, 275);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (183, 276);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (184, 277);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (184, 279);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (185, 278);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (185, 280);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (186, 281);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (186, 282);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (187, 281);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (187, 282);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (188, 283);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (188, 284);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (188, 285);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (189, 283);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (189, 284);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (189, 285);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (190, 216);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (191, 286);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (192, 216);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (192, 287);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (193, 288);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (194, 289);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (195, 290);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (195, 291);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (196, 292);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (197, 197);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (197, 217);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (197, 293);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (197, 294);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (197, 295);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (198, 197);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (198, 217);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (198, 293);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (198, 294);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (198, 295);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (198, 298);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (199, 296);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (199, 297);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (200, 299);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (200, 300);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (200, 301);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (200, 302);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (201, 303);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (202, 303);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (203, 303);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (204, 303);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (205, 133);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (205, 304);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (206, 73);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (207, 305);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (207, 306);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (207, 307);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (208, 308);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (209, 309);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (210, 310);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (211, 311);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (212, 312);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (213, 313);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (213, 314);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (214, 315);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (214, 316);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (215, 317);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (216, 315);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (217, 318);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (217, 319);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (218, 320);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (219, 321);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (219, 322);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (220, 216);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (220, 259);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (220, 323);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (221, 324);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (221, 325);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (222, 326);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (222, 327);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (223, 328);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (223, 329);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (223, 330);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (224, 331);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (224, 332);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (225, 333);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (226, 334);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (226, 335);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (227, 336);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (228, 319);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (229, 337);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (230, 338);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (230, 339);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (230, 341);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (231, 340);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (232, 342);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (233, 342);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (234, 343);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (234, 344);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (235, 345);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (235, 346);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (235, 347);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (236, 348);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (237, 349);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (237, 350);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (238, 351);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (239, 352);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (239, 353);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (239, 354);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (240, 355);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (241, 356);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (242, 357);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (243, 358);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (244, 359);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (245, 360);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (246, 361);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (247, 362);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (248, 363);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (248, 364);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (249, 365);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (249, 366);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (249, 367);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (250, 368);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (250, 369);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (251, 370);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (251, 371);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (252, 370);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (252, 371);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (253, 372);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (254, 373);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (255, 374);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (255, 375);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (256, 262);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (257, 376);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (258, 377);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (259, 377);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (260, 378);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (261, 379);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (261, 380);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (262, 381);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (263, 382);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (263, 383);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (263, 384);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (263, 385);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (264, 386);
INSERT INTO `english_phrase_vietnamese_phrase` VALUES (264, 387);

-- ----------------------------
-- Table structure for episode
-- ----------------------------
DROP TABLE IF EXISTS `episode`;
CREATE TABLE `episode`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of episode
-- ----------------------------
INSERT INTO `episode` VALUES (1, NULL, NULL, NULL, NULL, NULL, 'EPISODE 1');
INSERT INTO `episode` VALUES (2, NULL, NULL, NULL, NULL, NULL, 'EPISODE 2');
INSERT INTO `episode` VALUES (3, NULL, NULL, NULL, NULL, NULL, 'EPISODE 3');
INSERT INTO `episode` VALUES (4, NULL, NULL, NULL, NULL, NULL, 'EPISODE 4');
INSERT INTO `episode` VALUES (5, NULL, NULL, NULL, NULL, NULL, 'EPISODE 5');
INSERT INTO `episode` VALUES (6, NULL, NULL, NULL, NULL, NULL, 'EPISODE 6');
INSERT INTO `episode` VALUES (7, NULL, NULL, NULL, NULL, NULL, 'EPISODE 7');
INSERT INTO `episode` VALUES (8, NULL, NULL, NULL, NULL, NULL, 'EPISODE 8');
INSERT INTO `episode` VALUES (9, NULL, NULL, NULL, NULL, NULL, 'EPISODE 9');
INSERT INTO `episode` VALUES (10, NULL, NULL, NULL, NULL, NULL, 'EPISODE 10');
INSERT INTO `episode` VALUES (11, NULL, NULL, NULL, NULL, NULL, 'EPISODE 11');
INSERT INTO `episode` VALUES (12, NULL, NULL, NULL, NULL, NULL, 'EPISODE 12');
INSERT INTO `episode` VALUES (13, NULL, NULL, NULL, NULL, NULL, 'EPISODE 13');
INSERT INTO `episode` VALUES (14, NULL, NULL, NULL, NULL, NULL, 'EPISODE 14');
INSERT INTO `episode` VALUES (15, NULL, NULL, NULL, NULL, NULL, 'EPISODE 15');
INSERT INTO `episode` VALUES (16, NULL, NULL, NULL, NULL, NULL, 'EPISODE 16');
INSERT INTO `episode` VALUES (17, NULL, NULL, NULL, NULL, NULL, 'EPISODE 17');
INSERT INTO `episode` VALUES (18, NULL, NULL, NULL, NULL, NULL, 'EPISODE 18');
INSERT INTO `episode` VALUES (19, NULL, NULL, NULL, NULL, NULL, 'EPISODE 19');
INSERT INTO `episode` VALUES (20, NULL, NULL, NULL, NULL, NULL, 'EPISODE 20');
INSERT INTO `episode` VALUES (21, NULL, NULL, NULL, NULL, NULL, 'EPISODE 21');
INSERT INTO `episode` VALUES (22, NULL, NULL, NULL, NULL, NULL, 'EPISODE 22');
INSERT INTO `episode` VALUES (23, NULL, NULL, NULL, NULL, NULL, 'EPISODE 23');
INSERT INTO `episode` VALUES (24, NULL, NULL, NULL, NULL, NULL, 'EPISODE 24');
INSERT INTO `episode` VALUES (25, NULL, NULL, NULL, NULL, NULL, 'EPISODE 25');
INSERT INTO `episode` VALUES (26, NULL, NULL, NULL, NULL, NULL, 'EPISODE 26');
INSERT INTO `episode` VALUES (27, NULL, NULL, NULL, NULL, NULL, 'EPISODE 27');
INSERT INTO `episode` VALUES (28, NULL, NULL, NULL, NULL, NULL, 'EPISODE 28');
INSERT INTO `episode` VALUES (29, NULL, NULL, NULL, NULL, NULL, 'EPISODE 29');
INSERT INTO `episode` VALUES (30, NULL, NULL, NULL, NULL, NULL, 'EPISODE 30');
INSERT INTO `episode` VALUES (31, NULL, NULL, NULL, NULL, NULL, 'EPISODE 31');
INSERT INTO `episode` VALUES (32, NULL, NULL, NULL, NULL, NULL, 'EPISODE 32');
INSERT INTO `episode` VALUES (33, NULL, NULL, NULL, NULL, NULL, 'EPISODE 33');
INSERT INTO `episode` VALUES (34, NULL, NULL, NULL, NULL, NULL, 'EPISODE 34');
INSERT INTO `episode` VALUES (35, NULL, NULL, NULL, NULL, NULL, 'EPISODE 35');
INSERT INTO `episode` VALUES (36, NULL, NULL, NULL, NULL, NULL, 'EPISODE 36');
INSERT INTO `episode` VALUES (37, NULL, NULL, NULL, NULL, NULL, 'EPISODE 37');
INSERT INTO `episode` VALUES (38, NULL, NULL, NULL, NULL, NULL, 'EPISODE 38');
INSERT INTO `episode` VALUES (39, NULL, NULL, NULL, NULL, NULL, 'EPISODE 39');
INSERT INTO `episode` VALUES (40, NULL, NULL, NULL, NULL, NULL, 'EPISODE 40');
INSERT INTO `episode` VALUES (41, NULL, NULL, NULL, NULL, NULL, 'EPISODE 41');
INSERT INTO `episode` VALUES (42, NULL, NULL, NULL, NULL, NULL, 'EPISODE 42');
INSERT INTO `episode` VALUES (43, NULL, NULL, NULL, NULL, NULL, 'EPISODE 43');
INSERT INTO `episode` VALUES (44, NULL, NULL, NULL, NULL, NULL, 'EPISODE 44');
INSERT INTO `episode` VALUES (45, NULL, NULL, NULL, NULL, NULL, 'EPISODE 45');
INSERT INTO `episode` VALUES (46, NULL, NULL, NULL, NULL, NULL, 'EPISODE 45');
INSERT INTO `episode` VALUES (47, NULL, NULL, NULL, NULL, NULL, 'EPISODE 46');
INSERT INTO `episode` VALUES (48, NULL, NULL, NULL, NULL, NULL, 'EPISODE 47');
INSERT INTO `episode` VALUES (49, NULL, NULL, NULL, NULL, NULL, 'EPISODE 48');
INSERT INTO `episode` VALUES (50, NULL, NULL, NULL, NULL, NULL, 'EPISODE 49');
INSERT INTO `episode` VALUES (51, NULL, NULL, NULL, NULL, NULL, 'EPISODE 50');
INSERT INTO `episode` VALUES (52, NULL, NULL, NULL, NULL, NULL, 'EPISODE 51');
INSERT INTO `episode` VALUES (53, NULL, NULL, NULL, NULL, NULL, 'EPISODE 52');
INSERT INTO `episode` VALUES (54, NULL, NULL, NULL, NULL, NULL, 'EPISODE 53');
INSERT INTO `episode` VALUES (55, NULL, NULL, NULL, NULL, NULL, 'EPISODE 54');
INSERT INTO `episode` VALUES (56, NULL, NULL, NULL, NULL, NULL, 'EPISODE 55');
INSERT INTO `episode` VALUES (57, NULL, NULL, NULL, NULL, NULL, 'EPISODE 56');
INSERT INTO `episode` VALUES (58, NULL, NULL, NULL, NULL, NULL, 'EPISODE 57');
INSERT INTO `episode` VALUES (59, NULL, NULL, NULL, NULL, NULL, 'EPISODE 58');
INSERT INTO `episode` VALUES (60, NULL, NULL, NULL, NULL, NULL, 'EPISODE 59');
INSERT INTO `episode` VALUES (61, NULL, NULL, NULL, NULL, NULL, 'EPISODE 60');
INSERT INTO `episode` VALUES (62, NULL, NULL, NULL, NULL, NULL, 'EPISODE 61');
INSERT INTO `episode` VALUES (63, NULL, NULL, NULL, NULL, NULL, 'EPISODE 62');
INSERT INTO `episode` VALUES (64, NULL, NULL, NULL, NULL, NULL, 'EPISODE 63');
INSERT INTO `episode` VALUES (65, NULL, NULL, NULL, NULL, NULL, 'EPISODE 64');
INSERT INTO `episode` VALUES (66, NULL, NULL, NULL, NULL, NULL, 'EPISODE 65');
INSERT INTO `episode` VALUES (67, NULL, NULL, NULL, NULL, NULL, 'EPISODE 66');
INSERT INTO `episode` VALUES (68, NULL, NULL, NULL, NULL, NULL, 'EPISODE 67');
INSERT INTO `episode` VALUES (69, NULL, NULL, NULL, NULL, NULL, 'EPISODE 68');
INSERT INTO `episode` VALUES (70, NULL, NULL, NULL, NULL, NULL, 'EPISODE 69');
INSERT INTO `episode` VALUES (71, NULL, NULL, NULL, NULL, NULL, 'EPISODE 70');
INSERT INTO `episode` VALUES (72, NULL, NULL, NULL, NULL, NULL, 'EPISODE 71');
INSERT INTO `episode` VALUES (73, NULL, NULL, NULL, NULL, NULL, 'EPISODE 72');
INSERT INTO `episode` VALUES (74, NULL, NULL, NULL, NULL, NULL, 'EPISODE 73');
INSERT INTO `episode` VALUES (75, NULL, NULL, NULL, NULL, NULL, 'EPISODE 74');
INSERT INTO `episode` VALUES (76, NULL, NULL, NULL, NULL, NULL, 'EPISODE 75');
INSERT INTO `episode` VALUES (77, NULL, NULL, NULL, NULL, NULL, 'EPISODE 76');
INSERT INTO `episode` VALUES (78, NULL, NULL, NULL, NULL, NULL, 'EPISODE 77');
INSERT INTO `episode` VALUES (79, NULL, NULL, NULL, NULL, NULL, 'EPISODE 78');
INSERT INTO `episode` VALUES (80, NULL, NULL, NULL, NULL, NULL, 'EPISODE 79');
INSERT INTO `episode` VALUES (81, NULL, NULL, NULL, NULL, NULL, 'EPISODE 80');
INSERT INTO `episode` VALUES (82, NULL, NULL, NULL, NULL, NULL, 'EPISODE 81');
INSERT INTO `episode` VALUES (83, NULL, NULL, NULL, NULL, NULL, 'EPISODE 82');
INSERT INTO `episode` VALUES (84, NULL, NULL, NULL, NULL, NULL, 'EPISODE 83');
INSERT INTO `episode` VALUES (85, NULL, NULL, NULL, NULL, NULL, 'EPISODE 84');
INSERT INTO `episode` VALUES (86, NULL, NULL, NULL, NULL, NULL, 'EPISODE 85');
INSERT INTO `episode` VALUES (87, NULL, NULL, NULL, NULL, NULL, 'EPISODE 86');
INSERT INTO `episode` VALUES (88, NULL, NULL, NULL, NULL, NULL, 'EPISODE 87');
INSERT INTO `episode` VALUES (89, NULL, NULL, NULL, NULL, NULL, 'EPISODE 88');
INSERT INTO `episode` VALUES (90, NULL, NULL, NULL, NULL, NULL, 'EPISODE 89');
INSERT INTO `episode` VALUES (91, NULL, NULL, NULL, NULL, NULL, 'EPISODE 90');
INSERT INTO `episode` VALUES (92, NULL, NULL, NULL, NULL, NULL, 'EPISODE 91');
INSERT INTO `episode` VALUES (93, NULL, NULL, NULL, NULL, NULL, 'EPISODE 92');
INSERT INTO `episode` VALUES (94, NULL, NULL, NULL, NULL, NULL, 'EPISODE 93');
INSERT INTO `episode` VALUES (95, NULL, NULL, NULL, NULL, NULL, 'EPISODE 94');
INSERT INTO `episode` VALUES (96, NULL, NULL, NULL, NULL, NULL, 'EPISODE 95');
INSERT INTO `episode` VALUES (97, NULL, NULL, NULL, NULL, NULL, 'EPISODE 96');
INSERT INTO `episode` VALUES (98, NULL, NULL, NULL, NULL, NULL, 'EPISODE 97');
INSERT INTO `episode` VALUES (99, NULL, NULL, NULL, NULL, NULL, 'EPISODE 98');
INSERT INTO `episode` VALUES (100, NULL, NULL, NULL, NULL, NULL, 'EPISODE 99');
INSERT INTO `episode` VALUES (101, NULL, NULL, NULL, NULL, NULL, 'EPISODE 100');

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `data` longblob NULL,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of files
-- ----------------------------

-- ----------------------------
-- Table structure for genre
-- ----------------------------
DROP TABLE IF EXISTS `genre`;
CREATE TABLE `genre`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of genre
-- ----------------------------
INSERT INTO `genre` VALUES (1, '2021-01-02 12:58:04.000000', NULL, NULL, NULL, NULL, 'FAIRY TALE');
INSERT INTO `genre` VALUES (2, '2021-01-02 12:58:24.000000', NULL, NULL, NULL, NULL, 'MOVIE');
INSERT INTO `genre` VALUES (3, '2021-03-07 22:07:58.000000', NULL, NULL, NULL, NULL, 'IELTS');

-- ----------------------------
-- Table structure for itsentence
-- ----------------------------
DROP TABLE IF EXISTS `itsentence`;
CREATE TABLE `itsentence`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sentence` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `translation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of itsentence
-- ----------------------------

-- ----------------------------
-- Table structure for movie
-- ----------------------------
DROP TABLE IF EXISTS `movie`;
CREATE TABLE `movie`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `season_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `genre_id` bigint NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK2ggat6246891h4goynp4h9lk5`(`genre_id`) USING BTREE,
  CONSTRAINT `FK2ggat6246891h4goynp4h9lk5` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of movie
-- ----------------------------

-- ----------------------------
-- Table structure for movie_season_episode
-- ----------------------------
DROP TABLE IF EXISTS `movie_season_episode`;
CREATE TABLE `movie_season_episode`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `episode_id` bigint NULL DEFAULT NULL,
  `movie_id` bigint NULL DEFAULT NULL,
  `season_id` bigint NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK250nrc1mldqc8puugg69kx0ca`(`episode_id`) USING BTREE,
  INDEX `FKi2i4psox2v79oqsx58r4lvpwi`(`movie_id`) USING BTREE,
  INDEX `FK546i4gli53kt1hbmp1a9uf9s0`(`season_id`) USING BTREE,
  CONSTRAINT `FK250nrc1mldqc8puugg69kx0ca` FOREIGN KEY (`episode_id`) REFERENCES `episode` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK546i4gli53kt1hbmp1a9uf9s0` FOREIGN KEY (`season_id`) REFERENCES `season` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of movie_season_episode
-- ----------------------------

-- ----------------------------
-- Table structure for operation
-- ----------------------------
DROP TABLE IF EXISTS `operation`;
CREATE TABLE `operation`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `operation_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of operation
-- ----------------------------

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (1, '2020-12-12 10:10:14.000000', NULL, NULL, NULL, 'ADMIN PERMISSION', 'ADMIN PERMISSION');

-- ----------------------------
-- Table structure for permission_operation
-- ----------------------------
DROP TABLE IF EXISTS `permission_operation`;
CREATE TABLE `permission_operation`  (
  `permission_id` bigint NOT NULL,
  `operation_id` bigint NOT NULL,
  PRIMARY KEY (`permission_id`, `operation_id`) USING BTREE,
  INDEX `FK7nlykwfp4w4o56s2ds6nfsj40`(`operation_id`) USING BTREE,
  CONSTRAINT `FK7nlykwfp4w4o56s2ds6nfsj40` FOREIGN KEY (`operation_id`) REFERENCES `operation` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKjgwm48ps64lc6c7x0g5eld8um` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of permission_operation
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '2020-12-12 10:10:14.000000', NULL, NULL, NULL, 'ADMIN ROLE', 'ADMIN ROLE');

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission`  (
  `role_id` bigint NOT NULL,
  `permission_id` bigint NOT NULL,
  PRIMARY KEY (`role_id`, `permission_id`) USING BTREE,
  INDEX `FKf8yllw1ecvwqy3ehyxawqa1qp`(`permission_id`) USING BTREE,
  CONSTRAINT `FKa6jx8n8xkesmjmv6jqug6bg68` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKf8yllw1ecvwqy3ehyxawqa1qp` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES (1, 1);

-- ----------------------------
-- Table structure for season
-- ----------------------------
DROP TABLE IF EXISTS `season`;
CREATE TABLE `season`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of season
-- ----------------------------
INSERT INTO `season` VALUES (1, NULL, NULL, NULL, NULL, NULL, 'SEASON 1');
INSERT INTO `season` VALUES (2, NULL, NULL, NULL, NULL, NULL, 'SEASON 2');
INSERT INTO `season` VALUES (3, NULL, NULL, NULL, NULL, NULL, 'SEASON 3');
INSERT INTO `season` VALUES (4, NULL, NULL, NULL, NULL, NULL, 'SEASON 4');
INSERT INTO `season` VALUES (5, NULL, NULL, NULL, NULL, NULL, 'SEASON 5');
INSERT INTO `season` VALUES (6, NULL, NULL, NULL, NULL, NULL, 'SEASON 6');
INSERT INTO `season` VALUES (7, NULL, NULL, NULL, NULL, NULL, 'SEASON 7');
INSERT INTO `season` VALUES (8, NULL, NULL, NULL, NULL, NULL, 'SEASON 8');
INSERT INTO `season` VALUES (9, NULL, NULL, NULL, NULL, NULL, 'SEASON 9');
INSERT INTO `season` VALUES (10, NULL, NULL, NULL, NULL, NULL, 'SEASON 10');
INSERT INTO `season` VALUES (11, NULL, NULL, NULL, NULL, NULL, 'SEASON 11');
INSERT INTO `season` VALUES (12, NULL, NULL, NULL, NULL, NULL, 'SEASON 12');
INSERT INTO `season` VALUES (13, NULL, NULL, NULL, NULL, NULL, 'SEASON 13');
INSERT INTO `season` VALUES (14, NULL, NULL, NULL, NULL, NULL, 'SEASON 14');
INSERT INTO `season` VALUES (15, NULL, NULL, NULL, NULL, NULL, 'SEASON 15');

-- ----------------------------
-- Table structure for sentence
-- ----------------------------
DROP TABLE IF EXISTS `sentence`;
CREATE TABLE `sentence`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `audio` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sentence` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `translation` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `movie_season_episode_id` bigint NULL DEFAULT NULL,
  `description` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK7l3s9wfwcxjioqguhcvy6tjdk`(`movie_season_episode_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sentence
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `confirm_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `full_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '2020-12-12 10:10:14.000000', NULL, NULL, NULL, NULL, '$2a$10$RsRk5NeicWznHmNoRYp9OOcCpnFTQeS5z.cMnGMR0M0LoEW31rNny', '', '', '$2a$10$RsRk5NeicWznHmNoRYp9OOcCpnFTQeS5z.cMnGMR0M0LoEW31rNny', NULL, NULL, 'admin');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `user_id` bigint NOT NULL,
  `role_id` bigint NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE,
  INDEX `FKa68196081fvovjhkek5m97n3y`(`role_id`) USING BTREE,
  CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (1, 1);

-- ----------------------------
-- Table structure for vietnamese_phrase
-- ----------------------------
DROP TABLE IF EXISTS `vietnamese_phrase`;
CREATE TABLE `vietnamese_phrase`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime(6) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 388 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of vietnamese_phrase
-- ----------------------------
INSERT INTO `vietnamese_phrase` VALUES (1, '2021-03-30 05:38:14.000000', NULL, NULL, NULL, 'ăn trưa');
INSERT INTO `vietnamese_phrase` VALUES (2, '2021-03-30 05:44:04.000000', NULL, NULL, NULL, 'tổ chức tiệc');
INSERT INTO `vietnamese_phrase` VALUES (3, '2021-03-30 05:44:04.000000', NULL, NULL, NULL, 'mở tiệc');
INSERT INTO `vietnamese_phrase` VALUES (4, '2021-03-30 05:44:04.000000', NULL, NULL, NULL, 'tổ chức một bữa tiệc');
INSERT INTO `vietnamese_phrase` VALUES (5, '2021-03-30 05:59:50.000000', NULL, NULL, NULL, 'rút ra bài học');
INSERT INTO `vietnamese_phrase` VALUES (6, '2021-03-30 05:59:50.000000', NULL, NULL, NULL, 'có một bài học');
INSERT INTO `vietnamese_phrase` VALUES (7, '2021-03-30 06:01:21.000000', NULL, NULL, NULL, 'uống trà');
INSERT INTO `vietnamese_phrase` VALUES (8, '2021-03-30 06:01:42.000000', NULL, NULL, NULL, 'uống một tách trà');
INSERT INTO `vietnamese_phrase` VALUES (9, '2021-03-30 06:04:00.000000', NULL, NULL, NULL, 'uống một tách cà phê');
INSERT INTO `vietnamese_phrase` VALUES (10, '2021-03-30 06:04:00.000000', NULL, NULL, NULL, 'uống cà phê');
INSERT INTO `vietnamese_phrase` VALUES (11, '2021-03-30 06:08:54.000000', NULL, NULL, NULL, 'tắm');
INSERT INTO `vietnamese_phrase` VALUES (12, '2021-03-30 06:08:54.000000', NULL, NULL, NULL, 'đi tắm');
INSERT INTO `vietnamese_phrase` VALUES (13, '2021-03-30 06:08:54.000000', NULL, NULL, NULL, 'tắm vòi sen');
INSERT INTO `vietnamese_phrase` VALUES (14, '2021-03-30 06:11:51.000000', NULL, NULL, NULL, 'ăn tối');
INSERT INTO `vietnamese_phrase` VALUES (15, '2021-03-30 06:15:15.000000', NULL, NULL, NULL, 'ăn sáng');
INSERT INTO `vietnamese_phrase` VALUES (16, '2021-03-30 06:25:02.000000', NULL, NULL, NULL, 'ăn uống');
INSERT INTO `vietnamese_phrase` VALUES (17, '2021-03-30 06:25:02.000000', NULL, NULL, NULL, 'dùng bữa');
INSERT INTO `vietnamese_phrase` VALUES (18, '2021-03-30 06:29:51.000000', NULL, NULL, NULL, 'ăn chút gì đó');
INSERT INTO `vietnamese_phrase` VALUES (19, '2021-03-31 02:20:22.000000', NULL, NULL, NULL, 'thi đấu');
INSERT INTO `vietnamese_phrase` VALUES (20, '2021-03-31 02:20:22.000000', NULL, NULL, NULL, 'cuộc thi');
INSERT INTO `vietnamese_phrase` VALUES (21, '2021-03-31 02:20:22.000000', NULL, NULL, NULL, 'một cuộc thi');
INSERT INTO `vietnamese_phrase` VALUES (22, '2021-03-31 02:33:28.000000', NULL, NULL, NULL, 'thấy');
INSERT INTO `vietnamese_phrase` VALUES (23, '2021-03-31 02:33:28.000000', NULL, NULL, NULL, 'xem');
INSERT INTO `vietnamese_phrase` VALUES (24, '2021-03-31 02:33:28.000000', NULL, NULL, NULL, 'nhìn');
INSERT INTO `vietnamese_phrase` VALUES (25, '2021-03-31 02:37:08.000000', NULL, NULL, NULL, 'thử');
INSERT INTO `vietnamese_phrase` VALUES (26, '2021-03-31 02:37:08.000000', NULL, NULL, NULL, 'tự làm');
INSERT INTO `vietnamese_phrase` VALUES (27, '2021-03-31 02:38:12.000000', NULL, NULL, NULL, 'làm');
INSERT INTO `vietnamese_phrase` VALUES (28, '2021-03-31 02:48:35.000000', NULL, NULL, NULL, 'thượng lộ bình an');
INSERT INTO `vietnamese_phrase` VALUES (29, '2021-03-31 02:48:35.000000', NULL, NULL, NULL, 'chúc thượng lộ bình an');
INSERT INTO `vietnamese_phrase` VALUES (30, '2021-03-31 04:12:40.000000', NULL, NULL, NULL, 'giành thời gian');
INSERT INTO `vietnamese_phrase` VALUES (31, '2021-03-31 04:12:40.000000', NULL, NULL, NULL, 'rảnh');
INSERT INTO `vietnamese_phrase` VALUES (32, '2021-03-31 04:12:40.000000', NULL, NULL, NULL, 'có thời gian');
INSERT INTO `vietnamese_phrase` VALUES (33, '2021-03-31 04:15:52.000000', NULL, NULL, NULL, 'vui vẻ');
INSERT INTO `vietnamese_phrase` VALUES (34, '2021-03-31 04:15:52.000000', NULL, NULL, NULL, 'vui');
INSERT INTO `vietnamese_phrase` VALUES (35, '2021-03-31 05:03:45.000000', NULL, NULL, NULL, 'cắt tóc');
INSERT INTO `vietnamese_phrase` VALUES (36, '2021-03-31 05:06:31.000000', NULL, NULL, NULL, 'không có thì giờ');
INSERT INTO `vietnamese_phrase` VALUES (37, '2021-03-31 05:06:31.000000', NULL, NULL, NULL, 'không có thời gian');
INSERT INTO `vietnamese_phrase` VALUES (38, '2021-03-31 05:06:31.000000', NULL, NULL, NULL, 'không rảnh');
INSERT INTO `vietnamese_phrase` VALUES (39, '2021-03-31 05:11:23.000000', NULL, NULL, NULL, 'có');
INSERT INTO `vietnamese_phrase` VALUES (40, '2021-03-31 05:16:03.000000', NULL, NULL, NULL, 'phải');
INSERT INTO `vietnamese_phrase` VALUES (41, '2021-03-31 05:21:11.000000', NULL, NULL, NULL, 'có buổi họp');
INSERT INTO `vietnamese_phrase` VALUES (42, '2021-03-31 05:21:11.000000', NULL, NULL, NULL, 'có một buổi họp');
INSERT INTO `vietnamese_phrase` VALUES (43, '2021-03-31 05:21:11.000000', NULL, NULL, NULL, 'họp');
INSERT INTO `vietnamese_phrase` VALUES (44, '2021-03-31 05:21:11.000000', NULL, NULL, NULL, 'có cuộc họp');
INSERT INTO `vietnamese_phrase` VALUES (45, '2021-03-31 05:21:11.000000', NULL, NULL, NULL, 'có một cuộc họp');
INSERT INTO `vietnamese_phrase` VALUES (46, '2021-03-31 05:30:59.000000', NULL, NULL, NULL, 'có hẹn');
INSERT INTO `vietnamese_phrase` VALUES (47, '2021-03-31 05:30:59.000000', NULL, NULL, NULL, 'có cuộc hẹn');
INSERT INTO `vietnamese_phrase` VALUES (48, '2021-03-31 05:30:59.000000', NULL, NULL, NULL, 'hẹn');
INSERT INTO `vietnamese_phrase` VALUES (49, '2021-03-31 05:30:59.000000', NULL, NULL, NULL, 'có một cuộc hẹn');
INSERT INTO `vietnamese_phrase` VALUES (50, '2021-03-31 08:12:41.000000', NULL, NULL, NULL, 'uống một ly');
INSERT INTO `vietnamese_phrase` VALUES (51, '2021-03-31 08:12:41.000000', NULL, NULL, NULL, 'uống');
INSERT INTO `vietnamese_phrase` VALUES (52, '2021-03-31 08:12:41.000000', NULL, NULL, NULL, 'uống đi');
INSERT INTO `vietnamese_phrase` VALUES (53, '2021-03-31 08:14:17.000000', NULL, NULL, NULL, 'bơi');
INSERT INTO `vietnamese_phrase` VALUES (54, '2021-03-31 08:23:45.000000', NULL, NULL, NULL, 'chơi cờ');
INSERT INTO `vietnamese_phrase` VALUES (55, '2021-03-31 08:24:05.000000', NULL, NULL, NULL, 'chơi đá bóng');
INSERT INTO `vietnamese_phrase` VALUES (56, '2021-03-31 08:25:36.000000', NULL, NULL, NULL, 'chơi bài');
INSERT INTO `vietnamese_phrase` VALUES (57, '2021-03-31 08:29:17.000000', NULL, NULL, NULL, 'tổ chức bữa tiệc');
INSERT INTO `vietnamese_phrase` VALUES (58, '2021-03-31 08:29:17.000000', NULL, NULL, NULL, 'có một bữa tiệc');
INSERT INTO `vietnamese_phrase` VALUES (59, '2021-03-31 08:32:18.000000', NULL, NULL, NULL, 'tắm rửa');
INSERT INTO `vietnamese_phrase` VALUES (60, '2021-03-31 08:55:20.000000', NULL, NULL, NULL, 'tham dự');
INSERT INTO `vietnamese_phrase` VALUES (61, '2021-03-31 08:55:20.000000', NULL, NULL, NULL, 'tham gia');
INSERT INTO `vietnamese_phrase` VALUES (62, '2021-03-31 08:55:20.000000', NULL, NULL, NULL, 'đi vào');
INSERT INTO `vietnamese_phrase` VALUES (63, '2021-03-31 08:55:20.000000', NULL, NULL, NULL, 'bước vào');
INSERT INTO `vietnamese_phrase` VALUES (64, '2021-03-31 10:13:39.000000', NULL, NULL, NULL, 'vào');
INSERT INTO `vietnamese_phrase` VALUES (65, '2021-04-01 02:13:54.000000', NULL, NULL, NULL, 'tăng lên');
INSERT INTO `vietnamese_phrase` VALUES (66, '2021-04-01 02:13:54.000000', NULL, NULL, NULL, 'đi lên');
INSERT INTO `vietnamese_phrase` VALUES (67, '2021-04-01 02:13:54.000000', NULL, NULL, NULL, 'lên');
INSERT INTO `vietnamese_phrase` VALUES (68, '2021-04-01 02:16:38.000000', NULL, NULL, NULL, 'vào cuộc');
INSERT INTO `vietnamese_phrase` VALUES (69, '2021-04-01 02:18:47.000000', NULL, NULL, NULL, 'đi sâu vào');
INSERT INTO `vietnamese_phrase` VALUES (70, '2021-04-01 02:20:05.000000', NULL, NULL, NULL, 'tham gia vào');
INSERT INTO `vietnamese_phrase` VALUES (71, '2021-04-01 02:27:22.000000', NULL, NULL, NULL, 'đi');
INSERT INTO `vietnamese_phrase` VALUES (72, '2021-04-01 02:27:22.000000', NULL, NULL, NULL, 'biến mất');
INSERT INTO `vietnamese_phrase` VALUES (73, '2021-04-01 02:27:22.000000', NULL, NULL, NULL, 'tránh xa');
INSERT INTO `vietnamese_phrase` VALUES (74, '2021-04-01 02:27:22.000000', NULL, NULL, NULL, 'cút đi');
INSERT INTO `vietnamese_phrase` VALUES (75, '2021-04-01 02:27:22.000000', NULL, NULL, NULL, 'rời đi');
INSERT INTO `vietnamese_phrase` VALUES (76, '2021-04-01 07:33:58.000000', NULL, NULL, NULL, 'tiệc');
INSERT INTO `vietnamese_phrase` VALUES (77, '2021-04-01 07:33:58.000000', NULL, NULL, NULL, 'bữa tiệc');
INSERT INTO `vietnamese_phrase` VALUES (78, '2021-04-01 07:36:11.000000', NULL, NULL, NULL, 'gặp gỡ bạn bè');
INSERT INTO `vietnamese_phrase` VALUES (79, '2021-04-01 09:56:18.000000', NULL, NULL, NULL, 'tiếp tục');
INSERT INTO `vietnamese_phrase` VALUES (80, '2021-04-01 09:56:53.000000', NULL, NULL, NULL, 'tiếp tục bài học');
INSERT INTO `vietnamese_phrase` VALUES (81, '2021-04-01 10:00:14.000000', NULL, NULL, NULL, 'có mối quan hệ tốt');
INSERT INTO `vietnamese_phrase` VALUES (82, '2021-04-01 10:00:14.000000', NULL, NULL, NULL, 'có quan hệ tốt');
INSERT INTO `vietnamese_phrase` VALUES (83, '2021-04-01 10:02:56.000000', NULL, NULL, NULL, 'tiếp tục video này');
INSERT INTO `vietnamese_phrase` VALUES (84, '2021-04-01 10:14:03.000000', NULL, NULL, NULL, 'tiếp tục mọi thứ');
INSERT INTO `vietnamese_phrase` VALUES (85, '2021-04-01 10:34:39.000000', NULL, NULL, NULL, 'cứ tiếp tục mọi thứ');
INSERT INTO `vietnamese_phrase` VALUES (86, '2021-04-01 10:34:39.000000', NULL, NULL, NULL, 'cứ tiếp tục');
INSERT INTO `vietnamese_phrase` VALUES (87, '2021-04-01 10:35:46.000000', NULL, NULL, NULL, 'tiếp tục với nó');
INSERT INTO `vietnamese_phrase` VALUES (88, '2021-04-01 10:52:32.000000', NULL, NULL, NULL, 'chậm');
INSERT INTO `vietnamese_phrase` VALUES (89, '2021-04-01 10:52:32.000000', NULL, NULL, NULL, 'chậm trễ');
INSERT INTO `vietnamese_phrase` VALUES (90, '2021-04-01 13:53:04.000000', NULL, NULL, NULL, 'tìm kiếm câu trả lời');
INSERT INTO `vietnamese_phrase` VALUES (91, '2021-04-01 14:01:39.000000', NULL, NULL, NULL, 'tra cứu');
INSERT INTO `vietnamese_phrase` VALUES (92, '2021-04-01 14:01:39.000000', NULL, NULL, NULL, 'ngước nhìn');
INSERT INTO `vietnamese_phrase` VALUES (93, '2021-04-01 14:01:39.000000', NULL, NULL, NULL, 'tìm kiếm');
INSERT INTO `vietnamese_phrase` VALUES (94, '2021-04-01 14:01:39.000000', NULL, NULL, NULL, 'nhìn lên');
INSERT INTO `vietnamese_phrase` VALUES (95, '2021-04-01 14:04:42.000000', NULL, NULL, NULL, 'vượt qua lớp băng');
INSERT INTO `vietnamese_phrase` VALUES (96, '2021-04-01 14:07:48.000000', NULL, NULL, NULL, 'bước qua cánh cửa');
INSERT INTO `vietnamese_phrase` VALUES (97, '2021-04-01 14:14:24.000000', NULL, NULL, NULL, 'vượt qua thử thách');
INSERT INTO `vietnamese_phrase` VALUES (98, '2021-04-01 14:17:51.000000', NULL, NULL, NULL, 'vượt qua');
INSERT INTO `vietnamese_phrase` VALUES (99, '2021-04-01 14:17:51.000000', NULL, NULL, NULL, 'kết nối');
INSERT INTO `vietnamese_phrase` VALUES (100, '2021-04-01 14:22:38.000000', NULL, NULL, NULL, 'đi qua');
INSERT INTO `vietnamese_phrase` VALUES (101, '2021-04-01 14:22:57.000000', NULL, NULL, NULL, 'bước qua');
INSERT INTO `vietnamese_phrase` VALUES (102, '2021-04-01 14:23:50.000000', NULL, NULL, NULL, 'vượt qua một tình huống khủng khiếp');
INSERT INTO `vietnamese_phrase` VALUES (103, '2021-04-01 14:30:41.000000', NULL, NULL, NULL, 'cải thiện');
INSERT INTO `vietnamese_phrase` VALUES (104, '2021-04-01 14:32:58.000000', NULL, NULL, NULL, 'thăm');
INSERT INTO `vietnamese_phrase` VALUES (105, '2021-04-01 14:34:14.000000', NULL, NULL, NULL, 'ăn bên ngoài');
INSERT INTO `vietnamese_phrase` VALUES (106, '2021-04-01 14:34:14.000000', NULL, NULL, NULL, 'ăn ngoài');
INSERT INTO `vietnamese_phrase` VALUES (107, '2021-04-01 14:34:14.000000', NULL, NULL, NULL, 'ăn ở ngoài');
INSERT INTO `vietnamese_phrase` VALUES (108, '2021-04-01 14:41:26.000000', NULL, NULL, NULL, 'mang thứ gì đó lại');
INSERT INTO `vietnamese_phrase` VALUES (109, '2021-04-01 14:41:26.000000', NULL, NULL, NULL, 'đem thứ gì đó lại');
INSERT INTO `vietnamese_phrase` VALUES (110, '2021-04-01 14:41:55.000000', NULL, NULL, NULL, 'mang lại kết quả');
INSERT INTO `vietnamese_phrase` VALUES (111, '2021-04-01 14:43:08.000000', NULL, NULL, NULL, 'đem lại kết quả');
INSERT INTO `vietnamese_phrase` VALUES (112, '2021-04-01 14:43:24.000000', NULL, NULL, NULL, 'mang lại sự công bằng');
INSERT INTO `vietnamese_phrase` VALUES (113, '2021-04-01 14:43:24.000000', NULL, NULL, NULL, 'đem lại sự công bằng');
INSERT INTO `vietnamese_phrase` VALUES (114, '2021-04-01 14:44:32.000000', NULL, NULL, NULL, 'mời ai đó');
INSERT INTO `vietnamese_phrase` VALUES (115, '2021-04-01 15:01:46.000000', NULL, NULL, NULL, 'tự chăm sóc mình');
INSERT INTO `vietnamese_phrase` VALUES (116, '2021-04-01 15:02:52.000000', NULL, NULL, NULL, 'chăm sóc');
INSERT INTO `vietnamese_phrase` VALUES (117, '2021-04-01 15:04:24.000000', NULL, NULL, NULL, 'gọi lại cho ai đó');
INSERT INTO `vietnamese_phrase` VALUES (118, '2021-04-01 15:14:26.000000', NULL, NULL, NULL, 'bỏ gói hàng vào');
INSERT INTO `vietnamese_phrase` VALUES (119, '2021-04-01 15:58:14.000000', NULL, NULL, NULL, 'đào');
INSERT INTO `vietnamese_phrase` VALUES (120, '2021-04-01 15:58:14.000000', NULL, NULL, NULL, 'đào bới');
INSERT INTO `vietnamese_phrase` VALUES (121, '2021-04-01 16:01:27.000000', NULL, NULL, NULL, 'đào nơi trú ẩn');
INSERT INTO `vietnamese_phrase` VALUES (122, '2021-04-01 16:04:16.000000', NULL, NULL, NULL, 'tìm hiểu thông tin đó');
INSERT INTO `vietnamese_phrase` VALUES (123, '2021-04-01 16:05:51.000000', NULL, NULL, NULL, 'thoát khỏi cuộc khủng hoảng');
INSERT INTO `vietnamese_phrase` VALUES (124, '2021-04-01 16:05:51.000000', NULL, NULL, NULL, 'giải quyết cuộc khủng hoảng');
INSERT INTO `vietnamese_phrase` VALUES (125, '2021-04-01 16:08:03.000000', NULL, NULL, NULL, 'thoát khỏi những tình huống');
INSERT INTO `vietnamese_phrase` VALUES (126, '2021-04-01 16:08:03.000000', NULL, NULL, NULL, 'thoát khỏi những hoàn cảnh');
INSERT INTO `vietnamese_phrase` VALUES (127, '2021-04-01 16:08:35.000000', NULL, NULL, NULL, 'đào lên khỏi mặt đất');
INSERT INTO `vietnamese_phrase` VALUES (128, '2021-04-01 16:09:12.000000', NULL, NULL, NULL, 'bỏ tiền túi');
INSERT INTO `vietnamese_phrase` VALUES (129, '2021-04-01 16:10:20.000000', NULL, NULL, NULL, 'tìm hiểu điều này');
INSERT INTO `vietnamese_phrase` VALUES (130, '2021-04-01 16:11:16.000000', NULL, NULL, NULL, 'đào được khỏi mặt đất');
INSERT INTO `vietnamese_phrase` VALUES (131, '2021-04-01 16:11:40.000000', NULL, NULL, NULL, 'đào được');
INSERT INTO `vietnamese_phrase` VALUES (132, '2021-04-01 16:14:07.000000', NULL, NULL, NULL, 'thoát khỏi cuộc khủng hoảng tài chính');
INSERT INTO `vietnamese_phrase` VALUES (133, '2021-04-01 16:18:03.000000', NULL, NULL, NULL, 'thoát khỏi');
INSERT INTO `vietnamese_phrase` VALUES (134, '2021-04-02 04:05:05.000000', NULL, NULL, NULL, 'nghe về');
INSERT INTO `vietnamese_phrase` VALUES (135, '2021-04-02 04:05:05.000000', NULL, NULL, NULL, 'nghe');
INSERT INTO `vietnamese_phrase` VALUES (136, '2021-04-02 04:09:15.000000', NULL, NULL, NULL, 'tái chế');
INSERT INTO `vietnamese_phrase` VALUES (137, '2021-04-02 04:12:28.000000', NULL, NULL, NULL, 'nghe nói về');
INSERT INTO `vietnamese_phrase` VALUES (138, '2021-04-02 04:15:59.000000', NULL, NULL, NULL, 'trên bản tin');
INSERT INTO `vietnamese_phrase` VALUES (139, '2021-04-02 04:15:59.000000', NULL, NULL, NULL, 'trên tin tức');
INSERT INTO `vietnamese_phrase` VALUES (140, '2021-04-02 04:16:39.000000', NULL, NULL, NULL, 'tối qua');
INSERT INTO `vietnamese_phrase` VALUES (141, '2021-04-02 04:16:39.000000', NULL, NULL, NULL, 'đêm qua');
INSERT INTO `vietnamese_phrase` VALUES (142, '2021-04-02 04:22:20.000000', NULL, NULL, NULL, 'giữ môi trường sạch sẽ');
INSERT INTO `vietnamese_phrase` VALUES (143, '2021-04-02 04:22:20.000000', NULL, NULL, NULL, 'giữ vệ sinh môi trường');
INSERT INTO `vietnamese_phrase` VALUES (144, '2021-04-02 04:22:54.000000', NULL, NULL, NULL, 'giữ vệ sinh một cái gì đó');
INSERT INTO `vietnamese_phrase` VALUES (145, '2021-04-02 04:22:54.000000', NULL, NULL, NULL, 'giữ một cái gì đó sạch sẽ');
INSERT INTO `vietnamese_phrase` VALUES (146, '2021-04-02 04:24:14.000000', NULL, NULL, NULL, 'giữ vệ sinh đường phố và nơi công cộng');
INSERT INTO `vietnamese_phrase` VALUES (147, '2021-04-02 04:28:37.000000', NULL, NULL, NULL, 'tái chế giấy');
INSERT INTO `vietnamese_phrase` VALUES (148, '2021-04-02 04:28:37.000000', NULL, NULL, NULL, 'giấy tái chế');
INSERT INTO `vietnamese_phrase` VALUES (149, '2021-04-02 04:32:13.000000', NULL, NULL, NULL, 'thay vì');
INSERT INTO `vietnamese_phrase` VALUES (150, '2021-04-02 08:11:43.000000', NULL, NULL, NULL, 'vứt thứ gì đi');
INSERT INTO `vietnamese_phrase` VALUES (151, '2021-04-02 10:27:14.000000', NULL, NULL, NULL, 'nhìn lại cuộc đời mình');
INSERT INTO `vietnamese_phrase` VALUES (152, '2021-04-02 10:27:23.000000', NULL, NULL, NULL, 'nhìn lại');
INSERT INTO `vietnamese_phrase` VALUES (153, '2021-04-02 10:30:36.000000', NULL, NULL, NULL, 'nhìn lại một ngày của mình');
INSERT INTO `vietnamese_phrase` VALUES (154, '2021-04-02 10:33:22.000000', NULL, NULL, NULL, 'nhìn lại lịch sử');
INSERT INTO `vietnamese_phrase` VALUES (155, '2021-04-02 10:42:15.000000', NULL, NULL, NULL, 'lên xe buýt');
INSERT INTO `vietnamese_phrase` VALUES (156, '2021-04-02 10:44:38.000000', NULL, NULL, NULL, 'thân với');
INSERT INTO `vietnamese_phrase` VALUES (157, '2021-04-02 10:46:58.000000', NULL, NULL, NULL, 'đầu hàng');
INSERT INTO `vietnamese_phrase` VALUES (158, '2021-04-02 10:46:58.000000', NULL, NULL, NULL, 'nhượng bộ');
INSERT INTO `vietnamese_phrase` VALUES (159, '2021-04-02 10:53:26.000000', NULL, NULL, NULL, 'tới thăm');
INSERT INTO `vietnamese_phrase` VALUES (160, '2021-04-02 10:53:26.000000', NULL, NULL, NULL, 'ghé thăm');
INSERT INTO `vietnamese_phrase` VALUES (161, '2021-04-02 10:54:12.000000', NULL, NULL, NULL, 'quay lại');
INSERT INTO `vietnamese_phrase` VALUES (162, '2021-04-02 10:57:23.000000', NULL, NULL, NULL, 'đến');
INSERT INTO `vietnamese_phrase` VALUES (163, '2021-04-02 10:57:23.000000', NULL, NULL, NULL, 'tới');
INSERT INTO `vietnamese_phrase` VALUES (164, '2021-04-07 03:01:38.000000', NULL, NULL, NULL, 'tăng âm thanh đài');
INSERT INTO `vietnamese_phrase` VALUES (165, '2021-04-07 03:01:38.000000', NULL, NULL, NULL, 'vặn to đài');
INSERT INTO `vietnamese_phrase` VALUES (166, '2021-04-07 03:02:07.000000', NULL, NULL, NULL, 'tăng âm lượng');
INSERT INTO `vietnamese_phrase` VALUES (167, '2021-04-07 03:02:07.000000', NULL, NULL, NULL, 'tăng âm thanh');
INSERT INTO `vietnamese_phrase` VALUES (168, '2021-04-07 03:03:15.000000', NULL, NULL, NULL, 'tăng độ nhạy');
INSERT INTO `vietnamese_phrase` VALUES (169, '2021-04-07 03:05:12.000000', NULL, NULL, NULL, 'xuất hiện');
INSERT INTO `vietnamese_phrase` VALUES (170, '2021-04-07 03:06:33.000000', NULL, NULL, NULL, 'xuất hiện trong');
INSERT INTO `vietnamese_phrase` VALUES (171, '2021-04-07 03:28:36.000000', NULL, NULL, NULL, 'gửi');
INSERT INTO `vietnamese_phrase` VALUES (172, '2021-04-08 07:11:47.000000', NULL, NULL, NULL, 'có vẻ');
INSERT INTO `vietnamese_phrase` VALUES (173, '2021-04-08 07:11:47.000000', NULL, NULL, NULL, 'bắt gặp');
INSERT INTO `vietnamese_phrase` VALUES (174, '2021-04-08 07:14:43.000000', NULL, NULL, NULL, 'ghi lại tiêu đề');
INSERT INTO `vietnamese_phrase` VALUES (175, '2021-04-08 07:19:12.000000', NULL, NULL, NULL, 'mời một số người bạn');
INSERT INTO `vietnamese_phrase` VALUES (176, '2021-04-08 07:22:11.000000', NULL, NULL, NULL, 'hỏi mọi người');
INSERT INTO `vietnamese_phrase` VALUES (177, '2021-04-08 07:22:11.000000', NULL, NULL, NULL, 'mời');
INSERT INTO `vietnamese_phrase` VALUES (178, '2021-04-08 07:26:02.000000', NULL, NULL, NULL, 'trang điểm cho đôi mắt');
INSERT INTO `vietnamese_phrase` VALUES (179, '2021-04-08 07:27:45.000000', NULL, NULL, NULL, 'chơi hết mình');
INSERT INTO `vietnamese_phrase` VALUES (180, '2021-04-08 07:29:43.000000', NULL, NULL, NULL, 'phát triển khái niệm này');
INSERT INTO `vietnamese_phrase` VALUES (181, '2021-04-08 07:37:22.000000', NULL, NULL, NULL, 'phát triển');
INSERT INTO `vietnamese_phrase` VALUES (182, '2021-04-08 07:37:22.000000', NULL, NULL, NULL, 'thể hiện');
INSERT INTO `vietnamese_phrase` VALUES (183, '2021-04-08 07:37:22.000000', NULL, NULL, NULL, 'nghịch ngợm');
INSERT INTO `vietnamese_phrase` VALUES (184, '2021-04-08 07:39:39.000000', NULL, NULL, NULL, 'phá bỏ bức tường này');
INSERT INTO `vietnamese_phrase` VALUES (185, '2021-04-08 07:41:17.000000', NULL, NULL, NULL, 'chia nhỏ công thức này');
INSERT INTO `vietnamese_phrase` VALUES (186, '2021-04-08 07:42:00.000000', NULL, NULL, NULL, 'phá bỏ');
INSERT INTO `vietnamese_phrase` VALUES (187, '2021-04-08 07:42:00.000000', NULL, NULL, NULL, 'suy sụp');
INSERT INTO `vietnamese_phrase` VALUES (188, '2021-04-08 07:42:00.000000', NULL, NULL, NULL, 'vỡ');
INSERT INTO `vietnamese_phrase` VALUES (189, '2021-04-08 07:42:00.000000', NULL, NULL, NULL, 'phá vỡ');
INSERT INTO `vietnamese_phrase` VALUES (190, '2021-04-08 07:43:39.000000', NULL, NULL, NULL, 'đưa ra các vấn đề');
INSERT INTO `vietnamese_phrase` VALUES (191, '2021-04-08 07:45:41.000000', NULL, NULL, NULL, 'đưa ra');
INSERT INTO `vietnamese_phrase` VALUES (192, '2021-04-08 07:47:13.000000', NULL, NULL, NULL, 'giải quyết');
INSERT INTO `vietnamese_phrase` VALUES (193, '2021-04-08 07:47:13.000000', NULL, NULL, NULL, 'xử lý');
INSERT INTO `vietnamese_phrase` VALUES (194, '2021-04-08 07:49:41.000000', NULL, NULL, NULL, 'cư xử tệ bạc với ai đó');
INSERT INTO `vietnamese_phrase` VALUES (195, '2021-04-08 07:50:02.000000', NULL, NULL, NULL, 'quậy phá');
INSERT INTO `vietnamese_phrase` VALUES (196, '2021-04-08 07:50:02.000000', NULL, NULL, NULL, 'làm rối tung');
INSERT INTO `vietnamese_phrase` VALUES (197, '2021-04-08 07:52:31.000000', NULL, NULL, NULL, 'hủy bỏ');
INSERT INTO `vietnamese_phrase` VALUES (198, '2021-04-08 07:53:05.000000', NULL, NULL, NULL, 'trì hoãn');
INSERT INTO `vietnamese_phrase` VALUES (199, '2021-04-08 07:53:25.000000', NULL, NULL, NULL, 'trì hoãn việc gì');
INSERT INTO `vietnamese_phrase` VALUES (200, '2021-04-08 07:54:37.000000', NULL, NULL, NULL, 'hoãn');
INSERT INTO `vietnamese_phrase` VALUES (201, '2021-04-08 07:54:55.000000', NULL, NULL, NULL, 'chậm việc gì');
INSERT INTO `vietnamese_phrase` VALUES (202, '2021-04-08 07:54:55.000000', NULL, NULL, NULL, 'hoãn việc gì');
INSERT INTO `vietnamese_phrase` VALUES (203, '2021-04-08 07:57:38.000000', NULL, NULL, NULL, 'tìm');
INSERT INTO `vietnamese_phrase` VALUES (204, '2021-04-08 08:02:17.000000', NULL, NULL, NULL, 'không hoạt động');
INSERT INTO `vietnamese_phrase` VALUES (205, '2021-04-08 08:03:00.000000', NULL, NULL, NULL, 'hỏng');
INSERT INTO `vietnamese_phrase` VALUES (206, '2021-04-08 08:03:36.000000', NULL, NULL, NULL, 'đề cập');
INSERT INTO `vietnamese_phrase` VALUES (207, '2021-04-08 08:03:45.000000', NULL, NULL, NULL, 'đề cập các vấn đề');
INSERT INTO `vietnamese_phrase` VALUES (208, '2021-04-08 08:04:46.000000', NULL, NULL, NULL, 'đề cập đến');
INSERT INTO `vietnamese_phrase` VALUES (209, '2021-04-08 08:08:00.000000', NULL, NULL, NULL, 'gợi nhớ lại');
INSERT INTO `vietnamese_phrase` VALUES (210, '2021-04-09 03:51:06.000000', NULL, NULL, NULL, 'mong');
INSERT INTO `vietnamese_phrase` VALUES (211, '2021-04-09 03:51:06.000000', NULL, NULL, NULL, 'mong đợi');
INSERT INTO `vietnamese_phrase` VALUES (212, '2021-04-09 03:53:10.000000', NULL, NULL, NULL, 'đi chơi với');
INSERT INTO `vietnamese_phrase` VALUES (213, '2021-04-09 03:53:36.000000', NULL, NULL, NULL, 'đi chơi cùng');
INSERT INTO `vietnamese_phrase` VALUES (214, '2021-04-09 03:55:07.000000', NULL, NULL, NULL, 'khinh');
INSERT INTO `vietnamese_phrase` VALUES (215, '2021-04-09 03:55:34.000000', NULL, NULL, NULL, 'coi thường');
INSERT INTO `vietnamese_phrase` VALUES (216, '2021-04-09 03:56:50.000000', NULL, NULL, NULL, 'bỏ');
INSERT INTO `vietnamese_phrase` VALUES (217, '2021-04-09 03:56:50.000000', NULL, NULL, NULL, 'từ bỏ');
INSERT INTO `vietnamese_phrase` VALUES (218, '2021-04-09 04:10:05.000000', NULL, NULL, NULL, 'đưa ra một kế hoạch');
INSERT INTO `vietnamese_phrase` VALUES (219, '2021-04-09 04:10:32.000000', NULL, NULL, NULL, 'nghĩ ra ý tưởng');
INSERT INTO `vietnamese_phrase` VALUES (220, '2021-04-09 04:10:32.000000', NULL, NULL, NULL, 'đưa ra ý tưởng');
INSERT INTO `vietnamese_phrase` VALUES (221, '2021-04-09 04:13:02.000000', NULL, NULL, NULL, 'đưa ra các giải pháp');
INSERT INTO `vietnamese_phrase` VALUES (222, '2021-04-09 04:14:43.000000', NULL, NULL, NULL, 'đưa ra một ý tưởng');
INSERT INTO `vietnamese_phrase` VALUES (223, '2021-04-09 04:14:43.000000', NULL, NULL, NULL, 'nghĩ ra một ý tưởng');
INSERT INTO `vietnamese_phrase` VALUES (224, '2021-04-09 04:16:53.000000', NULL, NULL, NULL, 'không bị trừng phạt');
INSERT INTO `vietnamese_phrase` VALUES (225, '2021-04-09 04:16:53.000000', NULL, NULL, NULL, 'không bị phạt');
INSERT INTO `vietnamese_phrase` VALUES (226, '2021-04-09 04:23:54.000000', NULL, NULL, NULL, 'thân thiết');
INSERT INTO `vietnamese_phrase` VALUES (227, '2021-04-09 04:24:44.000000', NULL, NULL, NULL, 'giao thứ gì đó');
INSERT INTO `vietnamese_phrase` VALUES (228, '2021-04-09 04:27:34.000000', NULL, NULL, NULL, 'cởi chiếc áo này');
INSERT INTO `vietnamese_phrase` VALUES (229, '2021-04-09 04:28:18.000000', NULL, NULL, NULL, 'bắt chước');
INSERT INTO `vietnamese_phrase` VALUES (230, '2021-04-09 04:28:18.000000', NULL, NULL, NULL, 'được nghỉ');
INSERT INTO `vietnamese_phrase` VALUES (231, '2021-04-09 04:28:18.000000', NULL, NULL, NULL, 'cất cánh');
INSERT INTO `vietnamese_phrase` VALUES (232, '2021-04-09 04:28:18.000000', NULL, NULL, NULL, 'cởi');
INSERT INTO `vietnamese_phrase` VALUES (233, '2021-04-09 04:30:02.000000', NULL, NULL, NULL, 'cởi giày');
INSERT INTO `vietnamese_phrase` VALUES (234, '2021-04-09 04:31:57.000000', NULL, NULL, NULL, 'tới đúng giờ');
INSERT INTO `vietnamese_phrase` VALUES (235, '2021-04-09 04:31:57.000000', NULL, NULL, NULL, 'đến đúng giờ');
INSERT INTO `vietnamese_phrase` VALUES (236, '2021-04-09 06:41:52.000000', NULL, NULL, NULL, 'chia');
INSERT INTO `vietnamese_phrase` VALUES (237, '2021-04-09 06:41:52.000000', NULL, NULL, NULL, 'cắt');
INSERT INTO `vietnamese_phrase` VALUES (238, '2021-04-09 06:47:21.000000', NULL, NULL, NULL, 'tắt đèn');
INSERT INTO `vietnamese_phrase` VALUES (239, '2021-04-09 06:47:39.000000', NULL, NULL, NULL, 'tắt tv');
INSERT INTO `vietnamese_phrase` VALUES (240, '2021-04-12 04:34:48.000000', NULL, NULL, NULL, 'vượt qua nỗi sợ');
INSERT INTO `vietnamese_phrase` VALUES (241, '2021-04-12 04:34:48.000000', NULL, NULL, NULL, 'vượt qua nỗi sợ hãi');
INSERT INTO `vietnamese_phrase` VALUES (242, '2021-04-12 04:35:59.000000', NULL, NULL, NULL, 'tự mình vượt qua');
INSERT INTO `vietnamese_phrase` VALUES (243, '2021-04-12 06:55:30.000000', NULL, NULL, NULL, 'tiếp tục cố gắng');
INSERT INTO `vietnamese_phrase` VALUES (244, '2021-04-12 06:55:56.000000', NULL, NULL, NULL, 'cứ tiếp tục đi');
INSERT INTO `vietnamese_phrase` VALUES (245, '2021-04-12 07:00:21.000000', NULL, NULL, NULL, 'tiếp tục đi');
INSERT INTO `vietnamese_phrase` VALUES (246, '2021-04-12 07:04:15.000000', NULL, NULL, NULL, 'đón ai đó lúc');
INSERT INTO `vietnamese_phrase` VALUES (247, '2021-04-12 07:04:59.000000', NULL, NULL, NULL, 'đỡ ai đó dậy');
INSERT INTO `vietnamese_phrase` VALUES (248, '2021-04-12 07:06:55.000000', NULL, NULL, NULL, 'nhặt thứ gì đó lên');
INSERT INTO `vietnamese_phrase` VALUES (249, '2021-04-12 07:06:55.000000', NULL, NULL, NULL, 'lấy thứ gì đó');
INSERT INTO `vietnamese_phrase` VALUES (250, '2021-04-12 07:07:05.000000', NULL, NULL, NULL, 'đón ai đó');
INSERT INTO `vietnamese_phrase` VALUES (251, '2021-04-12 07:11:03.000000', NULL, NULL, NULL, 'làm trái');
INSERT INTO `vietnamese_phrase` VALUES (252, '2021-04-12 07:11:03.000000', NULL, NULL, NULL, 'đi ngược lại');
INSERT INTO `vietnamese_phrase` VALUES (253, '2021-04-12 07:11:03.000000', NULL, NULL, NULL, 'làm ngược lại');
INSERT INTO `vietnamese_phrase` VALUES (254, '2021-04-12 07:21:05.000000', NULL, NULL, NULL, 'ngược đời');
INSERT INTO `vietnamese_phrase` VALUES (255, '2021-04-12 07:21:05.000000', NULL, NULL, NULL, 'làm việc ngược đời');
INSERT INTO `vietnamese_phrase` VALUES (256, '2021-04-12 08:21:55.000000', NULL, NULL, NULL, 'bị loại khỏi các cuộc họp đưa ra quyết định');
INSERT INTO `vietnamese_phrase` VALUES (257, '2021-04-12 08:25:01.000000', NULL, NULL, NULL, 'loại bỏ những từ');
INSERT INTO `vietnamese_phrase` VALUES (258, '2021-04-12 08:25:01.000000', NULL, NULL, NULL, 'bỏ hết những từ');
INSERT INTO `vietnamese_phrase` VALUES (259, '2021-04-12 08:26:03.000000', NULL, NULL, NULL, 'loại bỏ');
INSERT INTO `vietnamese_phrase` VALUES (260, '2021-04-12 08:26:03.000000', NULL, NULL, NULL, 'cắt bỏ');
INSERT INTO `vietnamese_phrase` VALUES (261, '2021-04-12 08:26:03.000000', NULL, NULL, NULL, 'loại khỏi');
INSERT INTO `vietnamese_phrase` VALUES (262, '2021-04-12 08:31:19.000000', NULL, NULL, NULL, 'ngừng hoạt động');
INSERT INTO `vietnamese_phrase` VALUES (263, '2021-04-12 08:32:52.000000', NULL, NULL, NULL, 'ngừng làm việc');
INSERT INTO `vietnamese_phrase` VALUES (264, '2021-04-12 08:37:33.000000', NULL, NULL, NULL, 'cố gắng hết sức có thể');
INSERT INTO `vietnamese_phrase` VALUES (265, '2021-04-12 08:46:48.000000', NULL, NULL, NULL, 'trải qua');
INSERT INTO `vietnamese_phrase` VALUES (266, '2021-04-12 08:46:48.000000', NULL, NULL, NULL, 'bay qua');
INSERT INTO `vietnamese_phrase` VALUES (267, '2021-04-12 08:46:48.000000', NULL, NULL, NULL, 'qua');
INSERT INTO `vietnamese_phrase` VALUES (268, '2021-04-12 08:46:48.000000', NULL, NULL, NULL, 'lướt qua');
INSERT INTO `vietnamese_phrase` VALUES (269, '2021-04-12 08:48:31.000000', NULL, NULL, NULL, 'trôi qua');
INSERT INTO `vietnamese_phrase` VALUES (270, '2021-04-12 08:48:31.000000', NULL, NULL, NULL, 'xem qua');
INSERT INTO `vietnamese_phrase` VALUES (271, '2021-04-12 08:52:33.000000', NULL, NULL, NULL, 'gạt bỏ thứ gì đó');
INSERT INTO `vietnamese_phrase` VALUES (272, '2021-04-12 08:52:33.000000', NULL, NULL, NULL, 'gạt thứ gì đó sang một bên');
INSERT INTO `vietnamese_phrase` VALUES (273, '2021-04-12 09:06:30.000000', NULL, NULL, NULL, 'không quan tâm thứ gì đó');
INSERT INTO `vietnamese_phrase` VALUES (274, '2021-04-12 09:12:41.000000', NULL, NULL, NULL, 'yêu');
INSERT INTO `vietnamese_phrase` VALUES (275, '2021-04-12 09:12:41.000000', NULL, NULL, NULL, 'yêu thích');
INSERT INTO `vietnamese_phrase` VALUES (276, '2021-04-12 09:12:41.000000', NULL, NULL, NULL, 'phải lòng');
INSERT INTO `vietnamese_phrase` VALUES (277, '2021-04-12 09:14:34.000000', NULL, NULL, NULL, 'tìm ra những gì');
INSERT INTO `vietnamese_phrase` VALUES (278, '2021-04-12 09:14:54.000000', NULL, NULL, NULL, 'tìm ra chính xác những gì');
INSERT INTO `vietnamese_phrase` VALUES (279, '2021-04-12 09:15:28.000000', NULL, NULL, NULL, 'hiểu những gì');
INSERT INTO `vietnamese_phrase` VALUES (280, '2021-04-12 09:15:40.000000', NULL, NULL, NULL, 'hiểu chính xác những gì');
INSERT INTO `vietnamese_phrase` VALUES (281, '2021-04-12 09:20:56.000000', NULL, NULL, NULL, 'chứng minh rằng');
INSERT INTO `vietnamese_phrase` VALUES (282, '2021-04-12 09:20:56.000000', NULL, NULL, NULL, 'chỉ ra rằng');
INSERT INTO `vietnamese_phrase` VALUES (283, '2021-04-12 09:30:39.000000', NULL, NULL, NULL, 'giải thích');
INSERT INTO `vietnamese_phrase` VALUES (284, '2021-04-12 09:30:39.000000', NULL, NULL, NULL, 'theo dõi');
INSERT INTO `vietnamese_phrase` VALUES (285, '2021-04-12 09:30:39.000000', NULL, NULL, NULL, 'đuổi theo');
INSERT INTO `vietnamese_phrase` VALUES (286, '2021-04-12 09:34:30.000000', NULL, NULL, NULL, 'bỏ thứ gì đó');
INSERT INTO `vietnamese_phrase` VALUES (287, '2021-04-12 09:37:31.000000', NULL, NULL, NULL, 'bỏ qua');
INSERT INTO `vietnamese_phrase` VALUES (288, '2021-04-12 09:42:36.000000', NULL, NULL, NULL, 'đưa ra báo cáo');
INSERT INTO `vietnamese_phrase` VALUES (289, '2021-04-12 09:43:55.000000', NULL, NULL, NULL, 'đưa ra tuyên bố');
INSERT INTO `vietnamese_phrase` VALUES (290, '2021-04-12 09:47:51.000000', NULL, NULL, NULL, 'ngừng tìm kiếm');
INSERT INTO `vietnamese_phrase` VALUES (291, '2021-04-12 09:47:51.000000', NULL, NULL, NULL, 'dừng tìm kiếm');
INSERT INTO `vietnamese_phrase` VALUES (292, '2021-04-12 09:48:22.000000', NULL, NULL, NULL, 'hủy bỏ đám cưới');
INSERT INTO `vietnamese_phrase` VALUES (293, '2021-04-12 09:50:26.000000', NULL, NULL, NULL, 'ngừng');
INSERT INTO `vietnamese_phrase` VALUES (294, '2021-04-12 09:50:26.000000', NULL, NULL, NULL, 'gọi tắt là');
INSERT INTO `vietnamese_phrase` VALUES (295, '2021-04-12 09:50:26.000000', NULL, NULL, NULL, 'dừng');
INSERT INTO `vietnamese_phrase` VALUES (296, '2021-04-12 09:55:25.000000', NULL, NULL, NULL, 'kết thúc sau');
INSERT INTO `vietnamese_phrase` VALUES (297, '2021-04-12 09:55:25.000000', NULL, NULL, NULL, 'chấm dứt sau');
INSERT INTO `vietnamese_phrase` VALUES (298, '2021-04-12 09:56:23.000000', NULL, NULL, NULL, 'tạm dừng');
INSERT INTO `vietnamese_phrase` VALUES (299, '2021-04-12 09:59:46.000000', NULL, NULL, NULL, 'lặn xuống');
INSERT INTO `vietnamese_phrase` VALUES (300, '2021-04-12 09:59:46.000000', NULL, NULL, NULL, 'xuống');
INSERT INTO `vietnamese_phrase` VALUES (301, '2021-04-12 09:59:46.000000', NULL, NULL, NULL, 'đi xuống');
INSERT INTO `vietnamese_phrase` VALUES (302, '2021-04-12 09:59:46.000000', NULL, NULL, NULL, 'giảm xuống');
INSERT INTO `vietnamese_phrase` VALUES (303, '2021-04-12 10:02:44.000000', NULL, NULL, NULL, 'đi theo con đường này');
INSERT INTO `vietnamese_phrase` VALUES (304, '2021-04-12 10:05:21.000000', NULL, NULL, NULL, 'trốn thoát');
INSERT INTO `vietnamese_phrase` VALUES (305, '2021-04-13 08:08:08.000000', NULL, NULL, NULL, 'bảo quản');
INSERT INTO `vietnamese_phrase` VALUES (306, '2021-04-13 08:08:08.000000', NULL, NULL, NULL, 'cất giữ');
INSERT INTO `vietnamese_phrase` VALUES (307, '2021-04-13 08:08:08.000000', NULL, NULL, NULL, 'lưu trữ');
INSERT INTO `vietnamese_phrase` VALUES (308, '2021-04-15 07:10:13.000000', NULL, NULL, NULL, 'khác biệt chính');
INSERT INTO `vietnamese_phrase` VALUES (309, '2021-04-15 07:10:19.000000', NULL, NULL, NULL, 'những khác biệt chính');
INSERT INTO `vietnamese_phrase` VALUES (310, '2021-04-15 07:12:07.000000', NULL, NULL, NULL, 'bộ nhớ heap');
INSERT INTO `vietnamese_phrase` VALUES (311, '2021-04-15 07:13:26.000000', NULL, NULL, NULL, 'kiểu dữ liệu trừu tượng');
INSERT INTO `vietnamese_phrase` VALUES (312, '2021-04-15 07:13:36.000000', NULL, NULL, NULL, 'kiểu dữ liệu');
INSERT INTO `vietnamese_phrase` VALUES (313, '2021-04-15 07:15:09.000000', NULL, NULL, NULL, 'vùng đặc biệt');
INSERT INTO `vietnamese_phrase` VALUES (314, '2021-04-15 07:15:09.000000', NULL, NULL, NULL, 'khu vực đặc biệt');
INSERT INTO `vietnamese_phrase` VALUES (315, '2021-04-15 07:16:15.000000', NULL, NULL, NULL, 'bộ nhớ truy cập ngẫu nhiên');
INSERT INTO `vietnamese_phrase` VALUES (316, '2021-04-15 07:16:15.000000', NULL, NULL, NULL, 'bộ nhớ truy cập tạm thời');
INSERT INTO `vietnamese_phrase` VALUES (317, '2021-04-15 07:18:13.000000', NULL, NULL, NULL, 'cái gọi là');
INSERT INTO `vietnamese_phrase` VALUES (318, '2021-04-15 07:22:26.000000', NULL, NULL, NULL, 'đặt ra một câu hỏi');
INSERT INTO `vietnamese_phrase` VALUES (319, '2021-04-15 07:22:26.000000', NULL, NULL, NULL, 'đặt ra câu hỏi');
INSERT INTO `vietnamese_phrase` VALUES (320, '2021-04-15 07:27:12.000000', NULL, NULL, NULL, 'hãy theo dõi');
INSERT INTO `vietnamese_phrase` VALUES (321, '2021-04-15 07:31:47.000000', NULL, NULL, NULL, 'điểm mấu chốt');
INSERT INTO `vietnamese_phrase` VALUES (322, '2021-04-15 07:31:47.000000', NULL, NULL, NULL, 'vấn đề mấu chốt');
INSERT INTO `vietnamese_phrase` VALUES (323, '2021-04-15 07:40:10.000000', NULL, NULL, NULL, 'loại');
INSERT INTO `vietnamese_phrase` VALUES (324, '2021-04-15 07:45:53.000000', NULL, NULL, NULL, 'mỗi khi');
INSERT INTO `vietnamese_phrase` VALUES (325, '2021-04-15 07:45:53.000000', NULL, NULL, NULL, 'mỗi lần');
INSERT INTO `vietnamese_phrase` VALUES (326, '2021-04-15 08:26:50.000000', NULL, NULL, NULL, 'lén lút');
INSERT INTO `vietnamese_phrase` VALUES (327, '2021-04-15 08:26:50.000000', NULL, NULL, NULL, 'giấm giúi');
INSERT INTO `vietnamese_phrase` VALUES (328, '2021-04-15 08:30:31.000000', NULL, NULL, NULL, 'đi vào chi tiết');
INSERT INTO `vietnamese_phrase` VALUES (329, '2021-04-15 08:30:31.000000', NULL, NULL, NULL, 'chi tiết');
INSERT INTO `vietnamese_phrase` VALUES (330, '2021-04-15 08:30:31.000000', NULL, NULL, NULL, 'đi sâu vào vấn đề');
INSERT INTO `vietnamese_phrase` VALUES (331, '2021-04-15 08:36:33.000000', NULL, NULL, NULL, 'xét về mặt');
INSERT INTO `vietnamese_phrase` VALUES (332, '2021-04-15 08:36:33.000000', NULL, NULL, NULL, 'về mặt');
INSERT INTO `vietnamese_phrase` VALUES (333, '2021-04-15 09:00:27.000000', NULL, NULL, NULL, 'muốn');
INSERT INTO `vietnamese_phrase` VALUES (334, '2021-04-15 10:01:51.000000', NULL, NULL, NULL, 'một vài tình huống');
INSERT INTO `vietnamese_phrase` VALUES (335, '2021-04-15 10:01:51.000000', NULL, NULL, NULL, 'một số tình huống');
INSERT INTO `vietnamese_phrase` VALUES (336, '2021-04-15 10:02:58.000000', NULL, NULL, NULL, 'khá tiện dụng');
INSERT INTO `vietnamese_phrase` VALUES (337, '2021-04-15 10:11:00.000000', NULL, NULL, NULL, 'nói ngắn gọn');
INSERT INTO `vietnamese_phrase` VALUES (338, '2021-04-15 10:14:22.000000', NULL, NULL, NULL, 'phía sau');
INSERT INTO `vietnamese_phrase` VALUES (339, '2021-04-15 10:14:22.000000', NULL, NULL, NULL, 'đằng sau');
INSERT INTO `vietnamese_phrase` VALUES (340, '2021-04-15 10:16:27.000000', NULL, NULL, NULL, 'biến thành');
INSERT INTO `vietnamese_phrase` VALUES (341, '2021-04-15 10:17:32.000000', NULL, NULL, NULL, 'bên trong');
INSERT INTO `vietnamese_phrase` VALUES (342, '2021-04-15 10:28:10.000000', NULL, NULL, NULL, 'điều quan trọng là');
INSERT INTO `vietnamese_phrase` VALUES (343, '2021-04-15 10:29:01.000000', NULL, NULL, NULL, 'điều đó quan trọng đối với');
INSERT INTO `vietnamese_phrase` VALUES (344, '2021-04-15 10:29:01.000000', NULL, NULL, NULL, 'nó quan trọng với');
INSERT INTO `vietnamese_phrase` VALUES (345, '2021-04-15 10:34:42.000000', NULL, NULL, NULL, 'bất kể');
INSERT INTO `vietnamese_phrase` VALUES (346, '2021-04-15 10:34:42.000000', NULL, NULL, NULL, 'dù');
INSERT INTO `vietnamese_phrase` VALUES (347, '2021-04-15 10:34:42.000000', NULL, NULL, NULL, 'dù cho');
INSERT INTO `vietnamese_phrase` VALUES (348, '2021-04-15 10:55:56.000000', NULL, NULL, NULL, 'bộ xử lý trung tâm');
INSERT INTO `vietnamese_phrase` VALUES (349, '2021-04-15 10:56:12.000000', NULL, NULL, NULL, 'như');
INSERT INTO `vietnamese_phrase` VALUES (350, '2021-04-15 10:56:12.000000', NULL, NULL, NULL, 'cũng như');
INSERT INTO `vietnamese_phrase` VALUES (351, '2021-05-04 03:32:49.000000', NULL, NULL, NULL, 'được');
INSERT INTO `vietnamese_phrase` VALUES (352, '2021-05-04 08:26:49.000000', NULL, NULL, NULL, 'nóng hổi');
INSERT INTO `vietnamese_phrase` VALUES (353, '2021-05-04 08:26:49.000000', NULL, NULL, NULL, 'ấm áp');
INSERT INTO `vietnamese_phrase` VALUES (354, '2021-05-04 08:26:49.000000', NULL, NULL, NULL, 'ấm');
INSERT INTO `vietnamese_phrase` VALUES (355, '2021-05-06 03:32:22.000000', NULL, NULL, NULL, 'nhiệm vụ');
INSERT INTO `vietnamese_phrase` VALUES (356, '2021-05-06 03:33:00.000000', NULL, NULL, NULL, 'trình lên lịch cho tiến trình');
INSERT INTO `vietnamese_phrase` VALUES (357, '2021-05-06 03:33:09.000000', NULL, NULL, NULL, 'tiến trình');
INSERT INTO `vietnamese_phrase` VALUES (358, '2021-05-06 03:33:31.000000', NULL, NULL, NULL, 'thời gian');
INSERT INTO `vietnamese_phrase` VALUES (359, '2021-05-06 03:37:54.000000', NULL, NULL, NULL, 'trạng thái có thể chạy được');
INSERT INTO `vietnamese_phrase` VALUES (360, '2021-05-06 03:38:04.000000', NULL, NULL, NULL, 'tiến trình mới');
INSERT INTO `vietnamese_phrase` VALUES (361, '2021-05-06 03:39:56.000000', NULL, NULL, NULL, 'vứt rác bừa bãi');
INSERT INTO `vietnamese_phrase` VALUES (362, '2021-05-06 03:40:09.000000', NULL, NULL, NULL, 'vứt thứ gì đó bừa bãi');
INSERT INTO `vietnamese_phrase` VALUES (363, '2021-05-06 03:41:16.000000', NULL, NULL, NULL, 'phá hỏng');
INSERT INTO `vietnamese_phrase` VALUES (364, '2021-05-06 03:41:16.000000', NULL, NULL, NULL, 'làm hỏng');
INSERT INTO `vietnamese_phrase` VALUES (365, '2021-05-06 03:48:11.000000', NULL, NULL, NULL, 'cá nhân');
INSERT INTO `vietnamese_phrase` VALUES (366, '2021-05-06 03:48:11.000000', NULL, NULL, NULL, 'đơn');
INSERT INTO `vietnamese_phrase` VALUES (367, '2021-05-06 03:48:11.000000', NULL, NULL, NULL, 'riêng');
INSERT INTO `vietnamese_phrase` VALUES (368, '2021-05-06 03:48:26.000000', NULL, NULL, NULL, 'tiến trình riêng');
INSERT INTO `vietnamese_phrase` VALUES (369, '2021-05-06 03:48:26.000000', NULL, NULL, NULL, 'tiến trình đơn');
INSERT INTO `vietnamese_phrase` VALUES (370, '2021-05-06 03:49:03.000000', NULL, NULL, NULL, 'phân chia');
INSERT INTO `vietnamese_phrase` VALUES (371, '2021-05-06 03:49:03.000000', NULL, NULL, NULL, 'phân bổ');
INSERT INTO `vietnamese_phrase` VALUES (372, '2021-05-06 03:49:28.000000', NULL, NULL, NULL, 'khoảng thời gian');
INSERT INTO `vietnamese_phrase` VALUES (373, '2021-05-06 03:52:24.000000', NULL, NULL, NULL, 'trạng thái tắc nghẽn');
INSERT INTO `vietnamese_phrase` VALUES (374, '2021-05-06 03:52:40.000000', NULL, NULL, NULL, 'trạng thái đang chờ');
INSERT INTO `vietnamese_phrase` VALUES (375, '2021-05-06 03:52:40.000000', NULL, NULL, NULL, 'trạng thái chờ');
INSERT INTO `vietnamese_phrase` VALUES (376, '2021-05-06 03:53:04.000000', NULL, NULL, NULL, 'tạm thời');
INSERT INTO `vietnamese_phrase` VALUES (377, '2021-05-06 03:54:29.000000', NULL, NULL, NULL, 'các trạng thái');
INSERT INTO `vietnamese_phrase` VALUES (378, '2021-05-06 04:06:54.000000', NULL, NULL, NULL, 'chờ hẹn giờ');
INSERT INTO `vietnamese_phrase` VALUES (379, '2021-05-06 04:20:25.000000', NULL, NULL, NULL, 'hết thời gian');
INSERT INTO `vietnamese_phrase` VALUES (380, '2021-05-06 04:20:25.000000', NULL, NULL, NULL, 'thời gian chờ');
INSERT INTO `vietnamese_phrase` VALUES (381, '2021-05-06 06:59:47.000000', NULL, NULL, NULL, 'từ khóa');
INSERT INTO `vietnamese_phrase` VALUES (382, '2021-05-06 07:00:19.000000', NULL, NULL, NULL, 'cách truyền thống');
INSERT INTO `vietnamese_phrase` VALUES (383, '2021-05-06 07:00:19.000000', NULL, NULL, NULL, 'cách làm thông thường');
INSERT INTO `vietnamese_phrase` VALUES (384, '2021-05-06 07:00:19.000000', NULL, NULL, NULL, 'cách làm truyền thống');
INSERT INTO `vietnamese_phrase` VALUES (385, '2021-05-06 07:00:19.000000', NULL, NULL, NULL, 'cách thông thường');
INSERT INTO `vietnamese_phrase` VALUES (386, '2021-05-06 07:01:33.000000', NULL, NULL, NULL, 'có được');
INSERT INTO `vietnamese_phrase` VALUES (387, '2021-05-06 07:01:33.000000', NULL, NULL, NULL, 'đạt được');

SET FOREIGN_KEY_CHECKS = 1;

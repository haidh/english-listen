package shop.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import shop.entity.Role;

import java.util.List;

@Repository
public interface RoleRepository extends GenericRepository<Role, Long> {
    @Query("SELECT CASE WHEN COUNT(r) > 0 THEN true ELSE false END FROM Role r WHERE r.name = :name")
    Boolean checkExistedByName(@Param("name") String name);

    @Query("SELECT CASE WHEN COUNT(r) > 0 THEN true ELSE false END FROM Role r WHERE r.id = :id")
    Boolean checkExistedById(@Param("id") Long id);

    @Query("SELECT CASE WHEN COUNT(r) > 0 THEN true ELSE false END FROM Role r WHERE r.name = :name AND r.id <> :id")
    Boolean checkExistedNotById(@Param("name") String name, @Param("id") Long id);

    void deleteByIdIn(List<Long> listId);

    Role findRoleById(Long id);
}

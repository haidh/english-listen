package shop.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import shop.entity.Operation;
import shop.entity.Permission;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public interface OperationRepository extends GenericRepository<Operation, Long> {
    @Query("FROM Operation o WHERE o.operationKey IN :operations")
    List<Operation> findOperationIN(List<String> operations);
}

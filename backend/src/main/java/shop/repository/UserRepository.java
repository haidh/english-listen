package shop.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import shop.entity.User;

import java.util.List;

@Repository
public interface UserRepository extends GenericRepository<User, Long> {
    User findByEmail(String email);

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END from User u WHERE u.username = :username")
    Boolean checkUsernameExisted(@Param("username") String username);

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END from User u WHERE u.email = :email")
    Boolean checkEmailExisted(@Param("email") String email);

    User findByUsername(String username);

    User findUserById(Long id);

    Page<User> findByUsernameLikeAndFullNameLikeAndEmailLike(String username, String fullName, String email, Pageable pageable);

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END FROM User u WHERE u.username = :username AND u.id <> :id")
    Boolean checkExistedNotById(@Param("username") String username, @Param("id") Long id);

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END FROM User u WHERE u.email = :email AND u.id <> :id")
    Boolean checkEmailExistedNotById(@Param("email") String email, @Param("id") Long id);

    void deleteByIdIn(List<Long> listId);
}

package shop.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import shop.entity.GenericEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

//@NoRepositoryBean
@Repository
public interface GenericRepository<T extends GenericEntity, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor {

}

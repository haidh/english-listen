package shop.repository;

import org.springframework.stereotype.Repository;
import shop.entity.Episode;
import shop.entity.File;

@Repository
public interface FileRepository extends GenericRepository<File, Long> {

}

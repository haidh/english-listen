package shop.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import shop.entity.Episode;

import java.util.List;

@Repository
public interface EpisodeRepository extends GenericRepository<Episode, Long> {
    @Query(value = "SELECT * FROM episode INNER JOIN movie_season_episode ON episode.id = movie_season_episode.episode_id " +
            "INNER JOIN season ON movie_season_episode.season_id = season.id " +
            "INNER JOIN movie ON movie_season_episode.movie_id = movie.id " +
            "WHERE movie.id = ?1 AND season.id = ?2", nativeQuery = true)
    List<Episode> getEpisodeByMovieSeason(Long movieId, Long seasonId);
}

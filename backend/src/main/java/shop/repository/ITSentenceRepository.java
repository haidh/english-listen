package shop.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import shop.entity.ITSentence;
import shop.entity.Sentence;

import java.util.List;

@Repository
public interface ITSentenceRepository extends GenericRepository<ITSentence, Long> {
    @Query("SELECT CASE WHEN COUNT(s) > 0 THEN true ELSE false END FROM Sentence s WHERE s.sentence = ?1")
    Boolean checkExistedBySentence(String sentence);

    @Query("SELECT CASE WHEN COUNT(s) > 0 THEN true ELSE false END FROM Sentence s WHERE s.sentence = ?1 AND s.id <> ?2")
    Boolean checkExistedNotById(String name, Long id);

    void deleteByIdIn(List<Long> listId);
}

package shop.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import shop.entity.Permission;

import java.util.List;

@Repository
public interface PermissionRepository extends GenericRepository<Permission, Long> {
    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM Permission p WHERE p.name = :name")
    Boolean checkExistedByName(@Param("name") String name);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM Permission p WHERE p.id = :id")
    Boolean checkExistedById(@Param("id") Long id);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM Permission p WHERE p.name = :name AND p.id <> :id")
    Boolean checkExistedNotById(@Param("name") String name, @Param("id") Long id);

    void deleteByIdIn(List<Long> listPermissionId);
}

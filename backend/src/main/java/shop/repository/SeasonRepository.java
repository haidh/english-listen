package shop.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import shop.entity.Season;

import java.util.List;

@Repository
public interface SeasonRepository extends GenericRepository<Season, Long> {
    @Query(value = "SELECT season.* FROM season  INNER JOIN movie_season_episode ON season .id = movie_season_episode.season_id " +
            "INNER JOIN movie ON movie.id = movie_season_episode.movie_id " +
            "WHERE movie_season_episode.movie_id = ?1 GROUP BY season.id", nativeQuery = true)
    List<Season> getSeasonByMovie(Long seasonId);
}

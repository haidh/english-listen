package shop.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import shop.entity.MovieSeasonEpisode;

import java.util.List;

@Repository
public interface MovieSeasonEpisodeRepository extends GenericRepository<MovieSeasonEpisode, Long> {
    @Query(value = "SELECT * FROM movie_season_episode WHERE movie_season_episode.movie_id = ?1 " +
            "AND movie_season_episode.season_id = ?2 AND movie_season_episode.episode_id=?3", nativeQuery = true)
    MovieSeasonEpisode findMovieSeasonEpisodeByMovieAndSeasonAndEpisode(Long movieId, Long seasonId, Long episodeId);

    List<MovieSeasonEpisode> findMovieSeasonEpisodeByMovieId(Long movieId);

}

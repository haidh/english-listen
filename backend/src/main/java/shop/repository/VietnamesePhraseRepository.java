package shop.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import shop.entity.VietnamesePhrase;

import java.util.List;

@Repository
public interface VietnamesePhraseRepository extends GenericRepository<VietnamesePhrase, Long> {
    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM VietnamesePhrase p WHERE p.name = CONVERT(?1,BINARY)")
    Boolean checkExistedByName(String name);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM VietnamesePhrase p WHERE p.id = ?1")
    Boolean checkExistedById(Long id);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM VietnamesePhrase p WHERE p.name = ?1 AND p.id <> ?2")
    Boolean checkExistedNotById(String name, Long id);

    @Query("SELECT p FROM VietnamesePhrase p WHERE p.id = ?1")
    VietnamesePhrase findVietnamesePhraseById(Long id);

    @Query("SELECT p FROM VietnamesePhrase p WHERE p.name = CONVERT(?1,BINARY)")
    VietnamesePhrase findVietnamesePhraseByName(String name);

    void deleteByIdIn(List<Long> listId);
}

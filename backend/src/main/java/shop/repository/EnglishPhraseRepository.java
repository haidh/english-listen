package shop.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import shop.entity.EnglishPhrase;
import shop.entity.Sentence;

import java.util.List;

@Repository
public interface EnglishPhraseRepository extends GenericRepository<EnglishPhrase, Long> {

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM EnglishPhrase p WHERE p.id = ?1")
    Boolean checkExistedById(Long id);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM EnglishPhrase p WHERE p.name = ?1")
    Boolean checkExistedByName(String sentence);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM EnglishPhrase p WHERE p.name = ?1 AND p.id <> ?2")
    Boolean checkExistedNotById(String name, Long id);

    EnglishPhrase findEnglishPhraseByName(String name);

    void deleteEnglishPhraseByIdIn(List<Long> listId);

    @Query(value = "SELECT * FROM english_phrase WHERE LOWER(name) regexp :search",
            countQuery = "SELECT count(*) FROM english_phrase WHERE LOWER(name) regexp :search",
            nativeQuery = true)
    Page<EnglishPhrase> getPhrases(@Param("search") String search, Pageable pageable);

    @Query(value = "SELECT * FROM english_phrase e " +
            "INNER JOIN english_phrase_vietnamese_phrase ev ON e.id=ev.english_phrase_id " +
            "INNER JOIN vietnamese_phrase v ON ev.vietnamese_phrase_id=v.id " +
            "WHERE LOWER(v.name) regexp :search " +
            "GROUP BY e.id",

            countQuery = "SELECT count(*) FROM english_phrase e" +
                    "INNER JOIN english_phrase_vietnamese_phrase ev ON e.id=ev.english_phrase_id " +
                    "INNER JOIN vietnamese_phrase v ON ev.vietnamese_phrase_id=v.id " +
                    "WHERE LOWER(v.name) regexp :search " +
                    "GROUP BY e.id",
            nativeQuery = true)
    Page<EnglishPhrase> getVietnamesePhrases(@Param("search") String search, Pageable pageable);

    @Query(value = "SELECT * FROM english_phrase e " +
            "INNER JOIN english_phrase_vietnamese_phrase ev ON e.id=ev.english_phrase_id " +
            "INNER JOIN vietnamese_phrase v ON ev.vietnamese_phrase_id=v.id " +
            "WHERE  LOWER(e.name) regexp :searchPhrase " +
            "AND LOWER(v.name) regexp :searchVietnamesePhrase " +
            "GROUP BY e.id",

            countQuery = "SELECT count(*) FROM english_phrase e" +
                    "INNER JOIN english_phrase_vietnamese_phrase ev ON e.id=ev.english_phrase_id " +
                    "INNER JOIN vietnamese_phrase v ON ev.vietnamese_phrase_id=v.id " +
                    "WHERE  LOWER(e.name) regexp :searchPhrase " +
                    "AND LOWER(v.name) regexp :searchVietnamesePhrase " +
                    "GROUP BY e.id",
            nativeQuery = true)
    Page<EnglishPhrase> getEnglishVietnamesePhrases(@Param("searchPhrase") String searchPhrase, @Param("searchVietnamesePhrase") String searchVietnamesePhrase, Pageable pageable);
}

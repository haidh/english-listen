package shop.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import shop.entity.Movie;

import java.util.List;

@Repository
public interface MovieRepository extends GenericRepository<Movie, Long> {
    @Query("SELECT CASE WHEN COUNT(m) > 0 THEN true ELSE false END FROM Movie m WHERE m.name = :name")
    Boolean checkExistedByName(@Param("name") String name);

    @Query("SELECT CASE WHEN COUNT(m) > 0 THEN true ELSE false END FROM Movie m WHERE m.name = :name AND m.id <> :id")
    Boolean checkExistedNotById(@Param("name") String name, @Param("id") Long id);

    void deleteByIdIn(List<Long> listId);
}

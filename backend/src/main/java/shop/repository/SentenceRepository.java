package shop.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import shop.entity.Movie;
import shop.entity.Sentence;

import java.util.List;

@Repository
public interface SentenceRepository extends GenericRepository<Sentence, Long> {
    @Query("SELECT CASE WHEN COUNT(s) > 0 THEN true ELSE false END FROM Sentence s WHERE s.sentence = ?1 AND movie_season_episode_id=?2")
    Boolean checkExistedBySentence(String sentence, Long movieSeasonEpisodeId);

    @Query(value = "SELECT * , " +
            "MATCH (sentence) AGAINST (:search) as score " +
            "FROM sentence " +
            "WHERE MATCH (sentence) AGAINST (:search) > 0",
            countQuery = "SELECT count(*) ," +
                    "MATCH (sentence) AGAINST (:search) as score " +
                    "FROM sentence " +
                    "WHERE MATCH (sentence) AGAINST (:search) > 0",

            nativeQuery = true)
    Page<Sentence> getListSentenceSearch(@Param("search") String search, Pageable pageable);

    @Query(value = "SELECT * , " +
            "MATCH (translation ) AGAINST (:search) as score " +
            "FROM sentence " +
            "WHERE MATCH (translation) AGAINST (:search) > 0",
            countQuery = "SELECT * , " +
                    "MATCH (translation ) AGAINST (:search) as score " +
                    "FROM sentence " +
                    "WHERE MATCH (translation) AGAINST (:search) > 0",
            nativeQuery = true)
    Page<Sentence> getListTranslationSentenceSearch(@Param("search") String search, Pageable pageable);

//    @Query("SELECT CASE WHEN COUNT(s) > 0 THEN true ELSE false END FROM Sentence s WHERE s.sentence = ?1 AND s.id <> ?2")
//    Boolean checkExistedNotById(String sentence, Long id);

    @Query("SELECT CASE WHEN COUNT(s) > 0 THEN true ELSE false END FROM Sentence s WHERE s.sentence = ?1 AND s.id <> ?2 AND movie_season_episode_id=?3")
    Boolean checkExistedNotById(String sentence, Long id, Long movieSeasonEpisodeId);

    @Query("SELECT CASE WHEN COUNT(s) > 0 THEN true ELSE false END FROM Sentence s  WHERE s.sentence = ?1")
    Boolean checkExisted(String sentence);

    @Query(value = "SELECT sentence.id, sentence.sentence,sentence.translation,sentence.audio," +
            "movie.id as movie_id, movie.name as movie_name," +
            "season.id as season_id, season.name as season_name," +
            "episode.id as episode_id, episode.name as episode_name " +
            "FROM sentence INNER JOIN movie_season_episode ON sentence.movie_season_episode_id = movie_season_episode.id " +
            "LEFT JOIN movie ON movie.id = movie_season_episode.movie_id " +
            "LEFT JOIN season ON season.id = movie_season_episode.season_id " +
            "LEFT JOIN episode ON episode.id = movie_season_episode.episode_id " +
            "WHERE sentence.id = ?1", nativeQuery = true)
    Object getSentenceDetail(Long id);

    void deleteByIdIn(List<Long> listId);

    List<Sentence> getSentenceByMovieSeasonEpisodeId(Long id);

    @Query(value = "SELECT * FROM sentence WHERE LOWER(sentence) regexp :search",
            countQuery = "SELECT count(*) FROM sentence WHERE LOWER(sentence) regexp :search",
            nativeQuery = true)
    Page<Sentence> getSentenceBySearchSentence(@Param("search") String search, Pageable pageable);

    @Query(value = "SELECT * FROM sentence WHERE LOWER(translation) regexp :search",
            countQuery = "SELECT count(*) FROM sentence WHERE LOWER(translation) regexp :search",
            nativeQuery = true)
    Page<Sentence> getSentenceBySearchTranslation(@Param("search") String search, Pageable pageable);

    @Query(value = "SELECT * FROM sentence WHERE LOWER(sentence) regexp :sentence AND LOWER(translation) regexp :translation",
            countQuery = "SELECT count(*) FROM sentence WHERE LOWER(sentence) regexp :sentence AND LOWER(translation) regexp :translation",
            nativeQuery = true)
    Page<Sentence> getSentenceBySearchSentenceAndTranslation(@Param("sentence") String sentence, @Param("translation") String translation, Pageable pageable);

    @Query(value = "SELECT * FROM sentence WHERE LOWER(sentence) regexp :search AND movie_season_episode_id IN :movieSeasonEpisodeId",
            countQuery = "SELECT count(*) FROM sentence WHERE LOWER(sentence) regexp :search AND movie_season_episode_id IN :movieSeasonEpisodeId",
            nativeQuery = true)
    Page<Sentence> getSentenceBySearchSentenceAndMovie(@Param("search") String search, @Param("movieSeasonEpisodeId") List<Long> movieSeasonEpisodeId, Pageable pageable);

    @Query(value = "SELECT * FROM sentence WHERE LOWER(translation) regexp :search AND movie_season_episode_id IN :movieSeasonEpisodeId",
            countQuery = "SELECT count(*) FROM sentence WHERE LOWER(translation) regexp :search AND movie_season_episode_id IN :movieSeasonEpisodeId",
            nativeQuery = true)
    Page<Sentence> getSentenceBySearchTranslationAndMovie(@Param("search") String search, @Param("movieSeasonEpisodeId") List<Long> movieSeasonEpisodeId, Pageable pageable);


    @Query(value = "SELECT * FROM sentence WHERE LOWER(sentence) regexp :sentence AND LOWER(translation) regexp :translation AND movie_season_episode_id IN :movieSeasonEpisodeId",
            countQuery = "SELECT count(*) FROM sentence WHERE LOWER(sentence) regexp :sentence AND LOWER(translation) regexp :translation AND movie_season_episode_id IN :movieSeasonEpisodeId",
            nativeQuery = true)
    Page<Sentence> getSentenceBySearchSentenceAndTranslationAndMovie(@Param("sentence") String sentence, @Param("translation") String translation, @Param("movieSeasonEpisodeId") List<Long> movieSeasonEpisodeId, Pageable pageable);


    @Query(value = "SELECT * FROM sentence WHERE  movie_season_episode_id IN :movieSeasonEpisodeId",
            countQuery = "SELECT count(*) FROM sentence WHERE movie_season_episode_id IN :movieSeasonEpisodeId",
            nativeQuery = true)
    Page<Sentence> findAllByMovieId(@Param("movieSeasonEpisodeId") List<Long> movieSeasonEpisodeId, Pageable pageable);

    Sentence findSentenceByAudio(String audio);
}

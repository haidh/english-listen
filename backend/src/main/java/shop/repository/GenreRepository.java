package shop.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import shop.entity.Genre;
import shop.entity.Movie;
import shop.entity.Sentence;

import java.util.List;

@Repository
public interface GenreRepository extends GenericRepository<Genre, Long> {

}

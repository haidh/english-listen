package shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import shop.entity.Permission;
import shop.entity.User;
import shop.exceptions.Exception;
import shop.services.PermissionService;
import shop.services.ValidationExceptionService;
import shop.validator.PermissionValidator;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/permission")
@CrossOrigin
public class PermissionController {
    @Autowired
    private PermissionService permissionService;

    @Autowired
    private PermissionValidator permissionValidator;

    @Autowired
    private ValidationExceptionService validationExceptionService;

    @GetMapping("")
    public ResponseEntity<?> getAllPermission() {
        List<Permission> listPermission = permissionService.findAll();
        return new ResponseEntity<>(listPermission, HttpStatus.OK);
    }

    @GetMapping("/pagination")
    @PreAuthorize("hasAuthority('PERMISSION_READ')")
    public ResponseEntity<?> getPermissionPagination(@PageableDefault(size = 10) Pageable pageable,
                                                     @RequestParam(defaultValue = "") String name,
                                                     @RequestParam(defaultValue = "") String description) {
        Map<String, String> params = new HashMap<>();
        params.put("name", name);
        params.put("description", description);
        Map<String, Object> results = permissionService.getPermissionPagination(pageable, params);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('PERMISSION_CREATE')")
    public ResponseEntity<?> createPermission(@Valid @RequestBody Permission permission, BindingResult result, Principal principal) throws Exception {
        permissionValidator.validate(permission, result);

        ResponseEntity<?> validateErrors = validationExceptionService.validateException(result);
        if (validateErrors != null) {
            return validateErrors;
        }
        return new ResponseEntity<>(permissionService.create(permission, principal), HttpStatus.CREATED);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('PERMISSION_UPDATE')")
    public ResponseEntity<?> updatePermission(@Valid @RequestBody Permission permission, BindingResult result, Principal principal) throws Exception {
        permissionValidator.validate(permission, result);

        ResponseEntity<?> validateErrors = validationExceptionService.validateException(result);
        if (validateErrors != null) {
            return validateErrors;
        }
        return new ResponseEntity<>(permissionService.create(permission, principal), HttpStatus.CREATED);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getPermissionDetail(@PathVariable Long id){
        return new ResponseEntity<>(permissionService.findById(id), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('PERMISSION_DELETE')")
    public ResponseEntity<?> deletePermission(@PathVariable Long id) {
        try{
            permissionService.deleteById(id);
        }catch (Exception e){
            throw new Exception("Delete Error!");
        }

        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    @PreAuthorize("hasAuthority('PERMISSION_DELETE')")
    public ResponseEntity<?> deletePermission(@RequestParam List<Long> listId) {
        permissionService.deleteManyPermission(listId);
        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }

}

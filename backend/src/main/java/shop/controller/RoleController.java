package shop.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import shop.entity.Notification;
import shop.entity.Role;
import shop.exceptions.Exception;
import shop.services.RoleService;
import shop.services.ValidationExceptionService;
import shop.validator.RoleValidator;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/role")
@CrossOrigin
public class RoleController {
    @Autowired
    private RoleService roleService;

    @Autowired
    private RoleValidator roleValidator;

    @Autowired
    private ValidationExceptionService validationExceptionService;

    @GetMapping("")
    public ResponseEntity<?> getAllRole() {
        List<Role> listRole = roleService.findAll();
        return new ResponseEntity<>(listRole, HttpStatus.OK);
    }

    @GetMapping("/pagination")
    @PreAuthorize("hasAuthority('ROLE_READ')")
    public ResponseEntity<?> getRolePagination(@PageableDefault(size = 10) Pageable pageable,
                                               @RequestParam(defaultValue = "") String name,
                                               @RequestParam(defaultValue = "") String description) {
        Map<String, String> params = new HashMap<>();
        params.put("name", name);
        params.put("description", description);
        Map<String, Object> results = roleService.getRolePagination(pageable, params);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE')")
    public ResponseEntity<?> creatRole(@Valid @RequestBody Role role, BindingResult result, Principal principal) throws Exception {
        roleValidator.validate(role, result);

        ResponseEntity<?> validateErrors = validationExceptionService.validateException(result);
        if (validateErrors != null) {
            return validateErrors;
        }
        return new ResponseEntity<>(roleService.create(role, principal), HttpStatus.CREATED);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('ROLE_UPDATE')")
    public ResponseEntity<?> updateRole(@Valid @RequestBody Role role, BindingResult result, Principal principal) throws Exception {
        roleValidator.validate(role, result);

        ResponseEntity<?> validateErrors = validationExceptionService.validateException(result);
        if (validateErrors != null) {
            return validateErrors;
        }
        return new ResponseEntity<>(roleService.create(role, principal), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_READ')")
    public ResponseEntity<?> getRoleDetail(@PathVariable Long id) {
        return new ResponseEntity<>(roleService.findById(id), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_DELETE')")
    public ResponseEntity<?> deleteRole(@PathVariable Long id, Principal principal) {
        try {
            roleService.deleteById(id,principal);
        } catch (Exception | JsonProcessingException e) {
            throw new Exception("Delete Error!");
        }

        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    @PreAuthorize("hasAuthority('ROLE_DELETE')")
    public ResponseEntity<?> deleteManyRole(@RequestParam List<Long> listId) {
        roleService.deleteMany(listId);
        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }

}

package shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import shop.entity.Movie;
import shop.entity.Season;
import shop.exceptions.Exception;
import shop.services.MovieService;
import shop.services.SeasonService;
import shop.services.ValidationExceptionService;
import shop.validator.MovieValidator;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/season")
@CrossOrigin
public class SeasonController {
    @Autowired
    private SeasonService seasonService;

    @Autowired
    private MovieValidator movieValidator;

    @Autowired
    private ValidationExceptionService validationExceptionService;

    @GetMapping("")
    public ResponseEntity<?> getAllMovie() {
        List<Season> listSeason = seasonService.findAll();
        return new ResponseEntity<>(listSeason, HttpStatus.OK);
    }

    @GetMapping("/movie-season/{id}")
    public ResponseEntity<?> getSeasonByMovie(@PathVariable Long id) {
        List<Season> listSeason = seasonService.getSeasonByMovie(id);
        return new ResponseEntity<>(listSeason, HttpStatus.OK);
    }
}

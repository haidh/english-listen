package shop.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import shop.entity.User;
import shop.exceptions.Exception;
import shop.payload.JWTLoginSuccessResponse;
import shop.payload.LoginRequest;
import shop.security.JwtTokenProvider;
import shop.services.UserService;
import shop.services.ValidationExceptionService;
import shop.validator.UserValidator;

import javax.validation.Valid;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private ValidationExceptionService validationExceptionService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Value("${app.jwt.token.prefix}")
    private String jwtTokenPrefix;

    @Autowired
    private RabbitTemplate rabbitTemplate;
    private static final Logger log = LoggerFactory.getLogger(UserController.class);
    private ObjectMapper objectMapper = new ObjectMapper();


    @PostMapping("/register")
    public ResponseEntity<?> createUser(@Valid @RequestBody User user, BindingResult result) throws Exception {
        userValidator.validate(user, result);
        ResponseEntity<?> validateErrors = validationExceptionService.validateException(result);
        if (validateErrors != null) {
            return validateErrors;
        }

        return new ResponseEntity<>(userService.register(user), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword());
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);

        String jwt = jwtTokenPrefix + jwtTokenProvider.generateToken(authentication);
        User user = (User) authentication.getPrincipal();

        Set<String> operations = new HashSet<>();
        if (user.getAuthorities() != null) {
            operations = user.getAuthorities().stream().map(item -> ((GrantedAuthority) item).getAuthority()).collect(Collectors.toSet());
        }
        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
        return ResponseEntity.ok(new JWTLoginSuccessResponse(jwt, user.getId(), user.getUsername(), operations, sessionId));
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllUser() {
        List<User> listUser = userService.getAllUser();
        return new ResponseEntity<>(listUser, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
//    @PreAuthorize("hasAuthority('USER_DELETE')")
    public ResponseEntity<?> deleteUser(@PathVariable Long id, Principal principal) {
        try {
            userService.deleteById(id,principal);
        } catch (Exception | JsonProcessingException e) {
            throw new Exception("Delete Error!");
        }

        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }

    @DeleteMapping("/delete")
//    @PreAuthorize("hasAuthority('USER_DELETE')")
    public ResponseEntity<?> deleteManyUser(@RequestParam List<Long> listId) {
        userService.deleteMany(listId);
        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }

    @GetMapping("/pagination")
//    @PreAuthorize("hasAuthority('USER_READ')")
    public ResponseEntity<?> getUserPagination(@PageableDefault(size = 10) Pageable pageable,
                                               @RequestParam(defaultValue = "") String username,
                                               @RequestParam(defaultValue = "") String fullName,
                                               @RequestParam(defaultValue = "") String email,
                                               Principal principal) {
        Map<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("fullName", fullName);
        params.put("email", email);
        Map<String, Object> results = userService.getUserPagination(pageable, params);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @GetMapping("/{id}")
//    @PreAuthorize("hasAuthority('USER_READ')")
    public ResponseEntity<?> getUserDetail(@PathVariable Long id) {
        return new ResponseEntity<>(userService.findById(id), HttpStatus.CREATED);
    }

    @PostMapping("/update")
//    @PreAuthorize("hasAuthority('USER_UPDATE')")
    public ResponseEntity<?> updateUser(@Valid @RequestBody User user, BindingResult result, Principal principal) throws Exception {
        userValidator.validate(user, result);

        ResponseEntity<?> validateErrors = validationExceptionService.validateException(result);
        if (validateErrors != null) {
            return validateErrors;
        }
        return new ResponseEntity<>(userService.create(user, principal), HttpStatus.CREATED);
    }
}

package shop.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import shop.condition.OperationCondition;
import shop.entity.Operation;
import shop.entity.Permission;
import shop.services.OperationService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/operation", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin
public class OperationController {
    @Autowired
    private OperationService operationService;


    @GetMapping("")
    public ResponseEntity<?> getAllOperation() {
        List<Operation> listOperation = operationService.findAll();
        return new ResponseEntity<>(listOperation, HttpStatus.OK);
    }

    @GetMapping("/json")
    public ResponseEntity<?> getAllOperationJson() {
        String listOperation = operationService.getAllOperation();
        return new ResponseEntity<>(listOperation, HttpStatus.OK);
    }

    @PostMapping("/list")
    public ResponseEntity<?> getOperationByCondition(@RequestBody OperationCondition operationCondition) {
        List<Operation> listOperation = operationService.getOperationByCondition(operationCondition);
        return new ResponseEntity<>(listOperation, HttpStatus.OK);
    }

}

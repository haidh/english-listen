package shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import shop.entity.EnglishPhrase;
import shop.entity.ITSentence;
import shop.entity.VietnamesePhrase;
import shop.exceptions.Exception;
import shop.services.EnglishPhraseService;
import shop.services.ITSentenceService;
import shop.services.ValidationExceptionService;
import shop.services.VietnamesePhraseService;
import shop.validator.EnglishValidator;
import shop.validator.ITSentenceValidator;

import javax.servlet.ServletContext;
import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/it-sentence")
@CrossOrigin
public class ITSentenceController {

    @Autowired
    private ITSentenceService itSentenceService;


    @Autowired
    private ITSentenceValidator itSentenceValidator;

    @Autowired
    private ValidationExceptionService validationExceptionService;

    @Autowired
    ServletContext context;

    @PostMapping("/create")
    public ResponseEntity<?> createITSentence(@Valid @RequestBody ITSentence itSentence, BindingResult result, Principal principal) throws Exception {
        itSentenceValidator.validate(itSentence, result);

        ResponseEntity<?> validateErrors = validationExceptionService.validateException(result);
        if (validateErrors != null) {
            return validateErrors;
        }
        return new ResponseEntity<>(itSentenceService.create(itSentence, principal), HttpStatus.CREATED);
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateITSentence(@Valid @RequestBody ITSentence itSentence, BindingResult result, Principal principal) throws Exception {
        itSentenceValidator.validate(itSentence, result);

        ResponseEntity<?> validateErrors = validationExceptionService.validateException(result);
        if (validateErrors != null) {
            return validateErrors;
        }
        return new ResponseEntity<>(itSentenceService.update(itSentence, principal), HttpStatus.OK);
    }

    @GetMapping("/pagination")
    public ResponseEntity<?> getPagination(@PageableDefault(size = 10) Pageable pageable,
                                           @RequestParam(defaultValue = "") String sentence,
                                           @RequestParam(defaultValue = "") String translation) {
        Map<String, String> params = new HashMap<>();
        params.put("sentence", sentence);
        params.put("translation", translation);
        Map<String, Object> results = itSentenceService.getITSentencePagination(pageable, params);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
            itSentenceService.deleteById(id);
        } catch (Exception e) {
            throw new Exception("Delete Error!");
        }

        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteMany(@RequestParam List<Long> listId) {
        itSentenceService.deleteMany(listId);
        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getITSentenceDetail(@PathVariable Long id) {
        return new ResponseEntity<>(itSentenceService.findById(id), HttpStatus.CREATED);
    }
}

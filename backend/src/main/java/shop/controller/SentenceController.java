package shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import shop.entity.MovieSeasonEpisode;
import shop.entity.Sentence;
import shop.exceptions.Exception;
import shop.services.MovieSeasonEpisodeService;
import shop.services.SentenceService;
import shop.services.ValidationExceptionService;
import shop.validator.MovieValidator;

import javax.servlet.ServletContext;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping(value = "/api/sentence", produces = {MediaType.ALL_VALUE})
@CrossOrigin
public class SentenceController {

    @Autowired
    private SentenceService sentenceService;

    @Autowired
    private MovieSeasonEpisodeService movieSeasonEpisodeService;

    @Autowired
    private MovieValidator movieValidator;

    @Autowired
    private ValidationExceptionService validationExceptionService;

    @Autowired
    ServletContext context;


    @GetMapping("/pagination")
    public ResponseEntity<?> getPagination(@PageableDefault(size = 10) Pageable pageable,
                                           @RequestParam(defaultValue = "0") String page,
                                           @RequestParam(defaultValue = "10") String size,
                                           @RequestParam(defaultValue = "") String sentence,
                                           @RequestParam(defaultValue = "") String movieId,
                                           @RequestParam(defaultValue = "") String translation) throws IOException {
        Map<String, String> params = new HashMap<>();
        params.put("sentence", sentence.trim());
        params.put("translation", translation.trim());
        params.put("page", page);
        params.put("size", size);
        params.put("movieId", movieId);
        Map<String, Object> results = sentenceService.getSentencePagination(pageable, params);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @PostMapping(value = "/create", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> create(@RequestParam(value = "movieId") Long movieId,
                                    @RequestParam(value = "seasonId") Long seasonId,
                                    @RequestParam(value = "episodeId") Long episodeId,
                                    @RequestParam("file") MultipartFile file,
                                    @RequestParam(value = "sentence", defaultValue = "") String sentence,
                                    @RequestParam(value = "translation", defaultValue = "") String translation,
                                    Principal principal) throws IOException {

        MovieSeasonEpisode movieSeasonEpisode = movieSeasonEpisodeService.getMovieSeasonEpisode(movieId, seasonId, episodeId);
        Sentence sentenceSave = new Sentence();
//        if (file.isEmpty()) {
//            throw new Exception("File is now existed");
//        } else {
//            String fileName = file.getOriginalFilename();
//            byte fileContent[] = new byte[(int) file.getSize()];
//            InputStream inputStream = file.getInputStream();
//            inputStream.read(fileContent);
//            sentenceSave.setData(fileContent);
//            sentenceSave.setAudio(fileName);
//        }

        sentenceSave.setSentence(sentence.trim());
        sentenceSave.setTranslation(translation.trim());
        sentenceSave.setCreated_by(principal.getName());
        if (movieSeasonEpisode != null) {
            sentenceSave.setMovieSeasonEpisode(movieSeasonEpisode);
        }
        return new ResponseEntity<>(sentenceService.create(sentenceSave, principal), HttpStatus.CREATED);


    }

    @PostMapping(value = "/update", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> update(@RequestParam("sentenceId") Long sentenceId,
                                    @RequestParam("movieId") Long movieId,
                                    @RequestParam("seasonId") Long seasonId,
                                    @RequestParam("episodeId") Long episodeId,
                                    @RequestParam(value = "file") MultipartFile file,
                                    @RequestParam(value = "sentence", defaultValue = "") String sentence,
                                    @RequestParam(value = "translation", defaultValue = "") String translation,
                                    @RequestParam(value = "audio", defaultValue = "") String audio,
                                    Principal principal) throws IOException, IllegalAccessException {

        MovieSeasonEpisode movieSeasonEpisode = movieSeasonEpisodeService.getMovieSeasonEpisode(movieId, seasonId, episodeId);
        Sentence sentenceSave = new Sentence();
//        if (!file.isEmpty()) {
//            String fileName = file.getOriginalFilename();
//            byte fileContent[] = new byte[(int) file.getSize()];
//            InputStream inputStream = file.getInputStream();
//            inputStream.read(fileContent);
//            sentenceSave.setData(fileContent);
//            sentenceSave.setAudio(fileName);
//        } else {
//            sentenceSave.setData(sentenceService.findById(sentenceId).get().getData());
//            if (audio.length() != 0) {
//                sentenceSave.setAudio(audio);
//            }
//        }

        sentenceSave.setId(sentenceId);
        sentenceSave.setSentence(sentence.trim());
        sentenceSave.setTranslation(translation.trim());
        sentenceSave.setCreated_by(principal.getName());
        if (movieSeasonEpisode != null) {
            sentenceSave.setMovieSeasonEpisode(movieSeasonEpisode);
        }
        return new ResponseEntity<>(sentenceService.update(sentenceSave, principal), HttpStatus.CREATED);

    }

    @PostMapping(value = "/save-file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void saveFile(@RequestParam(value = "file") MultipartFile file) throws IOException {
        if (!file.isEmpty()) {
            File fileToGetPath = new File("");
            String filePath = fileToGetPath.getAbsolutePath().substring(0, fileToGetPath.getAbsolutePath().lastIndexOf("english-listen"));
            String directoryPath = filePath + "\\english-audio";
            Files.createDirectories(Paths.get(directoryPath));
            File destination = new File(directoryPath + "\\" + file.getOriginalFilename());
            file.transferTo(destination);
        }
    }

    @PostMapping(value = "/read-file-to-import-sentence", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void readFileToImportSentence(@RequestParam(value = "file") MultipartFile file) throws IOException {
        List<Sentence> sentences = new ArrayList<>();
        if (!file.isEmpty()) {
            Scanner sc = null;
            InputStream inputStream = file.getInputStream();
            sc = new Scanner(inputStream, "UTF-8");
            int audioIndex = 1;
            String audio = "";
            String lastAudio = "";
            if (file.getOriginalFilename().equalsIgnoreCase("english.txt")) {
                while (sc.hasNextLine()) {
                    String line = sc.nextLine();
                    if (audioIndex == 1) {
                        lastAudio = line;
                    } else if (audioIndex > 1 && audioIndex <= 10) {
                        audio = lastAudio + "_00" + (audioIndex - 1) + ".mp3";
                    } else if (audioIndex > 10 && audioIndex <= 100) {
                        audio = lastAudio + "_0" + (audioIndex - 1) + ".mp3";
                    } else {
                        audio = lastAudio + "_" + (audioIndex - 1) + ".mp3";
                    }
                    if (audioIndex > 1) {
                        if (!line.equals("")) {
                            Sentence sentence = new Sentence();
                            sentence.setSentence(line.trim());
                            sentence.setAudio(audio);
                            sentences.add(sentence);
                        }
                    }
                    audioIndex++;
                }
            } else {
                while (sc.hasNextLine()) {
                    String line = sc.nextLine();
                    if (audioIndex == 1) {
                        lastAudio = line;
                    } else if (audioIndex > 1 && audioIndex <= 10) {
                        audio = lastAudio + "_00" + (audioIndex - 1) + ".mp3";
                    } else if (audioIndex > 10 && audioIndex <= 100) {
                        audio = lastAudio + "_0" + (audioIndex - 1) + ".mp3";
                    } else {
                        audio = lastAudio + "_" + (audioIndex - 1) + ".mp3";
                    }

                    if (audioIndex > 1) {
                        if (!line.equals("")) {
                            Sentence sentence = sentenceService.findSentenceByAudio(audio);
                            sentence.setTranslation(line.trim());
                            sentenceService.create(sentence);
                        }
                    }
                    audioIndex++;
                }
            }
            sentenceService.saveAll(sentences);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDetail(@PathVariable Long id) throws IllegalAccessException {
        return new ResponseEntity<>(sentenceService.getSentenceDetail(id), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
            sentenceService.deleteById(id);
        } catch (Exception e) {
            throw new Exception("Delete Error!");
        }

        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteMany(@RequestParam List<Long> listId) {
        sentenceService.deleteMany(listId);
        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }

}

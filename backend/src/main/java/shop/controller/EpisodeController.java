package shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import shop.entity.Episode;
import shop.entity.Movie;
import shop.entity.Season;
import shop.exceptions.Exception;
import shop.services.EpisodeService;
import shop.services.MovieService;
import shop.services.ValidationExceptionService;
import shop.validator.MovieValidator;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/episode")
@CrossOrigin
public class EpisodeController {
    @Autowired
    private EpisodeService episodeService;

    @Autowired
    private ValidationExceptionService validationExceptionService;

    @GetMapping("")
    public ResponseEntity<?> getAll() {
        List<Episode> listEpisode = episodeService.findAll();
        return new ResponseEntity<>(listEpisode, HttpStatus.OK);
    }

    @GetMapping("/movie-season")
    public ResponseEntity<?> getSeasonByMovie(@RequestParam Long movieId, Long seasonId) {
        List<Episode> listEpisode = episodeService.getEpisodeByMovieSeason(movieId,seasonId);
        return new ResponseEntity<>(listEpisode, HttpStatus.OK);
    }
}

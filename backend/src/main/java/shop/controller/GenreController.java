package shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import shop.dto.GenreDTO;
import shop.dto.MovieDTO;
import shop.entity.Genre;
import shop.entity.Movie;
import shop.exceptions.Exception;
import shop.services.GenreService;
import shop.services.MovieService;
import shop.services.ValidationExceptionService;
import shop.validator.MovieValidator;

import javax.validation.Valid;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/genre")
@CrossOrigin
public class GenreController {
    @Autowired
    private GenreService genreService;

    @GetMapping("")
    public ResponseEntity<?> getAllGenre() {
        List<Genre> genres = genreService.findAll();
        List<GenreDTO> genreDTOS = genres.stream().map(genre -> new GenreDTO(genre.getId(), genre.getName(), genre.getDescription())).collect(Collectors.toList());
        return new ResponseEntity<>(genreDTOS, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDetail(@PathVariable Long id) {
        Optional<Genre> genreOptional = genreService.findGenreById(id);
        Genre genre = genreOptional.get();
        List<Movie> m = genre.getMovie();
        List<MovieDTO> movies = m.stream().map(movie -> new MovieDTO(movie.getId(), movie.getName(), movie.getDescription(), movie.getSeasonNumber()))
                .collect(Collectors.toList());
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

}

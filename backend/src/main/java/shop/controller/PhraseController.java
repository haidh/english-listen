package shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import shop.entity.EnglishPhrase;
import shop.entity.VietnamesePhrase;
import shop.exceptions.Exception;
import shop.services.EnglishPhraseService;
import shop.services.ValidationExceptionService;
import shop.services.VietnamesePhraseService;
import shop.validator.EnglishValidator;

import javax.servlet.ServletContext;
import javax.validation.Valid;
import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping("/api/phrase")
@CrossOrigin
public class PhraseController {

    @Autowired
    private EnglishPhraseService englishPhraseService;

    @Autowired
    private VietnamesePhraseService vietnamesePhraseService;

    @Autowired
    private EnglishValidator englishValidator;

    @Autowired
    private ValidationExceptionService validationExceptionService;

    @Autowired
    ServletContext context;


    @GetMapping("")
    public ResponseEntity<?> getPhrases(@RequestParam(defaultValue = "") String name,
                                        @RequestParam(defaultValue = "") String translation) {


        Map<String, Object> result = new HashMap<>();
        if (name.length() != 0 && translation.length() != 0) {

            Map<String, Object> englishPhrase = new HashMap<>();
            englishPhrase.put("vietnamesePhrasesSet", englishPhraseService.getPhrases(name.trim()).getVietnamesePhrasesSet());
            englishPhrase.put("englishPhrasesSet", vietnamesePhraseService.getPhrases(translation.trim()).getEnglishPhrasesSet());
            return new ResponseEntity<>(englishPhrase, HttpStatus.OK);
        } else {
            if (name.length() != 0) {
                EnglishPhrase englishPhrase = new EnglishPhrase();
                englishPhrase = englishPhraseService.getPhrases(name.trim());
                return new ResponseEntity<>(englishPhrase, HttpStatus.OK);
            }

            if (translation.length() != 0) {
                VietnamesePhrase vietnamesePhrase = new VietnamesePhrase();
                vietnamesePhrase = vietnamesePhraseService.getPhrases(translation.trim());
                return new ResponseEntity<>(vietnamesePhrase, HttpStatus.OK);
            }
        }

        return new ResponseEntity<>(new EnglishPhrase(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPhraseDetail(@PathVariable Long id) {
        return new ResponseEntity<>(englishPhraseService.findById(id), HttpStatus.CREATED);
    }


    @PostMapping("/create")
    public ResponseEntity<?> createPhrase(@Valid @RequestBody EnglishPhrase englishPhrase, BindingResult result, Principal principal) throws Exception {
        englishValidator.validate(englishPhrase, result);
        englishPhrase.setName(englishPhrase.getName().trim().toLowerCase());
        Set<VietnamesePhrase> vietnamesePhrases = new HashSet<>();
        if (englishPhrase.getVietnamesePhrases().size() > 0) {
            for (VietnamesePhrase vietnamesePhrase : englishPhrase.getVietnamesePhrases()) {
                vietnamesePhrase.setName(vietnamesePhrase.getName().trim().toLowerCase());
                vietnamesePhrases.add(vietnamesePhrase);
            }
        }
        englishPhrase.setVietnamesePhrases(vietnamesePhrases);
        ResponseEntity<?> validateErrors = validationExceptionService.validateException(result);
        if (validateErrors != null) {
            return validateErrors;
        }
        return new ResponseEntity<>(englishPhraseService.create(englishPhrase, principal), HttpStatus.CREATED);
    }

    @PostMapping("/update")
    public ResponseEntity<?> updatePhrase(@Valid @RequestBody EnglishPhrase englishPhrase, BindingResult result, Principal principal) throws Exception {
        englishValidator.validate(englishPhrase, result);
        englishPhrase.setName(englishPhrase.getName().trim().toLowerCase());
        Set<VietnamesePhrase> vietnamesePhrases = new HashSet<>();
        if (englishPhrase.getVietnamesePhrases().size() > 0) {
            for (VietnamesePhrase vietnamesePhrase : englishPhrase.getVietnamesePhrases()) {
                vietnamesePhrase.setName(vietnamesePhrase.getName().trim().toLowerCase());
                vietnamesePhrases.add(vietnamesePhrase);
            }
        }
        englishPhrase.setVietnamesePhrases(vietnamesePhrases);
        ResponseEntity<?> validateErrors = validationExceptionService.validateException(result);
        if (validateErrors != null) {
            return validateErrors;
        }
        return new ResponseEntity<>(englishPhraseService.create(englishPhrase, principal), HttpStatus.CREATED);
    }

    @GetMapping("/pagination")
    public ResponseEntity<?> getPagination(@PageableDefault(size = 10) Pageable pageable,
                                           @RequestParam(defaultValue = "") String englishPhrase,
                                           @RequestParam(defaultValue = "") String vietnamesePhrase) {
        Map<String, String> params = new HashMap<>();
        params.put("englishPhrase", englishPhrase.trim());
        params.put("vietnamesePhrase", vietnamesePhrase.trim());
        Map<String, Object> results = englishPhraseService.getPhrasePagination(pageable, params);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
            englishPhraseService.deleteById(id);
        } catch (Exception e) {
            throw new Exception("Delete Error!");
        }

        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteMany(@RequestParam List<Long> listId) {
        englishPhraseService.deleteMany(listId);
        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }
}

package shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import shop.entity.Movie;
import shop.exceptions.Exception;
import shop.services.MovieService;
import shop.services.ValidationExceptionService;
import shop.validator.MovieValidator;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/movie")
@CrossOrigin
public class MovieController {
    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieValidator movieValidator;

    @Autowired
    private ValidationExceptionService validationExceptionService;

    @GetMapping("")
    public ResponseEntity<?> getAllMovie() {
        List<Movie> listMovie = movieService.findAllOrderBy("name");
        return new ResponseEntity<>(listMovie, HttpStatus.OK);
    }

    @GetMapping("/pagination")
    public ResponseEntity<?> getPagination(@PageableDefault(size = 10) Pageable pageable,
                                           @RequestParam(defaultValue = "") String name,
                                           @RequestParam(defaultValue = "") String description) {
        Map<String, String> params = new HashMap<>();
        params.put("name", name);
        Map<String, Object> results = movieService.getMoviePagination(pageable, params);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<?> create(@Valid @RequestBody Movie movie, BindingResult result, Principal principal) throws Exception {
        movieValidator.validate(movie, result);

        ResponseEntity<?> validateErrors = validationExceptionService.validateException(result);
        if (validateErrors != null) {
            return validateErrors;
        }
        return new ResponseEntity<>(movieService.create(movie, principal), HttpStatus.CREATED);
    }

    @PostMapping("/update")
    public ResponseEntity<?> update(@Valid @RequestBody Movie movie, BindingResult result, Principal principal) throws Exception {
        movieValidator.validate(movie, result);

        ResponseEntity<?> validateErrors = validationExceptionService.validateException(result);
        if (validateErrors != null) {
            return validateErrors;
        }
        return new ResponseEntity<>(movieService.update(movie, principal), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDetail(@PathVariable Long id) {
        return new ResponseEntity<>(movieService.getMovieDetail(id), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
            movieService.deleteById(id);
        } catch (Exception e) {
            throw new Exception("Delete Error!");
        }

        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteMany(@RequestParam List<Long> listId) {
        movieService.deleteMany(listId);
        return new ResponseEntity<>("Delete Success", HttpStatus.OK);
    }

}

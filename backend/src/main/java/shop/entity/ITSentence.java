package shop.entity;

import javax.persistence.Entity;

@Entity
public class ITSentence extends GenericEntity {
    private String sentence;
    private String translation;

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }
}

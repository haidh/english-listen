package shop.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class VietnamesePhrase extends GenericEntity {
    private String name;

    @ManyToMany(mappedBy = "vietnamesePhrases", fetch = FetchType.LAZY)
    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<EnglishPhrase> englishPhrases;

    @Transient
    @JsonProperty
    private Set<String> englishPhrasesSet;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<EnglishPhrase> getEnglishPhrases() {
        return englishPhrases;
    }

    public void setEnglishPhrases(Set<EnglishPhrase> englishPhrases) {
        this.englishPhrases = englishPhrases;
    }

    public Set<String> getEnglishPhrasesSet() {
        return englishPhrasesSet;
    }

    public void setEnglishPhrasesSet(Set<String> englishPhrasesSet) {
        this.englishPhrasesSet = englishPhrasesSet;
    }
}

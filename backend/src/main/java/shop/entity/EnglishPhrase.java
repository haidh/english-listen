package shop.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class EnglishPhrase extends GenericEntity {
    private String name;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(
            name = "english_phrase_vietnamese_phrase",
            joinColumns = @JoinColumn(name = "english_phrase_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "vietnamese_phrase_id", referencedColumnName = "id")
    )
    @LazyCollection(LazyCollectionOption.FALSE)
    @OrderBy("id ASC")
    private Set<VietnamesePhrase> vietnamesePhrases;

    @Transient
    @JsonProperty
    private Set<String> vietnamesePhrasesSet;

    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<VietnamesePhrase> getVietnamesePhrases() {
        return vietnamesePhrases;
    }

    public void setVietnamesePhrases(Set<VietnamesePhrase> vietnamesePhrases) {
        this.vietnamesePhrases = vietnamesePhrases;
    }

    public Set<String> getVietnamesePhrasesSet() {
        return vietnamesePhrasesSet;
    }

    public void setVietnamesePhrasesSet(Set<String> vietnamesePhrasesSet) {
        this.vietnamesePhrasesSet = vietnamesePhrasesSet;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package shop.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Season extends GenericEntity {
    private String name;
    private String description;
    @OneToMany(
            mappedBy = "season",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<MovieSeasonEpisode> movie_seasons_episode = new ArrayList<>();

    @Transient
    @JsonProperty
    private List<Episode> episodes = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MovieSeasonEpisode> getMovie_seasons_episode() {
        return movie_seasons_episode;
    }

    public void setMovie_seasons_episode(List<MovieSeasonEpisode> movie_seasons_episode) {
        this.movie_seasons_episode = movie_seasons_episode;
    }

    public List<Episode> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(List<Episode> episodes) {
        this.episodes = episodes;
    }
}

package shop.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;

@Entity
public class Sentence extends GenericEntity {
    private String sentence;
    private String translation;
    private String audio;

//    @Lob
//    private byte[] data;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "movie_season_episode_id")
    @JsonIgnore
    private MovieSeasonEpisode movieSeasonEpisode;

    @Transient
    @JsonProperty
    private Movie movie;

    @Transient
    @JsonProperty
    private Season season;

    @Transient
    @JsonProperty
    private Episode episode;

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public MovieSeasonEpisode getMovieSeasonEpisode() {
        return movieSeasonEpisode;
    }

    public void setMovieSeasonEpisode(MovieSeasonEpisode movieSeasonEpisode) {
        this.movieSeasonEpisode = movieSeasonEpisode;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public Episode getEpisode() {
        return episode;
    }

    public void setEpisode(Episode episode) {
        this.episode = episode;
    }

//    public byte[] getData() {
//        return data;
//    }
//
//    public void setData(byte[] data) {
//        this.data = data;
//    }
}

package shop.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Episode extends GenericEntity {
    private String name;
    private String description;
    @OneToMany(
            mappedBy = "episode",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<MovieSeasonEpisode> movies_season_episode = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MovieSeasonEpisode> getMovies_season_episode() {
        return movies_season_episode;
    }

    public void setMovies_season_episode(List<MovieSeasonEpisode> movies_season_episode) {
        this.movies_season_episode = movies_season_episode;
    }
}

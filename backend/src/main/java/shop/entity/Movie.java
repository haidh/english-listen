package shop.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Movie extends GenericEntity {
    private String name;
    private String description;
    private String seasonNumber;
    @OneToMany(
            mappedBy = "movie",
            cascade = {CascadeType.ALL},
            orphanRemoval = true
    )
    private List<MovieSeasonEpisode> movie_seasons_episode = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "genre_id")
    @JsonIgnore
    private Genre genre;

    @Transient
    @JsonProperty
    private List<Season> seasons = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MovieSeasonEpisode> getMovie_seasons_episode() {
        return movie_seasons_episode;
    }

    public void setMovie_seasons_episode(List<MovieSeasonEpisode> movie_seasons_episode) {
        this.movie_seasons_episode = movie_seasons_episode;
    }

    public String getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(String seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
}

package shop.condition;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OperationCondition {
    @JsonProperty("operation")
    private List<String> operation;

    public List<String> getOperation() {
        return operation;
    }

    public void setOperation(List<String> operation) {
        this.operation = operation;
    }
}

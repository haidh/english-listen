package shop.dto;

import shop.entity.Movie;

import java.util.Date;
import java.util.List;

public class GenreDTO {
    private Long id;
    private String name;
    private String description;
    private List<MovieDTO> movie;

    public GenreDTO(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MovieDTO> getMovie() {
        return movie;
    }

    public void setMovie(List<MovieDTO> movie) {
        this.movie = movie;
    }
}

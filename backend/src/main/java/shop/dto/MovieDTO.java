package shop.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import shop.entity.GenericEntity;
import shop.entity.Genre;
import shop.entity.MovieSeasonEpisode;
import shop.entity.Season;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MovieDTO extends GenericEntity {
    private Long id;
    private String name;
    private String description;
    private String seasonNumber;

    public MovieDTO(Long id, String name, String description, String seasonNumber) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.seasonNumber = seasonNumber;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(String seasonNumber) {
        this.seasonNumber = seasonNumber;
    }
}

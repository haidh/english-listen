package shop.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import shop.entity.Movie;
import shop.entity.Role;
import shop.entity.Sentence;

@Component
public class SentenceValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Role.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Sentence sentence = (Sentence) o;
        if (sentence.getSentence().length() == 0) {
            errors.rejectValue("sentence", "Required", "Sentence is required");
        }
        if (sentence.getTranslation().length() == 0) {
            errors.rejectValue("translation", "Required", "Translation is required");
        }
    }
}

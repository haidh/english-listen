package shop.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import shop.entity.ITSentence;
import shop.entity.Movie;
import shop.entity.Role;

@Component
public class ITSentenceValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Role.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ITSentence itSentence = (ITSentence) o;
        if (itSentence.getSentence().length() == 0) {
            errors.rejectValue("sentence", "Required", "IT Sentence is required");
        }
        if (itSentence.getTranslation().length() == 0) {
            errors.rejectValue("translation", "Required", "IT Translation is required");
        }
    }
}

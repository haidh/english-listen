package shop.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import shop.entity.Movie;
import shop.entity.Role;

@Component
public class MovieValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Role.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Movie movie = (Movie) o;
        if (movie.getName().length() == 0) {
            errors.rejectValue("name", "Required", "Movie is required");
        }
    }
}

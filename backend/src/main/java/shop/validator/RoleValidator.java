package shop.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import shop.entity.Permission;
import shop.entity.Role;

@Component
public class RoleValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Role.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Role role = (Role) o;
        if (role.getName().length() == 0) {
            errors.rejectValue("name", "Required", "Role is required");
        }
        if (role.getName().length() == 0) {
            errors.rejectValue("name", "Required", "Role is required");
        }
    }
}

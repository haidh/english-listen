package shop.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import shop.entity.EnglishPhrase;
import shop.entity.VietnamesePhrase;

@Component
public class EnglishValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return EnglishPhrase.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        EnglishPhrase englishPhrase = (EnglishPhrase) o;
        if (englishPhrase.getName().length() == 0) {
            errors.rejectValue("name", "Required", "Phrase is required");
        }
        if (englishPhrase.getVietnamesePhrases().size() > 0) {
            for (VietnamesePhrase vietnamesePhrase : englishPhrase.getVietnamesePhrases()) {
                if (vietnamesePhrase.getName().length() == 0) {
                    errors.rejectValue("vietnamesePhrases", "Required", "Translation is required");
                }
            }
        }
    }
}

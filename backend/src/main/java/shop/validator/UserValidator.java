package shop.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import shop.entity.User;

@Component
public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;
        if (user.getUsername().length() == 0) {
            errors.rejectValue("username", "Required", "Username is required");
        }

        if (user.getUsername().length() < 4) {
            errors.rejectValue("username", "Length", "Username must be at least 4 characters");
        }

        if (user.getPassword() != null && user.getPassword().length() == 0) {
            errors.rejectValue("password", "Required", "Password is required");
        }

        if (user.getPassword() != null && user.getPassword().length() < 6) {
            errors.rejectValue("password", "Length", "Password must be at least 6 characters");
        }
    }
}

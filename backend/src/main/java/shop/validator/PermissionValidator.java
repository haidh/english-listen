package shop.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import shop.entity.Permission;

@Component
public class PermissionValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Permission.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Permission permission = (Permission) o;
        if (permission.getName().length() == 0) {
            errors.rejectValue("name", "Required", "Permission is required");
        }
    }
}

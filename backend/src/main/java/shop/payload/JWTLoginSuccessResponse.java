package shop.payload;

import java.util.Set;

public class JWTLoginSuccessResponse {
    private String token;
    private String type = "Bearer";
    private Long id;
    private String username;
    private Set<String> operations;
    private String sessionId;

    public JWTLoginSuccessResponse(String token, Long id, String username) {
        this.token = token;
        this.id = id;
        this.username = username;
    }

    public JWTLoginSuccessResponse(String token, Long id, String username, Set<String> operations, String sessionId) {
        this.token = token;
        this.type = type;
        this.id = id;
        this.username = username;
        this.operations = operations;
        this.sessionId = sessionId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<String> getRole() {
        return operations;
    }

    public void setRole(Set<String> role) {
        this.operations = role;
    }

    public Set<String> getOperations() {
        return operations;
    }

    public void setOperations(Set<String> operations) {
        this.operations = operations;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String toString() {
        return "JWTLoginSuccessResponse{" +
                "token='" + token + '\'' +
                ", type='" + type + '\'' +
                ", id=" + id +
                ", username='" + username + '\'' +
                ", role=" + operations +
                '}';
    }
}

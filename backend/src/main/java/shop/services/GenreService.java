package shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import shop.entity.*;
import shop.exceptions.Exception;
import shop.repository.GenreRepository;
import shop.repository.MovieRepository;
import shop.repository.MovieSeasonEpisodeRepository;
import shop.repository.SentenceRepository;
import shop.search_specification.MovieSpecification;
import shop.search_specification.SearchCriteria;
import shop.search_specification.SearchOperation;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.security.Principal;
import java.util.*;

@Service
public class GenreService extends GenericService<Genre, Long> {
    @Autowired
    private GenreRepository genreRepository;

    public Optional<Genre> findGenreById(Long id) {
        return genreRepository.findById(id);
    }


}

package shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import shop.entity.EnglishPhrase;
import shop.entity.Sentence;
import shop.entity.VietnamesePhrase;
import shop.exceptions.Exception;
import shop.repository.EnglishPhraseRepository;
import shop.repository.VietnamesePhraseRepository;
import shop.search_specification.EnglishPhraseSpecification;
import shop.search_specification.SearchCriteria;
import shop.search_specification.SearchOperation;
import shop.search_specification.SentenceSpecification;

import java.security.Principal;
import java.util.*;

@Service
public class EnglishPhraseService extends GenericService<EnglishPhrase, Long> {
    @Autowired
    private EnglishPhraseRepository englishPhraseRepository;

    @Autowired
    private VietnamesePhraseRepository vietnamesePhraseRepository;

    public EnglishPhrase getPhrases(String name) {
        EnglishPhrase phrase = englishPhraseRepository.findEnglishPhraseByName(name);
        Set<String> vietnamesePhrasesSet = new HashSet<>();
        if (phrase != null && phrase.getVietnamesePhrases().size() != 0) {
            for (VietnamesePhrase vietnamesePhrase : phrase.getVietnamesePhrases()) {
                vietnamesePhrasesSet.add(vietnamesePhrase.getName());
            }
            phrase.setVietnamesePhrasesSet(vietnamesePhrasesSet);
        } else {
            phrase = new EnglishPhrase();
        }
        return phrase;
    }

    public EnglishPhrase create(EnglishPhrase englishPhrase, Principal principal) {
        if (englishPhrase.getId() != null) {
            if (englishPhraseRepository.checkExistedNotById(englishPhrase.getName(), englishPhrase.getId())) {
                throw new Exception("Phrase is existed!");
            }
        } else {
            if (englishPhraseRepository.checkExistedByName(englishPhrase.getName())) {
                throw new Exception("Phrase is existed!");
            }
        }
        String username = principal.getName();
        englishPhrase.setCreated_by(username);
        Set<VietnamesePhrase> vietnamesePhraseHashSet = new HashSet<>();
        for (VietnamesePhrase vietnamesePhrase : englishPhrase.getVietnamesePhrases()) {
            if (vietnamesePhraseRepository.checkExistedByName(vietnamesePhrase.getName())) {
                VietnamesePhrase vietnamesePhraseExisted = vietnamesePhraseRepository.findVietnamesePhraseByName(vietnamesePhrase.getName());
                vietnamesePhraseHashSet.add(vietnamesePhraseExisted);
            } else {
                vietnamesePhraseHashSet.add(vietnamesePhrase);
            }
        }
        englishPhrase.setVietnamesePhrases(vietnamesePhraseHashSet);
        return englishPhraseRepository.save(englishPhrase);
    }

    public Map<String, Object> getPhrasePagination(Pageable pageable, Map<String, String> params) {
        EnglishPhraseSpecification englishPhraseSpecification = new EnglishPhraseSpecification();
        Map<String, Object> results = new HashMap<>();
        Page<EnglishPhrase> pages = null;
        if (params.containsKey("englishPhrase") && !params.get("englishPhrase").equals("")
                && params.containsKey("vietnamesePhrase") && !params.get("vietnamesePhrase").equals("")) {
            String vietnamesePhrase = "[[:<:]]" + params.get("vietnamesePhrase").trim().toLowerCase() + "[[:>:]]";
            String phrase = "[[:<:]]" + params.get("englishPhrase").trim().toLowerCase() + "[[:>:]]";
            pages = englishPhraseRepository.getEnglishVietnamesePhrases(phrase, vietnamesePhrase, pageable);
        } else {
            if (params.containsKey("englishPhrase") && !params.get("englishPhrase").equals("")) {
                englishPhraseSpecification.add(new SearchCriteria("name", params.get("englishPhrase"), SearchOperation.MATCH));
                String phrase = "[[:<:]]" + params.get("englishPhrase").trim().toLowerCase() + "[[:>:]]";
                pages = englishPhraseRepository.getPhrases(phrase, pageable);
            } else if (params.containsKey("vietnamesePhrase") && !params.get("vietnamesePhrase").equals("")) {
                String vietnamesePhrase = "[[:<:]]" + params.get("vietnamesePhrase").trim().toLowerCase() + "[[:>:]]";
                pages = englishPhraseRepository.getVietnamesePhrases(vietnamesePhrase, pageable);
            } else {
                pages = englishPhraseRepository.findAll(englishPhraseSpecification, pageable);
            }
        }

        results.put("phrases", pages.getContent());
        results.put("totalElements", pages.getTotalElements());
        results.put("page", pages.getPageable().getPageNumber());
        results.put("totalPages", pages.getTotalPages());

        return results;
    }

    public void deleteMany(List<Long> listId) {
        try {
            englishPhraseRepository.deleteEnglishPhraseByIdIn(listId);
        } catch (Exception e) {
            throw new Exception("Delete Error!");
        }
    }


    @Override
    public Optional<EnglishPhrase> findById(Long id) {
        if (!englishPhraseRepository.checkExistedById(id)) {
            throw new Exception("Phrase is not existed!");
        }
        return super.findById(id);
    }
}

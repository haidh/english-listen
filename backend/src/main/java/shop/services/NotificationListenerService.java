package shop.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.orm.hibernate5.SpringSessionContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import shop.entity.Notification;

import java.security.Principal;

@Service
public class NotificationListenerService {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @RabbitListener(queues = "user.notification")
    public void receiveNotification(String message) throws JsonProcessingException {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Notification notification = objectMapper.readValue(message, Notification.class);
        this.simpMessagingTemplate.convertAndSendToUser("admin","/queue/deleteMessage", notification);
    }
}

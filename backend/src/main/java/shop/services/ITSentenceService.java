package shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import shop.entity.ITSentence;
import shop.entity.Sentence;
import shop.exceptions.Exception;
import shop.repository.ITSentenceRepository;
import shop.search_specification.ITSentenceSpecification;
import shop.search_specification.SearchCriteria;
import shop.search_specification.SearchOperation;
import shop.search_specification.SentenceSpecification;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ITSentenceService extends GenericService<ITSentence, Long> {
    @Autowired
    private ITSentenceRepository itSentenceRepository;


    public ITSentence create(ITSentence itSentence, Principal principal) {
        if (itSentence.getSentence().length() == 0) {
            throw new Exception("IT Sentence is required");
        }
        if (itSentence.getTranslation().length() == 0) {
            throw new Exception("Translation is required");
        }

        if (itSentenceRepository.checkExistedBySentence(itSentence.getSentence())) {
            throw new Exception("IT Sentence is existed!");
        }
        String username = principal.getName();
        itSentence.setCreated_by(username);
        return itSentenceRepository.save(itSentence);
    }

    public ITSentence update(ITSentence itSentence, Principal principal) {
        if (itSentence.getSentence().length() == 0) {
            throw new Exception("Sentence is required");
        }
        if (itSentence.getTranslation().length() == 0) {
            throw new Exception("Translation is required");
        }

        if (itSentenceRepository.checkExistedNotById(itSentence.getSentence(), itSentence.getId())) {
            throw new Exception("Sentence is existed!");
        }
        String username = principal.getName();
        itSentence.setUpdated_by(username);
        return itSentenceRepository.save(itSentence);
    }


    public Map<String, Object> getITSentencePagination(Pageable pageable, Map<String, String> params) {
        ITSentenceSpecification itSentenceSpecification = new ITSentenceSpecification();
        if (params.containsKey("sentence")) {
            itSentenceSpecification.add(new SearchCriteria("sentence", params.get("sentence"), SearchOperation.MATCH));
        }
        if (params.containsKey("translation")) {
            itSentenceSpecification.add(new SearchCriteria("translation", params.get("translation"), SearchOperation.MATCH));
        }


        Page<Sentence> pages = itSentenceRepository.findAll(itSentenceSpecification, pageable);
        Map<String, Object> results = new HashMap<>();
        results.put("itSentences", pages.getContent());
        results.put("totalElements", pages.getTotalElements());
        results.put("page", pages.getPageable().getPageNumber());
        results.put("totalPages", pages.getTotalPages());
        return results;
    }

    public void deleteMany(List<Long> listId) {
        try {
            itSentenceRepository.deleteByIdIn(listId);
        } catch (Exception e) {
            throw new Exception("Delete Error!");
        }
    }


}

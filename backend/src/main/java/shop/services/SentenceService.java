package shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import shop.entity.*;
import shop.exceptions.Exception;
import shop.repository.MovieSeasonEpisodeRepository;
import shop.repository.SentenceRepository;
import shop.search_specification.SearchCriteria;
import shop.search_specification.SearchOperation;
import shop.search_specification.SentenceSpecification;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.*;

@Service
public class SentenceService extends GenericService<Sentence, Long> {
    @Autowired
    private SentenceRepository sentenceRepository;
    @Autowired
    private MovieSeasonEpisodeRepository movieSeasonEpisodeRepository;

    public Map<String, Object> getSentencePagination(Pageable pageable, Map<String, String> params) throws IOException {
        SentenceSpecification sentenceSpecification = new SentenceSpecification();
        Page<Sentence> pages = null;
        if (params.containsKey("movieId") && !params.get("movieId").equals("")) {
            pageable = PageRequest.of(Integer.parseInt(params.get("page")), Integer.parseInt(params.get("size")), Sort.by("id").ascending());
            List<MovieSeasonEpisode> movieSeasonEpisodes = movieSeasonEpisodeRepository.findMovieSeasonEpisodeByMovieId(Long.parseLong(params.get("movieId")));
            List<Long> movieSeasonEpisodeId = new ArrayList<>();
            movieSeasonEpisodes.forEach(movieSeasonEpisode -> {
                movieSeasonEpisodeId.add(movieSeasonEpisode.getId());
            });
            if (params.containsKey("sentence") && !params.get("sentence").equals("") &&
                    params.containsKey("translation") && !params.get("translation").equals("")) {
                String sentence = "[[:<:]]" + params.get("sentence").trim().toLowerCase() + "[[:>:]]";
                String translation = "[[:<:]]" + params.get("translation").trim().toLowerCase() + "[[:>:]]";
                pages = sentenceRepository.getSentenceBySearchSentenceAndTranslationAndMovie(sentence, translation, movieSeasonEpisodeId, pageable);
            } else {
                if (params.containsKey("sentence") && !params.get("sentence").equals("")) {
                    String sentence = "[[:<:]]" + params.get("sentence").trim().toLowerCase() + "[[:>:]]";
                    pages = sentenceRepository.getSentenceBySearchSentenceAndMovie(sentence, movieSeasonEpisodeId, pageable);
                } else if (params.containsKey("translation") && !params.get("translation").equals("")) {
                    String translation = "[[:<:]]" + params.get("translation").trim().toLowerCase() + "[[:>:]]";
                    pages = sentenceRepository.getSentenceBySearchTranslationAndMovie(translation, movieSeasonEpisodeId, pageable);
                } else {
                    pages = sentenceRepository.findAllByMovieId(movieSeasonEpisodeId, pageable);
                }
            }
        } else {
            if (params.containsKey("sentence") && !params.get("sentence").equals("") &&
                    params.containsKey("translation") && !params.get("translation").equals("")) {
                String sentence = "[[:<:]]" + params.get("sentence").trim().toLowerCase() + "[[:>:]]";
                String translation = "[[:<:]]" + params.get("translation").trim().toLowerCase() + "[[:>:]]";
                pages = sentenceRepository.getSentenceBySearchSentenceAndTranslation(sentence, translation, pageable);
            } else {
                if (params.containsKey("sentence") && !params.get("sentence").equals("")) {
                    String sentence = "[[:<:]]" + params.get("sentence").trim().toLowerCase() + "[[:>:]]";
                    pages = sentenceRepository.getSentenceBySearchSentence(sentence, pageable);
                } else if (params.containsKey("translation") && !params.get("translation").equals("")) {
                    String translation = "[[:<:]]" + params.get("translation").trim().toLowerCase() + "[[:>:]]";
                    pages = sentenceRepository.getSentenceBySearchTranslation(translation, pageable);
                } else {
                    pages = sentenceRepository.findAll(sentenceSpecification, pageable);
                }
            }
        }

//
//        File fileToGetPath = new File("");
//        for (Sentence s : pages.getContent()) {
//            if (s.getData() != null && s.getData().length > 0) {
//                String filePath = fileToGetPath.getAbsolutePath().substring(0, fileToGetPath.getAbsolutePath().lastIndexOf("english-listen"));
//                String directoryPath = filePath + "\\english-audio";
//                Files.createDirectories(Paths.get(directoryPath));
//                File file = new File(directoryPath + "\\" + s.getAudio());
//                if (!file.exists()) {
//                    FileOutputStream fos = new FileOutputStream(file);
//                    fos.write(s.getData());
//                    fos.close();
//                }
//            }
//        }
        Map<String, Object> results = new HashMap<>();
        results.put("sentences", pages.getContent());
        results.put("totalElements", pages.getTotalElements());
        results.put("page", pages.getPageable().getPageNumber());
        results.put("totalPages", pages.getTotalPages());
        return results;
    }

    public Sentence create(Sentence sentenceSave, Principal principal) {

        if (sentenceSave.getSentence().length() == 0) {
            throw new Exception("Sentence is required");
        }
        if (sentenceSave.getTranslation().length() == 0) {
            throw new Exception("Translation is required");
        }

        if (sentenceRepository.checkExisted(sentenceSave.getSentence())) {
            throw new Exception("Sentence is existed!");
        }
        String username = principal.getName();
        sentenceSave.setCreated_by(username);
        return sentenceRepository.save(sentenceSave);
    }

    public Sentence update(Sentence sentenceSave, Principal principal) {
        if (sentenceSave.getSentence().length() == 0) {
            throw new Exception("Sentence is required");
        }
        if (sentenceSave.getTranslation().length() == 0) {
            throw new Exception("Translation is required");
        }

        if (sentenceRepository.checkExistedNotById(sentenceSave.getSentence(), sentenceSave.getId(), sentenceSave.getMovieSeasonEpisode().getId())) {
//        if (sentenceRepository.checkExistedNotById(sentenceSave.getSentence(), sentenceSave.getId())) {
            throw new Exception("Sentence is existed!");
        }
        String username = principal.getName();
        sentenceSave.setUpdated_by(username);
        return sentenceRepository.save(sentenceSave);
    }

    public Sentence getSentenceDetail(Long id) throws IllegalAccessException {
        Object object = sentenceRepository.getSentenceDetail(id);
        Object[] objectResult = (Object[]) object;
        Sentence sentence = new Sentence();
        sentence.setId(Long.parseLong(objectResult[0].toString()));
        if (objectResult[1] != null) {
            sentence.setSentence(objectResult[1].toString());
        }
        if (objectResult[2] != null) {
            sentence.setTranslation(objectResult[2].toString());
        }
        if (objectResult[3] != null) {
            sentence.setAudio(objectResult[3].toString());
        }


        Episode episode = new Episode();
        episode.setId(Long.parseLong(objectResult[8].toString()));
        episode.setName(objectResult[9].toString());

        Season season = new Season();
        season.setId(Long.parseLong(objectResult[6].toString()));
        season.setName(objectResult[7].toString());

        Movie movie = new Movie();
        movie.setId(Long.parseLong(objectResult[4].toString()));
        movie.setName(objectResult[5].toString());

        sentence.setMovie(movie);
        sentence.setSeason(season);
        sentence.setEpisode(episode);
        return sentence;
    }

    public void deleteMany(List<Long> listId) {
        try {
            sentenceRepository.deleteByIdIn(listId);
        } catch (Exception e) {
            throw new Exception("Delete Error!");
        }
    }

    public Sentence findSentenceByAudio(String audio) {
        return sentenceRepository.findSentenceByAudio(audio);
    }
}

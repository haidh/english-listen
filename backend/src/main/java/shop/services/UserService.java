package shop.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import shop.entity.*;
import shop.exceptions.Exception;
import shop.repository.OperationRepository;
import shop.repository.PermissionRepository;
import shop.repository.RoleRepository;
import shop.repository.UserRepository;

import java.security.Principal;
import java.util.*;

@Service
@Transactional
public class UserService extends GenericService<User, Long> {
    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private RabbitTemplate rabbitTemplate;
    private ObjectMapper objectMapper = new ObjectMapper();

    public User register(User user) {
        try {
            if (userRepository.checkUsernameExisted(user.getUsername())) {
                throw new Exception("Username is existed!");
            }
            if (userRepository.checkEmailExisted(user.getUsername())) {
                throw new Exception("User Email is existed");
            }

            String passwordEncoder = bCryptPasswordEncoder.encode(user.getPassword());
            user.setPassword(passwordEncoder);
            user.setConfirmPassword(passwordEncoder);

            if (user.getUsername().equals("admin")) {
                List<Operation> operations = operationRepository.findAll();
                Permission adminPermission = new Permission();
                adminPermission.setName("ADMIN PERMISSION");
                adminPermission.setDescription("ADMIN PERMISSION");
                Set<Operation> operationSet = new HashSet<>();
                for (Operation operation : operations) {
                    operationSet.add(operation);
                }
                adminPermission.setOperations(operationSet);
                permissionRepository.save(adminPermission);

                Set<Permission> permissions = new HashSet<>();
                permissions.add(adminPermission);
                Role adminRole = new Role();
                adminRole.setName("ADMIN ROLE");
                adminRole.setDescription("ADMIN ROLE");
                adminRole.setPermissions(permissions);

                roleRepository.save(adminRole);
                Set<Role> roles = new HashSet<>();
                roles.add(adminRole);
                user.setRoles(roles);
            }
            return userRepository.save(user);
        } catch (Exception e) {
            throw new Exception("Username and Email are existed!");
        }
    }

    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    public Map<String, Object> getUserPagination(Pageable pageable, Map<String, String> params) {
        String username = "%" + params.get("username") + "%";
        String fullName = "%" + params.get("fullName") + "%";
        String email = "%" + params.get("email") + "%";
        Page<User> userPage = userRepository.findByUsernameLikeAndFullNameLikeAndEmailLike(username, fullName, email, pageable);
        Map<String, Object> results = new HashMap<>();
        results.put("users", userPage.getContent());
        results.put("totalElements", userPage.getTotalElements());
        results.put("page", userPage.getPageable().getPageNumber());
        results.put("totalPages", userPage.getTotalPages());
        return results;
    }


    public User create(User user, Principal principal) {
        if (user.getId() != null) {
            if (userRepository.checkExistedNotById(user.getUsername(), user.getId())) {
                throw new Exception("Username is existed!");
            }
            if (userRepository.checkEmailExistedNotById(user.getEmail(), user.getId())) {
                throw new Exception("Username is existed!");
            }
        } else {
            if (userRepository.checkUsernameExisted(user.getUsername())) {
                throw new Exception("Username is existed!");
            }
            if (userRepository.checkEmailExisted(user.getEmail())) {
                throw new Exception("Email is existed!");
            }
        }

        String username = principal.getName();
        user.setCreated_by(username);
        return userRepository.save(user);
    }

    public void deleteById(Long id, Principal principal) throws JsonProcessingException {
        String username = principal.getName();
        User user;
        try {
            user = userRepository.findUserById(id);
        } catch (Exception e) {
            throw new Exception("User is not existed");
        }
        userRepository.deleteById(id);

        Notification notification = new Notification();
        notification.setTitle("User Notification");
        notification.setStatus("Delete");
        notification.setMessage(username + " has just deleted the user " + user.getUsername());
        String notificationJson = objectMapper.writeValueAsString(notification);
        rabbitTemplate.convertAndSend("user.notification", notificationJson);
    }

    public void deleteMany(List<Long> listId) {
        try {
            userRepository.deleteByIdIn(listId);
        } catch (Exception e) {
            throw new Exception("Delete Error!");
        }

    }
}

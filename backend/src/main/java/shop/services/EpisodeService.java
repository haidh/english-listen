package shop.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import shop.entity.Episode;
import shop.entity.Movie;
import shop.entity.Notification;
import shop.entity.Role;
import shop.exceptions.Exception;
import shop.repository.EpisodeRepository;
import shop.repository.MovieRepository;
import shop.search_specification.EpisodeSpecification;
import shop.search_specification.MovieSpecification;
import shop.search_specification.SearchCriteria;
import shop.search_specification.SearchOperation;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EpisodeService extends GenericService<Episode, Long> {
    @Autowired
    private EpisodeRepository episodeRepository;

    public List<Episode> getEpisodeByMovieSeason(Long movieId, Long seasonId) {
        return episodeRepository.getEpisodeByMovieSeason(movieId,seasonId);
    }
}

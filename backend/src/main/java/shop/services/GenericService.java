package shop.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import shop.entity.GenericEntity;
import shop.exceptions.Exception;
import shop.repository.GenericRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class GenericService<T extends GenericEntity, ID extends Serializable> {
    private static Logger logger = LoggerFactory.getLogger(GenericService.class);

    @Autowired
    protected GenericRepository<T, ID> genericRepository;

    public T create(T entity) {
        return genericRepository.save(entity);
    }

    public T update(T entity, ID id) {
        Optional<T> oldEntity = findById(id);
        if (oldEntity == null) {
            throw new EntityNotFoundException("No entity with id " + id);
        }
        return genericRepository.save(entity);
    }

    public List<T> findAll() {
        return genericRepository.findAll();
    }

    public List<T> findAllOrderBy(String columnName) {
        return genericRepository.findAll(Sort.by(Sort.Direction.ASC,columnName));
    }

    @Transactional
    public List<T> saveAll(List<T> list) {
        return genericRepository.saveAll(list);
    }


    public List<T> findAll(Specification specification) {
        return genericRepository.findAll(specification);
    }

    public Page<T> findAll(Specification specification, Pageable pageable) {
        return genericRepository.findAll(specification, pageable);
    }

    public Optional<T> findById(ID id) {
        return genericRepository.findById(id);
    }

    public void deleteById(ID id) {
        genericRepository.deleteById(id);
    }

    public void delete(T entity) {
        genericRepository.delete(entity);
    }

}

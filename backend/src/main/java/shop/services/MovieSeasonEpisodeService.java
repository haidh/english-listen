package shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import shop.entity.Episode;
import shop.entity.Movie;
import shop.entity.MovieSeasonEpisode;
import shop.entity.Season;
import shop.exceptions.Exception;
import shop.repository.MovieRepository;
import shop.repository.MovieSeasonEpisodeRepository;
import shop.search_specification.MovieSpecification;
import shop.search_specification.SearchCriteria;
import shop.search_specification.SearchOperation;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MovieSeasonEpisodeService extends GenericService<Movie, Long> {
    @Autowired
    private MovieSeasonEpisodeRepository movieSeasonEpisodeRepository;

    public MovieSeasonEpisode getMovieSeasonEpisode(Long movieId, Long seasonId, Long episodeId) {
        try{
            return movieSeasonEpisodeRepository.findMovieSeasonEpisodeByMovieAndSeasonAndEpisode(movieId, seasonId, episodeId);
        }catch (Exception e) {
            throw new Exception("Not find Movie Season Episode");
        }
    }
}

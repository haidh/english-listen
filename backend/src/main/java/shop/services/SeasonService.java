package shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shop.entity.Season;
import shop.repository.SeasonRepository;

import java.util.List;

@Service
public class SeasonService extends GenericService<Season, Long> {
    @Autowired
    private SeasonRepository seasonRepository;

    public List<Season> getSeasonByMovie(Long id) {
        return seasonRepository.getSeasonByMovie(id);
    }
}

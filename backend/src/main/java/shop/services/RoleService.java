package shop.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import shop.entity.Notification;
import shop.entity.Role;
import shop.exceptions.Exception;
import shop.repository.RoleRepository;
import shop.search_specification.RoleSpecification;
import shop.search_specification.SearchCriteria;
import shop.search_specification.SearchOperation;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class RoleService extends GenericService<Role, Long> {
    @Autowired
    private RoleRepository roleRepository;

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    private RabbitTemplate rabbitTemplate;
    private ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger log = LoggerFactory.getLogger(RoleService.class);

    public Map<String, Object> getRolePagination(Pageable pageable, Map<String, String> params) {
        RoleSpecification roleSpecification = new RoleSpecification();
        if (params.containsKey("name")) {
            roleSpecification.add(new SearchCriteria("name", params.get("name"), SearchOperation.MATCH));
        }
        if (params.containsKey("description")) {
            roleSpecification.add(new SearchCriteria("description", params.get("description"), SearchOperation.MATCH));
        }

        Page<Role> rolePage = roleRepository.findAll(roleSpecification, pageable);
        Map<String, Object> results = new HashMap<>();
        results.put("roles", rolePage.getContent());
        results.put("totalElements", rolePage.getTotalElements());
        results.put("page", rolePage.getPageable().getPageNumber());
        results.put("totalPages", rolePage.getTotalPages());
        return results;
    }

    public Role create(Role role, Principal principal) {
        if (role.getId() != null) {
            if (roleRepository.checkExistedNotById(role.getName(), role.getId())) {
                throw new Exception("Role Name is existed!");
            }
        } else {
            if (roleRepository.checkExistedByName(role.getName())) {
                throw new Exception("Role Name is existed!");
            }
        }

        String username = principal.getName();
        role.setCreated_by(username);
        return roleRepository.save(role);
    }

    @Override
    public Optional<Role> findById(Long id) {
        if (!roleRepository.checkExistedById(id)) {
            throw new Exception("Role is not existed!");
        }
        return super.findById(id);
    }

    public void deleteMany(List<Long> listId) {
        try {
            roleRepository.deleteByIdIn(listId);
        } catch (Exception e) {
            throw new Exception("Delete Error!");
        }
    }

    public Role findRoleById(Long id) {
        try {
            return roleRepository.findRoleById(id);
        } catch (Exception e) {
            throw new Exception("Role is not existed");
        }

    }

    public void deleteById(Long id, Principal principal) throws JsonProcessingException {
        String username = principal.getName();
        Role role;
        try {
            role = roleRepository.findRoleById(id);
        } catch (Exception e) {
            throw new Exception("Role is not existed");
        }

        roleRepository.deleteById(id);

        Notification notification = new Notification();
        notification.setTitle("User Notification");
        notification.setStatus("Delete");
        notification.setMessage(username + " has just deleted the role " + role.getName());
        String notificationJson = objectMapper.writeValueAsString(notification);
        rabbitTemplate.convertAndSend("user.notification", notificationJson);
    }
}

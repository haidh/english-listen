package shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import shop.entity.*;
import shop.exceptions.Exception;
import shop.repository.MovieRepository;
import shop.repository.MovieSeasonEpisodeRepository;
import shop.repository.SentenceRepository;
import shop.search_specification.MovieSpecification;
import shop.search_specification.SearchCriteria;
import shop.search_specification.SearchOperation;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MovieService extends GenericService<Movie, Long> {
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private SentenceRepository sentenceRepository;
    @Autowired
    private MovieSeasonEpisodeRepository movieSeasonEpisodeRepository;
    @PersistenceContext
    EntityManager entityManager;

    public Map<String, Object> getMoviePagination(Pageable pageable, Map<String, String> params) {
        MovieSpecification movieSpecification = new MovieSpecification();
        if (params.containsKey("name")) {
            movieSpecification.add(new SearchCriteria("name", params.get("name"), SearchOperation.MATCH));
        }

        Page<Movie> pages = movieRepository.findAll(movieSpecification, pageable);
        Map<String, Object> results = new HashMap<>();
        results.put("movies", pages.getContent());
        results.put("totalElements", pages.getTotalElements());
        results.put("page", pages.getPageable().getPageNumber());
        results.put("totalPages", pages.getTotalPages());
        return results;
    }

    public Movie create(Movie movie, Principal principal) {
        if (movieRepository.checkExistedByName(movie.getName())) {
            throw new Exception("Movie is existed!");
        }

        List<MovieSeasonEpisode> listMovieSeasonEpisodes = new ArrayList<>();

        for (Season season : movie.getSeasons()) {
            for (Episode episode : season.getEpisodes()) {
                MovieSeasonEpisode movieSeasonEpisode = new MovieSeasonEpisode();
                movieSeasonEpisode.setMovie(movie);
                movieSeasonEpisode.setSeason(season);
                movieSeasonEpisode.setEpisode(episode);
                listMovieSeasonEpisodes.add(movieSeasonEpisode);
            }
        }
        movie.setMovie_seasons_episode(listMovieSeasonEpisodes);

        return movieRepository.save(movie);
    }

    public Movie update(Movie movie, Principal principal) {
        if (movieRepository.checkExistedNotById(movie.getName(), movie.getId())) {
            throw new Exception("Movie is existed!");
        }


        List<MovieSeasonEpisode> listMovieSeasonEpisodes = new ArrayList<>();

        for (Season season : movie.getSeasons()) {
            for (Episode episode : season.getEpisodes()) {
                MovieSeasonEpisode movieSeasonEpisode = movieSeasonEpisodeRepository.findMovieSeasonEpisodeByMovieAndSeasonAndEpisode(movie.getId(), season.getId(), episode.getId());
                if (movieSeasonEpisode == null) {
                    movieSeasonEpisode = new MovieSeasonEpisode();
                }
                movieSeasonEpisode.setMovie(movie);
                movieSeasonEpisode.setSeason(season);
                movieSeasonEpisode.setEpisode(episode);
                listMovieSeasonEpisodes.add(movieSeasonEpisode);
            }
        }
        movie.setMovie_seasons_episode(listMovieSeasonEpisodes);

        return movieRepository.save(movie);
    }

    public void deleteMany(List<Long> listId) {
        try {
            movieRepository.deleteByIdIn(listId);
        } catch (Exception e) {
            throw new Exception("Delete Error!");
        }
    }

    public Movie getMovieDetail(Long id) {
        String queryString = "SELECT\n" +
                "\tmovie.id,\n" +
                "\tmovie.NAME,\n" +
                "\tmovie.description,\n" +
                "\tseason.id AS season_id,\n" +
                "\tseason.NAME AS season_name,\n" +
                "\tepisode.id AS episode_id,\n" +
                "\tepisode.NAME AS episode_name \n" +
                "FROM\n" +
                "\tmovie\n" +
                "\tLEFT JOIN movie_season_episode ON movie.id = movie_season_episode.movie_id\n" +
                "\tLEFT JOIN season ON season.id = movie_season_episode.season_id\n" +
                "\tLEFT JOIN episode ON episode.id = movie_season_episode.episode_id \n" +
                "WHERE\n" +
                "\tmovie.id = ?1";
        Query query = entityManager.createNativeQuery(queryString);
        query.setParameter(1, id);
        List<Object[]> listResult = query.getResultList();
        Movie movieUpdate = new Movie();
        List<Season> listSeason = new ArrayList<>();


        for (Object[] result : listResult) {
            Long movieId = Long.parseLong(result[0].toString());
            movieUpdate.setId(movieId);
            movieUpdate.setName(result[1].toString());
            movieUpdate.setDescription(result[2].toString());
            Season season = new Season();
            if (listSeason.stream().anyMatch(s -> s.getId() == Long.parseLong(result[3].toString()))) {
                List<Season> listSeasonUpdate = movieUpdate.getSeasons();
                for (Season s : listSeasonUpdate) {
                    if (s.getId() == Long.parseLong(result[3].toString())) {
                        Episode episode = new Episode();
                        episode.setId((Long.parseLong(result[5].toString())));
                        episode.setName(result[6].toString());
                        List<Episode> listEpisode = s.getEpisodes();
                        listEpisode.add(episode);
                        s.setEpisodes(listEpisode);

                    }
                }
                movieUpdate.setSeasons(listSeasonUpdate);
            } else {
                List<Episode> listEpisode = new ArrayList<>();
                if (result[3] != null) {
                    season.setId(Long.parseLong(result[3].toString()));
                }
                if (result[4] != null) {
                    season.setName(result[4].toString());
                }

                Episode episode = new Episode();
                if (result[5] != null) {
                    episode.setId((Long.parseLong(result[5].toString())));
                }
                if (result[6] != null) {
                    episode.setName(result[6].toString());
                }
                if (episode != null) {
                    listEpisode.add(episode);
                    season.setEpisodes(listEpisode);
                }

                if (season.getId() != null) {
                    listSeason.add(season);
                    movieUpdate.setSeasons(listSeason);
                }
            }
        }
        return movieUpdate;
    }
}

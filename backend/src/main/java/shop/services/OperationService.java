package shop.services;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shop.condition.OperationCondition;
import shop.entity.Operation;
import shop.repository.OperationRepository;
import shop.search_specification.OperationSpecification;
import shop.search_specification.SearchCriteria;
import shop.search_specification.SearchOperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OperationService extends GenericService<Operation, Long> {
    @Autowired
    private OperationRepository operationRepository;

    public String getAllOperation() {
        List<Operation> listOperations = operationRepository.findAll();
        JsonArray results = new JsonArray();
        for (Operation operation : listOperations) {
            List<String> listObject = new ArrayList<>();
            for (int i = 0; i < results.size(); i++) {
                JsonObject asJsonObject = results.get(i).getAsJsonObject();
                String asString = asJsonObject.get("label").getAsString();
                listObject.add(asString);
            }

            String[] stringArray = operation.getOperationKey().split("_");
            String parentString = stringArray[0];
            if (!listObject.contains(parentString)) {
                JsonObject parentObject = new JsonObject();
                parentObject.addProperty("label", parentString);
                parentObject.addProperty("value", parentString);
                JsonArray childArray = new JsonArray();
                if (operation.getOperationKey().contains(parentString)) {
                    JsonObject childObject = new JsonObject();
                    childObject.addProperty("id", operation.getId());
                    childObject.addProperty("label", operation.getName());
                    childObject.addProperty("value", operation.getOperationKey());
                    childArray.add(childObject);
                    parentObject.add("children", childArray);
                    results.add(parentObject);
                }
            } else {
                for (int i = 0; i < results.size(); i++) {
                    JsonObject asJsonObject = results.get(i).getAsJsonObject();
                    for (Map.Entry<String, JsonElement> entry : asJsonObject.entrySet()) {
                        if (entry.getKey().equals("label") && entry.getValue().toString().replace("\"", "").equals(parentString.toString())) {
                            JsonArray childArray = asJsonObject.getAsJsonArray("children");
                            JsonObject childObject = new JsonObject();
                            childObject.addProperty("id", operation.getId());
                            childObject.addProperty("label", operation.getName());
                            childObject.addProperty("value", operation.getOperationKey());
                            childArray.add(childObject);
                        }
                    }
                }
            }

        }
        return results.toString();
    }

    public List<Operation> getOperationByCondition(OperationCondition operationCondition) {
        List<Operation> listOperation = operationRepository.findOperationIN(operationCondition.getOperation());
        return listOperation;
    }
}

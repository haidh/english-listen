package shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shop.entity.EnglishPhrase;
import shop.entity.VietnamesePhrase;
import shop.repository.VietnamesePhraseRepository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service
public class VietnamesePhraseService extends GenericService<VietnamesePhrase, Long> {
    @Autowired
    private VietnamesePhraseRepository vietnamesePhraseRepository;

    public VietnamesePhrase getPhrases(String name) {
        VietnamesePhrase phrase = vietnamesePhraseRepository.findVietnamesePhraseByName(name);
        Set<String> englishPhrasesSet = new HashSet<>();
        if (phrase != null && phrase.getEnglishPhrases().size() != 0) {
            for (EnglishPhrase englishPhrase : phrase.getEnglishPhrases()) {
                englishPhrasesSet.add(englishPhrase.getName());
            }
            phrase.setEnglishPhrasesSet(englishPhrasesSet);
        } else {
            phrase = new VietnamesePhrase();
        }
        return phrase;
    }
}

package shop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import shop.entity.Permission;
import shop.exceptions.Exception;
import shop.repository.PermissionRepository;
import shop.search_specification.PermissionSpecification;
import shop.search_specification.SearchCriteria;
import shop.search_specification.SearchOperation;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PermissionService extends GenericService<Permission, Long> {
    @Autowired
    private PermissionRepository permissionRepository;

    @PersistenceContext
    EntityManager entityManager;

    public Map<String, Object> getPermissionPagination(Pageable pageable, Map<String, String> params) {
        PermissionSpecification permissionSpecification = new PermissionSpecification();
        if (params.containsKey("name")) {
            permissionSpecification.add(new SearchCriteria("name", params.get("name"), SearchOperation.MATCH));
        }
        if (params.containsKey("description")) {
            permissionSpecification.add(new SearchCriteria("description", params.get("description"), SearchOperation.MATCH));
        }

        Page<Permission> permissionPage = permissionRepository.findAll(permissionSpecification, pageable);
        Map<String, Object> results = new HashMap<>();
        results.put("permission", permissionPage.getContent());
        results.put("totalElements", permissionPage.getTotalElements());
        results.put("page", permissionPage.getPageable().getPageNumber());
        results.put("totalPages", permissionPage.getTotalPages());
        return results;
    }

    public Permission create(Permission permission, Principal principal) {
        if (permission.getId() != null) {
            if (permissionRepository.checkExistedNotById(permission.getName(), permission.getId())) {
                throw new Exception("Permissions Name is existed!");
            }
        } else {
            if (permissionRepository.checkExistedByName(permission.getName())) {
                throw new Exception("Permissions Name is existed!");
            }
        }

        String username = principal.getName();
        permission.setCreated_by(username);
        return permissionRepository.save(permission);
    }

    @Override
    public Optional<Permission> findById(Long id) {
        if (!permissionRepository.checkExistedById(id)) {
            throw new Exception("Permissions Name is not existed!");
        }
        return super.findById(id);
    }

    public void deleteManyPermission(List<Long> listPermissionId) {
        try {
            permissionRepository.deleteByIdIn(listPermissionId);
        } catch (Exception e) {
            throw new Exception("Delete Error!");
        }

    }
}

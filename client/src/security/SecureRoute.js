import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

const SecuredRoute = ({ component: Component, validToken, ...otherProps }) => (
  <Route
    {...otherProps}
    render={(props) =>
      validToken === true ? <Component {...props} /> : <Redirect to="/login" />
    }
  />
);

const mapStateToProps = (state) => ({
  validToken: state.user.validToken,
});


export default connect(mapStateToProps)(SecuredRoute);

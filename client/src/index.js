import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import store from "./store/store";
import { usePromiseTracker } from "react-promise-tracker";
import Loader from "react-loader-spinner";
import "./style.css";
import "react-toastify/dist/ReactToastify.css";

const LoadingIndicator = (props) => {
  const { promiseInProgress } = usePromiseTracker();
  return (
    promiseInProgress && (
      <div className="spinnerShow">
        <Loader
          type="Grid"
          color="#008b8e"
          className="spinnerActive"
          height="60"
          width="60"
        />
      </div>
    )
  );
};

ReactDOM.render(
  <Provider store={store}>
    <App />
    <LoadingIndicator />
  </Provider>,
  document.getElementById("root")
);

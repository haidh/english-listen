import React, { Component } from "react";
import User from "./component/content/user-page/User";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./component/content/register-login-page/Login";
import Resgiter from "./component/content/register-login-page/Resgiter";
import Page403 from "./component/content/register-login-page/Page403";
import jwt_decode from "jwt-decode";
import setJWTToken from "./security/setJWTToken";
import store from "./store/store";
import { LOGIN_USER, GET_NOTIFICATION } from "./action_types/types";
import { logout } from "./actions/UserAction";
import SecureRoute from "./security/SecureRoute";
import Permission from "./component/content/permission-page/Permission";
import PermissionCreate from "./component/content/permission-page/PermissionCreate";
import PermissionUpdate from "./component/content/permission-page/PermissionUpdate";
import Role from "./component/content/role-page/Role";
import RoleCreate from "./component/content/role-page/RoleCreate";
import RoleUpdate from "./component/content/role-page/RoleUpdate";
import RoleView from "./component/content/role-page/RoleView";
import PermissionView from "./component/content/permission-page/PermissionView";
import SockJS from "sockjs-client";
import Stomp from "stompjs";
import UserView from "./component/content/user-page/UserView";
import UserCreate from "./component/content/user-page/UserCreate";
import UserUpdate from "./component/content/user-page/UserUpdate";
import Movie from "./component/content/movie-page/Movie";
import MovieCreate from "./component/content/movie-page/MovieCreate";
import MovieUpdate from "./component/content/movie-page/MovieUpdate";
import Sentence from "./component/content/sentence-page/Sentence";
import SentenceCreate from "./component/content/sentence-page/SentenceCreate";
import SentenceUpdate from "./component/content/sentence-page/SentenceUpdate";
import Phrase from "./component/content/phrase-page/Phrase";
import PhraseCreate from "./component/content/phrase-page/PhraseCreate";
import PhraseUpdate from "./component/content/phrase-page/PhraseUpdate";
import ITSentence from "./component/content/sentence-it-page/ITSentence";
import ITSentenceCreate from "./component/content/sentence-it-page/ITSentenceCreate";
import ITSentenceUpdate from "./component/content/sentence-it-page/ITSentenceUpdate";

const jwtToken = localStorage.jwtToken;

if (jwtToken) {
  setJWTToken(jwtToken);
  const decoded_jwtToken = jwt_decode(jwtToken);
  store.dispatch({
    type: LOGIN_USER,
    payload: decoded_jwtToken,
  });

  const currentTime = Date.now() / 1000;
  if (decoded_jwtToken.exp < currentTime) {
    store.dispatch(logout());
    window.location.href = "/login";
  } else {
    // let token = jwtToken.split("Bearer ")[1];
    // var sock = new SockJS("http://localhost:8002/ws?token=" + token);
    // let stompClient = Stomp.over(sock);
    // stompClient.connect({}, function (frame) {
    //   stompClient.subscribe("/user/queue/deleteMessage", function (
    //     notification
    //   ) {
    //     let message = JSON.parse(notification.body);
    //     store.dispatch({
    //       type: GET_NOTIFICATION,
    //       payload: message,
    //     });
    //     console.log(message);
    //   });
    // });
  }
}

class App extends Component {
  render() {
    return (
      <Router basename="/">
        <div className="App">
          <div>
            <Switch>
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Resgiter} />
              <Route exact path="/page-403" component={Page403} />
              <SecureRoute exact path="/" component={User} />
              <SecureRoute exact path="/permission" component={Permission} />
              <SecureRoute
                exact
                path="/permission/view/:id"
                component={PermissionView}
              />
              <SecureRoute
                exact
                path="/permission/create"
                component={PermissionCreate}
              />
              <SecureRoute
                exact
                path="/permission/update/:id"
                component={PermissionUpdate}
              />
              <SecureRoute exact path="/role" component={Role} />
              <SecureRoute exact path="/role/view/:id" component={RoleView} />
              <SecureRoute exact path="/role/create" component={RoleCreate} />
              <SecureRoute
                exact
                path="/role/update/:id"
                component={RoleUpdate}
              />

              <SecureRoute exact path="/user" component={User} />
              <SecureRoute exact path="/user/view/:id" component={UserView} />
              <SecureRoute exact path="/user/create" component={UserCreate} />
              <SecureRoute
                exact
                path="/user/update/:id"
                component={UserUpdate}
              />

              <SecureRoute exact path="/movie" component={Movie} />
              <SecureRoute exact path="/movie/create" component={MovieCreate} />
              <SecureRoute
                exact
                path="/movie/update/:id"
                component={MovieUpdate}
              />
              <SecureRoute exact path="/sentence" component={Sentence} />
              <SecureRoute
                exact
                path="/sentence/create"
                component={SentenceCreate}
              />
              <SecureRoute
                exact
                path="/sentence/update/:id"
                component={SentenceUpdate}
              />

              <SecureRoute exact path="/it" component={ITSentence} />
              <SecureRoute
                exact
                path="/it/create"
                component={ITSentenceCreate}
              />
              <SecureRoute
                exact
                path="/it/update/:id"
                component={ITSentenceUpdate}
              />

              <SecureRoute exact path="/phrase" component={Phrase} />
              <SecureRoute
                exact
                path="/phrase/create"
                component={PhraseCreate}
              />
              <SecureRoute
                exact
                path="/phrase/update/:id"
                component={PhraseUpdate}
              />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;

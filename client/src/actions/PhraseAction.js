import axios from "axios";
import {
  GET_ERRORS,
  GET_PHRASES,
  CREATE_PHRASE,
  GET_PHRASE_PAGINATION,
  DELETE_PHRASE,
  DELETE_LIST_PHRASE,
  GET_PHRASE_DETAIL,
} from "../action_types/types";
import { toast } from "react-toastify";
import API from '../api';
export const getHightLight = (params) => async (dispatch) => {
  try {
    const res = await API.get("phrase", {
      params,
    });
    console.log(res.data);
    dispatch({
      type: GET_PHRASES,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const getPagination = (search) => async (dispatch) => {
  try {
    search.page = search.page - 1;
    var params = {
      page: search.page,
      size: search.size,
      sort: search.sortBy + "," + search.sort,
      englishPhrase: search.englishPhrase,
      vietnamesePhrase: search.vietnamesePhrase,
    };
    const url = "phrase/pagination";
    const res = await API.get(url, {
      params,
    });
    dispatch({
      type: GET_PHRASE_PAGINATION,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const create = (params, history) => async (dispatch) => {
  try {
    const res = await API.post(
      "phrase/create",
      params
    );
    dispatch({
      type: CREATE_PHRASE,
      payload: res.data,
    });
    toast.success("Create Phrase Success");
  } catch (error) {
    if (error.response.data.name) {
      toast.error(error.response.data.name);
    } else if (error.response.data.message) {
      toast.error(error.response.data.message);
    } else {
      toast.error(error.response.data.vietnamesePhrases);
    }

    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const update = (params, history) => async (dispatch) => {
  try {
    const res = await API.post(
      "phrase/update",
      params
    );
    dispatch({
      type: CREATE_PHRASE,
      payload: res.data,
    });
    toast.success("Update Phrase Success");
    history.push("/phrase");
  } catch (error) {
    if (error.response.data.name) {
      toast.error(error.response.data.name);
    } else if (error.response.data.message) {
      toast.error(error.response.data.message);
    } else {
      toast.error(error.response.data.vietnamesePhrases);
    }

    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const deleteById = (id) => async (dispatch) => {
  try {
    const res = await API.delete(
      `phrase/delete/${id}`
    );
    dispatch({
      type: DELETE_PHRASE,
      payload: res.data,
    });
    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const deleteList = (listId) => async (dispatch) => {
  try {
    const res = await API.delete(
      `phrase/delete?listId=` + listId
    );

    dispatch({
      type: DELETE_LIST_PHRASE,
      payload: res.data,
    });

    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const getDetail = (id) => async (dispatch) => {
  try {
    const res = await API.get(`phrase/${id}`);
    dispatch({
      type: GET_PHRASE_DETAIL,
      payload: res.data,
    });
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

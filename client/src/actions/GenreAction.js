import axios from "axios";
import {
  GET_ERRORS,
  GET_ALL_GENRE,
  GET_GENRE_DETAIL
} from "../action_types/types";
import { toast } from "react-toastify";
import API from '../api';
export const getAllGenre = () => async (dispatch) => {
  try {
    const res = await API.get("genre");
    dispatch({
      type: GET_ALL_GENRE,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const getGenreDetail = (id) => async (dispatch) => {
  try {
    const res = await API.get(`genre/${id}`);
    console.log(res.data);
    dispatch({
      type: GET_GENRE_DETAIL,
      payload: res.data,
    });
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

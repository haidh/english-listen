import axios from "axios";
import {
  GET_ERRORS,
  GET_OPERATION,
  GET_OPERATION_BY_CONDITION,
} from "../action_types/types";
import API from '../api';
export const getOperation = () => async (dispatch) => {
  try {
    const res = await API.get("operation");
    dispatch({
      type: GET_OPERATION,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const getOperationByCondition = (checkOperations) => async (dispatch) => {
  try {
      const params = {
          operation: checkOperations
      }
    const res = await API.post("operation/list", params);
    dispatch({
      type: GET_OPERATION_BY_CONDITION,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

import axios from "axios";
import { GET_ERRORS, GET_SEASON_ALL,GET_SEASON_BY_MOVIE } from "../action_types/types";
import API from '../api';
export const getAllSeason = () => async (dispatch) => {
  try {
    const url = "season";
    const res = await API.get(url);
    dispatch({
      type: GET_SEASON_ALL,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const getSeasonByMovie = (id) => async (dispatch) => {
  try {
    const url = `season/movie-season/${id}`;
    const res = await API.get(url);
    dispatch({
      type: GET_SEASON_BY_MOVIE,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

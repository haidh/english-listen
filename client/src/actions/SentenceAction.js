import axios from "axios";
import {
  GET_ERRORS,
  GET_SENTENCE_PAGINATION,
  GET_MOVIE_DETAIL,
  GET_SENTENCE_DETAIL,
  UPDATE_SENTENCE,
  CREATE_SENTENCE,
  DELETE_SENTENCE,
  DELETE_LIST_SENTENCE,
  READ_FILE_TO_IMPORT_SENTENCE,
  SAVE_FILE,
} from "../action_types/types";
import { toast } from "react-toastify";
import API from '../api';
export const readFile = (state) => async (dispatch) => {
  try {
    const formData = new FormData();
    if (state.selectedFile !== null) {
      formData.append("file", state.selectedFile);
    } else {
      var file = new File([""], "");
      formData.append("file", file, {
        type: "text/plain",
      });
    }
    API.post(
      "sentence/read-file-to-import-sentence",
      formData
    );
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const getPagination = (search) => async (dispatch) => {
  try {
    search.page = search.page - 1;
    var params = {
      page: search.page,
      size: search.size,
      sort: search.sortBy + "," + search.sort,
      sentence: search.sentence,
      translation: search.translation,
      movieId: "",
    };
    if (search.movieSelected.length > 0) {
      params.movieId = search.movieSelected[0].value;
    }
    const url = "sentence/pagination";
    const res = await API.get(url, {
      params,
    });
    dispatch({
      type: GET_SENTENCE_PAGINATION,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const create = (state, history) => async (dispatch) => {
  const formData = new FormData();
  formData.append("movieId", state.movieIdSelected);
  formData.append("seasonId", state.seasonIdSelected);
  formData.append("episodeId", state.episodeIdSelected);
  formData.append("translation", state.translation.value);
  if (state.selectedFile !== null) {
    formData.append("file", state.selectedFile);
  } else {
    var file = new File([""], "");
    formData.append("file", file, {
      type: "text/plain",
    });
  }
  formData.append("sentence", state.sentence.value);
  try {
    const res = await axios
      .create({
        baseURL: "http://localhost:8080/api/",
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .post("/sentence/create", formData);
    dispatch({
      type: CREATE_SENTENCE,
      payload: res.data,
    });
    toast.success("Create Sentence Success", {});
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const update = (state, history) => async (dispatch) => {
  const formData = new FormData();
  formData.append("sentenceId", state.sentenceId);
  formData.append("movieId", state.movieIdSelected);
  formData.append("seasonId", state.seasonIdSelected);
  formData.append("episodeId", state.episodeIdSelected);
  formData.append("translation", state.translation.value);
  formData.append("audio", state.audio);
  if (state.selectedFile !== null) {
    formData.append("file", state.selectedFile);
  } else {
    var file = new File([""], "");
    formData.append("file", file, {
      type: "text/plain",
    });
  }

  formData.append("sentence", state.sentence.value);
  try {
    const res = await API.post(
      "sentence/update",
      formData
    );

    if (res.data) {
      toast.success("Update Sentence Success");
      history.push("/sentence?page=" + state.page + "&size=" + state.size);
      window.location.reload();
    }
    dispatch({
      type: UPDATE_SENTENCE,
      payload: res.data,
    });
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const saveFile = (state) => async (dispatch) => {
  const formData = new FormData();
  if (state.selectedFile !== null) {
    formData.append("file", state.selectedFile);
  } else {
    var file = new File([""], "");
    formData.append("file", file, {
      type: "text/plain",
    });
  }
  try {
    API.post("sentence/save-file", formData);
  } catch (error) {}
};

export const getDetail = (id) => async (dispatch) => {
  try {
    const res = await API.get(`sentence/${id}`);
    dispatch({
      type: GET_SENTENCE_DETAIL,
      payload: res.data,
    });
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const getMovieDetail = (id) => async (dispatch) => {
  try {
    const res = await API.get(`movie/${id}`);
    dispatch({
      type: GET_MOVIE_DETAIL,
      payload: res.data,
    });
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const deleteById = (id) => async (dispatch) => {
  try {
    const res = await API.delete(
      `sentence/delete/${id}`
    );
    dispatch({
      type: DELETE_SENTENCE,
      payload: res.data,
    });
    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const deleteList = (listId) => async (dispatch) => {
  try {
    const res = await API.delete(
      `sentence/delete?listId=` + listId
    );

    dispatch({
      type: DELETE_LIST_SENTENCE,
      payload: res.data,
    });

    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

import axios from "axios";
import {
  GET_ERRORS,
  GET_IT_SENTENCE_PAGINATION,
  GET_IT_SENTENCE_DETAIL,
  UPDATE_IT_SENTENCE,
  CREATE_IT_SENTENCE,
  DELETE_IT_SENTENCE,
  DELETE_LIST_IT_SENTENCE,
} from "../action_types/types";
import { toast } from "react-toastify";
import API from '../api';
export const getPagination = (search) => async (dispatch) => {
  try {
    search.page = search.page - 1;
    var params = {
      page: search.page,
      size: search.size,
      sort: search.sortBy + "," + search.sort,
      sentence: search.sentence,
      translation: search.translation,
    };
    const url = "it-sentence/pagination";
    const res = await API.get(url, {
      params,
    });
    dispatch({
      type: GET_IT_SENTENCE_PAGINATION,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const create = (params, history) => async (dispatch) => {
  try {
    const res = await API.post(
      "it-sentence/create",
      params
    );
    dispatch({
      type: CREATE_IT_SENTENCE,
      payload: res.data,
    });
    toast.success("Create IT Sentence Success");
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const update = (params, history) => async (dispatch) => {
  try {
    const res = await API.post(
      "it-sentence/update",
      params
    );
    dispatch({
      type: UPDATE_IT_SENTENCE,
      payload: res.data,
    });
    toast.success("Update Sentence Success");
    history.push("/it");
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const deleteById = (id) => async (dispatch) => {
  try {
    const res = await API.delete(
      `it-sentence/delete/${id}`
    );
    dispatch({
      type: DELETE_IT_SENTENCE,
      payload: res.data,
    });
    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const deleteList = (listId) => async (dispatch) => {
  try {
    const res = await API.delete(
      `it-sentence/delete?listId=` + listId
    );

    dispatch({
      type: DELETE_LIST_IT_SENTENCE,
      payload: res.data,
    });

    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const getDetail = (id) => async (dispatch) => {
  try {
    const res = await API.get(`it-sentence/${id}`);
    dispatch({
      type: GET_IT_SENTENCE_DETAIL,
      payload: res.data,
    });
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

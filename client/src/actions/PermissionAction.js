import axios from "axios";
import {
  GET_ERRORS,
  GET_PERMISSION_PAGINATION,
  GET_PERMISSION_ALL,
  CREATE_PERMISION,
  GET_PERMISSION_DETAIL,
  DELETE_PERMISSION,
  DELETE_LIST_PERMISSION,
  UPDATE_PERMISION,
} from "../action_types/types";
import { toast } from "react-toastify";
import API from '../api';
export const getAllPermission = () => async (dispatch) => {
  try {
    const url = "permission";
    const res = await API.get(url);
    dispatch({
      type: GET_PERMISSION_ALL,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const getPermission = (search) => async (dispatch) => {
  try {
    search.page = search.page - 1;
    const url =
      "permission/pagination?page=" +
      search.page +
      "&size=" +
      search.size +
      "&sort=" +
      search.sortBy +
      "," +
      search.sort +
      "&name=" +
      search.name +
      "&description=" +
      search.description;
    const res = await API.get(url, search);
    dispatch({
      type: GET_PERMISSION_PAGINATION,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const createPermission = (permission, history) => async (dispatch) => {
  try {
    const res = await API.post("permission/create", permission);
    dispatch({
      type: CREATE_PERMISION,
      payload: res.data,
    });
    if (permission.id) {
      toast.success("Update Permission Success", {});
    } else {
      toast.success("Create Permission Success", {});
    }

    history.push("/permission");
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const updatePermission = (permission, history) => async (dispatch) => {
  try {
    const res = await API.post("permission/update", permission);
    dispatch({
      type: UPDATE_PERMISION,
      payload: res.data,
    });
    toast.success("Update Permission Success", {});
    history.push("/permission");
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const getPermissionDetail = (id) => async (dispatch) => {
  try {
    const res = await API.get(`permission/${id}`);
    dispatch({
      type: GET_PERMISSION_DETAIL,
      payload: res.data,
    });
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const deletePermission = (id) => async (dispatch) => {
  try {
    const res = await API.delete(`permission/delete/${id}`);

    dispatch({
      type: DELETE_PERMISSION,
      payload: res.data,
    });
    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const deleteListPermission = (listId) => async (dispatch) => {
  try {
    const res = await API.delete(`permission/delete?listId=` + listId);

    dispatch({
      type: DELETE_LIST_PERMISSION,
      payload: res.data,
    });

    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

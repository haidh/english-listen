import axios from "axios";
import {
  GET_ERRORS,
  GET_ROLE,
  CREATE_ROLE,
  GET_ROLE_DETAIL,
  DELETE_ROLE,
  DELETE_LIST_ROLE,
  UPDATE_ROLE,
  GET_ALL_ROLE,
} from "../action_types/types";
import { toast } from "react-toastify";
import API from '../api';
export const getRole = (search) => async (dispatch) => {
  try {
    search.page = search.page - 1;
    const url =
      "role/pagination?page=" +
      search.page +
      "&size=" +
      search.size +
      "&sort=" +
      search.sortBy +
      "," +
      search.sort +
      "&name=" +
      search.name +
      "&description=" +
      search.description;
    const res = await API.get(url, search);
    dispatch({
      type: GET_ROLE,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const getAllRole = () => async (dispatch) => {
  try {
    const res = await API.get("role");
    dispatch({
      type: GET_ALL_ROLE,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const createRole = (role, history) => async (dispatch) => {
  try {
    const res = await API.post("role/create", role);
    dispatch({
      type: CREATE_ROLE,
      payload: res.data,
    });
    toast.success("Create Role Success", {});
    history.push("/role");
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const updateRole = (role, history) => async (dispatch) => {
  try {
    const res = await API.post("role/update", role);
    dispatch({
      type: UPDATE_ROLE,
      payload: res.data,
    });
    toast.success("Update Role Success", {});
    history.push("/role");
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const getRoleDetail = (id) => async (dispatch) => {
  try {
    const res = await API.get(`role/${id}`);
    dispatch({
      type: GET_ROLE_DETAIL,
      payload: res.data,
    });
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const deleteRole = (id) => async (dispatch) => {
  try {
    const res = await API.delete(`role/delete/${id}`);
    dispatch({
      type: DELETE_ROLE,
      payload: res.data,
    });
    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const deleteListRole = (listId) => async (dispatch) => {
  try {
    const res = await API.delete(`role/delete?listId=` + listId);

    dispatch({
      type: DELETE_LIST_ROLE,
      payload: res.data,
    });

    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};


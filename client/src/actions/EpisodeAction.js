import axios from "axios";
import {
  GET_ERRORS,
  GET_EPISODE_ALL,
  GET_EPISODE_BY_MOVIE_SEASON,
} from "../action_types/types";
import API from '../api';
export const getAllEpisode = () => async (dispatch) => {
  try {
    const url = "episode";
    const res = await API.get(url);
    dispatch({
      type: GET_EPISODE_ALL,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const getEpisodeByMovieSeason = (movieId, seasonId) => async (
  dispatch
) => {
  try {
    const url = `episode/movie-season`;
    const res = await API.get(url, {
      params: {
        movieId: movieId,
        seasonId: seasonId,
      },
    });
    dispatch({
      type: GET_EPISODE_BY_MOVIE_SEASON,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

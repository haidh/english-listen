import axios from "axios";
import {
  GET_ERRORS,
  REGISTER_USER,
  LOGIN_USER,
  LOGOUT_USER,
  GET_USER_PAGINATION,
  DELETE_USER,
  DELETE_LIST_USER,
  GET_USER_DETAIL,
  CREATE_USER,
  UPDATE_USER,
} from "../action_types/types";
import setJWTToken from "../security/setJWTToken";
import jwt_decode from "jwt-decode";
import { toast } from "react-toastify";
import API from '../api';

export const registerUser = (user, history) => async (dispatch) => {
  try {
    const res = await API.post("user/register", user);
    dispatch({
      type: REGISTER_USER,
      payload: res.data,
    });
    history.push("/login");
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const login = (LoginRequest) => async (dispatch) => {
  try {
    const res = await API.post("user/login", LoginRequest);

    const { token, sessionId } = res.data;
    console.log(res);
    localStorage.setItem("jwtToken", token);
    localStorage.setItem("sessionId", sessionId);
    setJWTToken(token);
    const decoded = jwt_decode(token);
    dispatch({
      type: LOGIN_USER,
      payload: decoded,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const logout = () => async (dispatch) => {
  localStorage.removeItem("jwtToken");
  setJWTToken(false);
  dispatch({
    type: LOGOUT_USER,
    payload: {},
  });
};

export const getUser = (search) => async (dispatch) => {
  try {
    search.page = search.page - 1;
    const url =
      "user/pagination?page=" +
      search.page +
      "&size=" +
      search.size +
      "&sort=" +
      search.sortBy +
      "," +
      search.sort +
      "&username=" +
      search.username +
      "&fullName=" +
      search.fullName +
      "&email=" +
      search.email;
    const res = await API.get(url, search);
    dispatch({
      type: GET_USER_PAGINATION,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const deleteUser = (id) => async (dispatch) => {
  try {
    const res = await API.delete(`user/delete/${id}`);

    dispatch({
      type: DELETE_USER,
      payload: res.data,
    });
    console.log(res)
    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data);
  }
};

export const deleteListUser = (listId) => async (dispatch) => {
  try {
    const res = await API.delete(`user/delete?listId=` + listId);

    dispatch({
      type: DELETE_LIST_USER,
      payload: res.data,
    });

    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const getUserDetails = (id) => async (dispatch) => {
  try {
    const res = await API.get(`user/${id}`);
    dispatch({
      type: GET_USER_DETAIL,
      payload: res.data,
    });
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const createUser = (user, history) => async (dispatch) => {
  try {
    const res = await API.post("user/update", user);
    dispatch({
      type: CREATE_USER,
      payload: res.data,
    });
    toast.success("Create Role Success", {});
    history.push("/user");
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const updateUser = (user, history) => async (dispatch) => {
  try {
    const res = await API.post("user/update", user);
    dispatch({
      type: UPDATE_USER,
      payload: res.data,
    });
    toast.success("Update Role Success", {});
    history.push("/user");
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

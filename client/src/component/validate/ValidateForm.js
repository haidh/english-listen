export const validateForm = (fieldName, field) => {
  const regex = new RegExp('[!@#$%^&*(),.?":{}|<>]');
  let fieldValidate = {
    value: field.value,
    isInvalid: false,
    message: "",
  };
  if (field.value.length === 0 && fieldName !== "description") {
    fieldValidate = {
      value: field.value,
      isInvalid: true,
      message: "This field is required",
    };
  } else if (field.value.length < 6 && fieldName !== "description") {
    fieldValidate = {
      value: field.value,
      isInvalid: true,
      message: "This field is less than 6",
    };
  } else if (regex.test(field.value) && fieldName !== "description") {
    fieldValidate = {
      value: field.value,
      isInvalid: true,
      message: "This field includes special character",
    };
  }
  return fieldValidate;
};

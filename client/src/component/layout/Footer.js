import React, { Component } from "react";
import "../../style.css";
import { Link } from "react-router-dom";
class Footer extends Component {
  render() {
    return (
      <div>
        {/* BEGIN: Footer*/}
        <footer className="footer footer-static footer-light navbar-border fixedFooter">
          <p className="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span className="float-md-left d-block d-md-inline-block">
              Copyright © 2020{" "}
              <Link
                className="text-bold-800 grey darken-2"
                to="/"
              >
                PIXINVENT{" "}
              </Link>
            </span>
            <span className="float-md-right d-none d-lg-block">
              Hand-crafted &amp; Made with{" "}
              <i className="feather icon-heart pink" />
            </span>
          </p>
        </footer>
        {/* END: Footer*/}
      </div>
    );
  }
}

export default Footer;

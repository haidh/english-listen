import React, { Component } from "react";
import { Link } from "react-router-dom";

class MenuSideBar extends Component {
  state = {
    openMenu: true,
    clickMenu: {
      english: false,
      users: true,
    },
  };
  componentDidMount() {
    this.activeMenu();
    const link = window.location.href;
    if (
      link.includes("movie") ||
      link.includes("sentence") ||
      link.includes("it") ||
      link.includes("phrase")
    ) {
      this.setState({
        clickMenu: {
          english: true,
        },
      });
    } else if (
      link.includes("user") ||
      link.includes("role") ||
      link.includes("permission")
    ) {
      this.setState({
        clickMenu: {
          users: true,
        },
      });
    }
  }
  activeMenu = () => {
    const link = window.location.href;
    switch (true) {
      case link.includes("permission"):
        return "permission";
      case link.includes("role"):
        return "role";
      case link.includes("movie"):
        return "movie";
      case link.includes("sentence"):
        return "sentence";
      case link.includes("it"):
        return "it";
      case link.includes("phrase"):
        return "phrase";
      default:
        return "user";
    }
  
  };

  showMenu = (typeMenu) => {
    if (typeMenu === "english") {
      this.setState({
        clickMenu: {
          english: !this.state.clickMenu.english,
        },
      });
    } else {
      this.setState({
        clickMenu: {
          users: !this.state.clickMenu.users,
        },
      });
    }

    return this.state;
  };

  render() {
    return (
      <div>
        {/* BEGIN: Main Menu*/}
        <div
          className="main-menu menu-fixed menu-light menu-accordion menu-shadow"
          data-scroll-to-active="true"
        >
          <div className="main-menu-content">
            <ul
              className="navigation navigation-main"
              id="main-menu-navigation"
              data-menu="menu-navigation"
            >
              <li
                className={
                  ("nav-item", this.state.clickMenu.english ? "open" : "")
                }
              >
                <a
                  className="iconShow"
                  onClick={() => this.showMenu("english")}
                >
                  <i className="feather icon-user" />
                  <span className="menu-title">English</span>
                </a>

                <ul className="menu-content">
                  <li
                    className={
                      this.activeMenu() === "movie" ? "active" : "menu-item"
                    }
                  >
                    <Link className="menu-item" to="/movie">
                      Movie
                    </Link>
                  </li>
                  <li
                    className={
                      this.activeMenu() === "sentence" ? "active" : "menu-item"
                    }
                  >
                    <Link className="menu-item" to="/sentence?page=1&size=10">
                      Sentence
                    </Link>
                  </li>

                  <li
                    className={
                      this.activeMenu() === "it" ? "active" : "menu-item"
                    }
                  >
                    <Link className="menu-item" to="/it">
                      IT Sentence
                    </Link>
                  </li>
                  <li
                    className={
                      this.activeMenu() === "phrase" ? "active" : "menu-item"
                    }
                  >
                    <Link className="menu-item" to="/phrase">
                      Phrase
                    </Link>
                  </li>
                </ul>
              </li>
              <li
                className={
                  ("nav-item", this.state.clickMenu.users ? "open" : "")
                }
              >
                <a className="iconShow" onClick={() => this.showMenu("users")}>
                  <i className="feather icon-user" />
                  <span className="menu-title" data-i18n="Users">
                    Users
                  </span>
                </a>

                <ul className="menu-content">
                  <li
                    className={
                      this.activeMenu() === "user" ? "active" : "menu-item"
                    }
                  >
                    <Link className="menu-item" to="/user">
                      User
                    </Link>
                  </li>
                  <li
                    className={
                      this.activeMenu() === "role" ? "active" : "menu-item"
                    }
                  >
                    <Link className="menu-item" to="/role">
                      Role
                    </Link>
                  </li>

                  <li
                    className={
                      this.activeMenu() === "permission"
                        ? "active"
                        : "menu-item"
                    }
                  >
                    <Link className="menu-item" to="/permission">
                      Permission
                    </Link>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        {/* END: Main Menu*/}
      </div>
    );
  }
}

export default MenuSideBar;

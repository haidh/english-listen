import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { logout } from "../../actions/UserAction";
import { Link } from "react-router-dom";

class Header extends Component {
  state = {
    notification: {},
    listNotification: [],
  };
  componentDidMount() {}

  componentWillReceiveProps(nextProps) {
    const listNotification = [];
    if (nextProps.notification !== undefined) {
      listNotification.push(nextProps.notification);
      this.setState({
        notification: nextProps.notification,
        listNotification: listNotification,
      });
    }
  }
  logout = () => {
    this.props.logout();
    window.location.href = "/#/login";
  };
  removeNewNotification = () => {
    this.setState({
      listNotification: [],
    });
  };
  render() {
    const { user } = this.props;
    const { notification, listNotification } = this.state;
    return (
      <div>
        {/* BEGIN: Header*/}
        <nav className="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-dark bg-gradient-x-primary navbar-shadow">
          <div className="navbar-wrapper">
            <div className="navbar-header">
              <ul className="nav navbar-nav flex-row">
                <li className="nav-item mobile-menu d-md-none mr-auto">
                  <a
                    className="nav-link nav-menu-main menu-toggle hidden-xs"
                    href="#"
                  >
                    <i className="feather icon-menu font-large-1" />
                  </a>
                </li>
                <li className="nav-item">
                  <Link className="navbar-brand" to="/">
                    <img
                      className="brand-logo"
                      alt="stack admin logo"
                      src="app-assets/images/logo/stack-logo-light.png"
                    />
                    <h2 className="brand-text">Stack</h2>
                  </Link>
                </li>
                <li className="nav-item d-md-none">
                  <a
                    className="nav-link open-navbar-container"
                    data-toggle="collapse"
                    data-target="#navbar-mobile"
                  >
                    <i className="fa fa-ellipsis-v" />
                  </a>
                </li>
              </ul>
            </div>
            <div className="navbar-container content">
              <div className="collapse navbar-collapse" id="navbar-mobile">
                <ul className="nav navbar-nav mr-auto float-left">
                  <li className="nav-item d-none d-md-block">
                    <a
                      className="nav-link nav-menu-main menu-toggle hidden-xs"
                      href="#"
                    >
                      <i className="feather icon-menu" />
                    </a>
                  </li>
                  <li className="dropdown nav-item mega-dropdown d-none d-lg-block">
                    <a
                      className="dropdown-toggle nav-link"
                      href="#"
                      data-toggle="dropdown"
                    >
                      Mega
                    </a>
                    <ul className="mega-dropdown-menu dropdown-menu row p-1">
                      <li className="col-md-4 bg-mega p-2">
                        <h3 className="text-white mb-1 font-weight-bold">
                          Mega Menu Sidebar
                        </h3>
                        <p className="text-white line-height-2">
                          Candy canes bonbon toffee. Cheesecake dragée gummi
                          bears chupa chups powder bonbon. Apple pie cookie
                          sweet.
                        </p>
                        <button className="btn btn-outline-white">
                          Learn More
                        </button>
                      </li>
                      <li className="col-md-5 px-2">
                        <h6 className="font-weight-bold font-medium-2 ml-1">
                          Apps
                        </h6>
                        <ul className="row mt-2">
                          <li className="col-6 col-xl-4">
                            <a
                              className="text-center mb-2 mb-xl-3"
                              href="app-email.html"
                              target="_blank"
                            >
                              <i className="feather icon-mail font-large-1 mr-0" />
                              <p className="font-medium-2 mt-25 mb-0">Email</p>
                            </a>
                          </li>
                          <li className="col-6 col-xl-4">
                            <a
                              className="text-center mb-2 mb-xl-3"
                              href="app-chat.html"
                              target="_blank"
                            >
                              <i className="feather icon-message-square font-large-1 mr-0" />
                              <p className="font-medium-2 mt-25 mb-0">Chat</p>
                            </a>
                          </li>
                          <li className="col-6 col-xl-4">
                            <a
                              className="text-center mb-2 mb-xl-3 mt-75 mt-xl-0"
                              href="app-todo.html"
                              target="_blank"
                            >
                              <i className="feather icon-check-square font-large-1 mr-0" />
                              <p className="font-medium-2 mt-25 mb-0">Todo</p>
                            </a>
                          </li>
                          <li className="col-6 col-xl-4">
                            <a
                              className="text-center mb-2 mt-75 mt-xl-0"
                              href="app-kanban.html"
                              target="_blank"
                            >
                              <i className="feather icon-file-plus font-large-1 mr-0" />
                              <p className="font-medium-2 mt-25 mb-50">
                                Kanban
                              </p>
                            </a>
                          </li>
                          <li className="col-6 col-xl-4">
                            <a
                              className="text-center mb-2 mt-75 mt-xl-0"
                              href="app-contacts.html"
                              target="_blank"
                            >
                              <i className="feather icon-users font-large-1 mr-0" />
                              <p className="font-medium-2 mt-25 mb-50">
                                Contacts
                              </p>
                            </a>
                          </li>
                          <li className="col-6 col-xl-4">
                            <a
                              className="text-center mb-2 mt-75 mt-xl-0"
                              href="invoice-template.html"
                              target="_blank"
                            >
                              <i className="feather icon-printer font-large-1 mr-0" />
                              <p className="font-medium-2 mt-25 mb-50">
                                Invoice
                              </p>
                            </a>
                          </li>
                        </ul>
                      </li>
                      <li className="col-md-3">
                        <h6 className="font-weight-bold font-medium-2">
                          Components
                        </h6>
                        <ul className="row mt-1 mt-xl-2">
                          <li className="col-12 col-xl-6 pl-0">
                            <ul className="mega-component-list">
                              <li className="mega-component-item">
                                <a
                                  className="mb-1 mb-xl-2"
                                  href="component-alerts.html"
                                  target="_blank"
                                >
                                  Alert
                                </a>
                              </li>
                              <li className="mega-component-item">
                                <a
                                  className="mb-1 mb-xl-2"
                                  href="component-callout.html"
                                  target="_blank"
                                >
                                  Callout
                                </a>
                              </li>
                              <li className="mega-component-item">
                                <a
                                  className="mb-1 mb-xl-2"
                                  href="component-buttons-basic.html"
                                  target="_blank"
                                >
                                  Buttons
                                </a>
                              </li>
                              <li className="mega-component-item">
                                <a
                                  className="mb-1 mb-xl-2"
                                  href="component-carousel.html"
                                  target="_blank"
                                >
                                  Carousel
                                </a>
                              </li>
                            </ul>
                          </li>
                          <li className="col-12 col-xl-6 pl-0">
                            <ul className="mega-component-list">
                              <li className="mega-component-item">
                                <a
                                  className="mb-1 mb-xl-2"
                                  href="component-dropdowns.html"
                                  target="_blank"
                                >
                                  Drop Down
                                </a>
                              </li>
                              <li className="mega-component-item">
                                <a
                                  className="mb-1 mb-xl-2"
                                  href="component-list-group.html"
                                  target="_blank"
                                >
                                  List Group
                                </a>
                              </li>
                              <li className="mega-component-item">
                                <a
                                  className="mb-1 mb-xl-2"
                                  href="component-modals.html"
                                  target="_blank"
                                >
                                  Modals
                                </a>
                              </li>
                              <li className="mega-component-item">
                                <a
                                  className="mb-1 mb-xl-2"
                                  href="component-pagination.html"
                                  target="_blank"
                                >
                                  Pagination
                                </a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                  <li className="nav-item d-none d-md-block">
                    <a className="nav-link nav-link-expand" href="#">
                      <i className="ficon feather icon-maximize" />
                    </a>
                  </li>
                  <li className="nav-item nav-search">
                    <a className="nav-link nav-link-search" href="#">
                      <i className="ficon feather icon-search" />
                    </a>
                    <div className="search-input">
                      <input
                        className="input"
                        type="text"
                        placeholder="Explore Stack..."
                        tabIndex={0}
                        data-search="template-search"
                      />
                      <div className="search-input-close">
                        <i className="feather icon-x" />
                      </div>
                      <ul className="search-list" />
                    </div>
                  </li>
                </ul>
                <ul className="nav navbar-nav float-right">
                  <li className="dropdown dropdown-language nav-item">
                    <a
                      className="dropdown-toggle nav-link"
                      id="dropdown-flag"
                      href="#"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <i className="flag-icon flag-icon-us" />
                      <span className="selected-language" />
                    </a>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="dropdown-flag"
                    >
                      <a className="dropdown-item" href="#" data-language="en">
                        <i className="flag-icon flag-icon-us" /> English
                      </a>
                      <a className="dropdown-item" href="#" data-language="fr">
                        <i className="flag-icon flag-icon-fr" /> French
                      </a>
                      <a className="dropdown-item" href="#" data-language="pt">
                        <i className="flag-icon flag-icon-pt" /> Portuguese
                      </a>
                      <a className="dropdown-item" href="#" data-language="de">
                        <i className="flag-icon flag-icon-de" /> German
                      </a>
                    </div>
                  </li>
                  <li className="dropdown dropdown-notification nav-item">
                    <a
                      className="nav-link nav-link-label"
                      href="#"
                      data-toggle="dropdown"
                    >
                      <i className="ficon feather icon-bell" />
                      {listNotification.length > 0 ? (
                        <span
                          className="badge badge-pill badge-danger badge-up"
                          onClick={this.removeNewNotification}
                        >
                          {listNotification.length}
                        </span>
                      ) : null}
                    </a>
                    <ul className="dropdown-menu dropdown-menu-media dropdown-menu-right">
                      <li className="dropdown-menu-header">
                        <h6 className="dropdown-header m-0">
                          <span className="grey darken-2">Notifications</span>
                        </h6>
                      </li>
                      <li className="scrollable-container media-list">
                        {Object.keys(notification).length > 0 ? (
                          <a href="/#">
                            <div className="media">
                              <div className="media-left align-self-center">
                                <i className="feather icon-download-cloud icon-bg-circle bg-red bg-darken-1" />
                              </div>
                              <div className="media-body">
                                <h6 className="media-heading red darken-1">
                                  {notification.title}
                                </h6>
                                <p className="notification-text font-small-3 text-muted">
                                  {notification.message}
                                </p>
                              </div>
                            </div>
                          </a>
                        ) : null}
                      </li>
                      <li className="dropdown-menu-footer">
                        <a
                          className="dropdown-item text-muted text-center"
                          href="/#"
                        >
                          Read all notifications
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li className="dropdown dropdown-notification nav-item">
                    <a
                      className="nav-link nav-link-label"
                      href="#"
                      data-toggle="dropdown"
                    >
                      <i className="ficon feather icon-mail" />
                      <span className="badge badge-pill badge-warning badge-up">
                        3
                      </span>
                    </a>
                    <ul className="dropdown-menu dropdown-menu-media dropdown-menu-right">
                      <li className="dropdown-menu-header">
                        <h6 className="dropdown-header m-0">
                          <span className="grey darken-2">Messages</span>
                          <span className="notification-tag badge badge-warning float-right m-0">
                            4 New
                          </span>
                        </h6>
                      </li>
                      <li className="scrollable-container media-list">
                        <a href="/#">
                          <div className="media">
                            <div className="media-left">
                              <div className="avatar avatar-online avatar-sm rounded-circle">
                                <img
                                  src="../../../app-assets/images/portrait/small/avatar-s-1.png"
                                  alt="avatar"
                                />
                                <i />
                              </div>
                            </div>
                            <div className="media-body">
                              <h6 className="media-heading">Margaret Govan</h6>
                              <p className="notification-text font-small-3 text-muted">
                                I like your portfolio, let's start.
                              </p>
                              <small>
                                <time
                                  className="media-meta text-muted"
                                  dateTime="2015-06-11T18:29:20+08:00"
                                >
                                  Today
                                </time>
                              </small>
                            </div>
                          </div>
                        </a>
                        <a href="/#">
                          <div className="media">
                            <div className="media-left">
                              <span className="avatar avatar-sm avatar-busy rounded-circle">
                                <img
                                  src="../../../app-assets/images/portrait/small/avatar-s-2.png"
                                  alt="avatar"
                                />
                                <i />
                              </span>
                            </div>
                            <div className="media-body">
                              <h6 className="media-heading">Bret Lezama</h6>
                              <p className="notification-text font-small-3 text-muted">
                                I have seen your work, there is
                              </p>
                              <small>
                                <time
                                  className="media-meta text-muted"
                                  dateTime="2015-06-11T18:29:20+08:00"
                                >
                                  Tuesday
                                </time>
                              </small>
                            </div>
                          </div>
                        </a>
                        <a href="/#">
                          <div className="media">
                            <div className="media-left">
                              <div className="avatar avatar-online avatar-sm rounded-circle">
                                <img
                                  src="../../../app-assets/images/portrait/small/avatar-s-3.png"
                                  alt="avatar"
                                />
                                <i />
                              </div>
                            </div>
                            <div className="media-body">
                              <h6 className="media-heading">Carie Berra</h6>
                              <p className="notification-text font-small-3 text-muted">
                                Can we have call in this week ?
                              </p>
                              <small>
                                <time
                                  className="media-meta text-muted"
                                  dateTime="2015-06-11T18:29:20+08:00"
                                >
                                  Friday
                                </time>
                              </small>
                            </div>
                          </div>
                        </a>
                        <a href="/#">
                          <div className="media">
                            <div className="media-left">
                              <span className="avatar avatar-sm avatar-away rounded-circle">
                                <img
                                  src="/app-assets/images/portrait/small/avatar-s-6.png"
                                  alt="avatar"
                                />
                                <i />
                              </span>
                            </div>
                            <div className="media-body">
                              <h6 className="media-heading">Eric Alsobrook</h6>
                              <p className="notification-text font-small-3 text-muted">
                                We have project party this saturday.
                              </p>
                              <small>
                                <time
                                  className="media-meta text-muted"
                                  dateTime="2015-06-11T18:29:20+08:00"
                                >
                                  last month
                                </time>
                              </small>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li className="dropdown-menu-footer">
                        <a
                          className="dropdown-item text-muted text-center"
                          href="/#"
                        >
                          Read all messages
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li className="dropdown dropdown-user nav-item">
                    <a
                      className="dropdown-toggle nav-link dropdown-user-link"
                      href="#"
                      data-toggle="dropdown"
                    >
                      <div className="avatar avatar-online">
                        <img
                          src={
                            process.env.PUBLIC_URL +
                            "/app-assets/images/portrait/small/avatar-s-1.png"
                          }
                          alt="avatar"
                        />
                        <i />
                      </div>
                      <span className="user-name">{user.full_name}</span>
                    </a>
                    <div className="dropdown-menu dropdown-menu-right">
                      <a className="dropdown-item" href="user-profile.html">
                        <i className="feather icon-user" /> Edit Profile
                      </a>
                      <a className="dropdown-item" href="app-email.html">
                        <i className="feather icon-mail" /> My Inbox
                      </a>
                      <a className="dropdown-item" href="user-cards.html">
                        <i className="feather icon-check-square" /> Task
                      </a>
                      <a className="dropdown-item" href="app-chat.html">
                        <i className="feather icon-message-square" /> Chats
                      </a>
                      <div className="dropdown-divider" />
                      <a className="dropdown-item" onClick={this.logout}>
                        <i className="feather icon-power" /> Logout
                      </a>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
        {/* END: Header*/}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.data,
  validToken: state.user.validToken,
  notification: state.user.notification,
});

Header.propTypes = {
  user: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { logout })(Header);

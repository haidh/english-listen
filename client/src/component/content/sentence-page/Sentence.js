import React, { Component } from "react";
import "../../../style.css";
import { connect } from "react-redux";
import ReactAudioPlayer from "react-audio-player";
import {
  readFile,
  getPagination,
  deleteById,
  deleteList,
} from "../../../actions/SentenceAction";
import { getAllGenre, getGenreDetail } from "../../../actions/GenreAction";
import { getAllMovie } from "../../../actions/MovieAction";
import { getHightLight } from "../../../actions/PhraseAction";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { TextField, Checkbox } from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";
import { Link } from "react-router-dom";
import { trackPromise } from "react-promise-tracker";
import Header from "../../layout/Header";
import { Modal } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";
import Highlighter from "react-highlight-words";
import Select from "react-select";

class Sentence extends Component {
  state = {
    selectedFile: null,
    sentence: "",
    translation: "",
    page: 1,
    size: 10,
    sortBy: "id",
    sort: "DESC",
    sorted: false,
    sentences: [],
    totalElements: 0,
    totalPages: 0,
    idDelete: "",
    listIdDelete: [],
    showModal: false,
    showModalMany: false,
    checkAll: false,
    vietnameseHightLight: [],
    englishHightLight: [],
    select: [
      { value: "50%", label: "50%" },
      { value: "60%", label: "60%" },
      { value: "70%", label: "70%" },
      { value: "80%", label: "80%" },
      { value: "90%", label: "90%" },
      { value: "100%", label: "100%" },
      { value: "150%", label: "150%" },
    ],
    selected: [{ value: "100%", label: "100%" }],
    movie: [],
    movieSelected: [],
    checkAutoPlaySentence: false,
    autoPlayCount: 0,
    checkPlayOneAudio: false,
    genres: [],
    genreSelected: [],
  };

  getPageSizeParam() {
    const query = new URLSearchParams(this.props.location.search);
    const page = parseInt(query.get("page"));
    const size = parseInt(query.get("size"));
    if (!isNaN(page) && !isNaN(size)) {
      this.setState(
        {
          page: page,
          size: size,
        },
        () => {
          trackPromise(this.props.getPagination(this.state));
        }
      );
    }
  }
  componentDidMount() {
    this.getPageSizeParam();
    trackPromise(this.props.getAllGenre());
    trackPromise(this.props.getAllMovie());
    trackPromise(this.props.getPagination(this.state));
  }

  componentWillReceiveProps(nextProps) {
    const movies = nextProps.movies;
    const allMovie = [];
    const genres = nextProps.genres;
    const allGenre = [];
    genres.forEach((g) => {
      let genre = { value: "", label: "" };
      genre.value = g.id;
      genre.label = g.name;
      allGenre.push(genre);
    });

    movies.forEach((m) => {
      let movie = { value: "", label: "" };
      movie.value = m.id;
      movie.label = m.name;
      allMovie.push(movie);
    });
    this.setState({
      movies: allMovie,
      genres: allGenre,
    });
    if (
      JSON.stringify(nextProps.sentences) !==
      JSON.stringify(this.props.sentences)
    ) {
      const sentences = nextProps.sentences;
      if (sentences.length > 0) {

        sentences.map((sentence) => {
          sentence.translations = [];
          if (sentence.translation.includes("\n")) {
            var s = sentence.translation.split("\n");
            sentence.translations = s;
          } else {
            sentence.translations.push(sentence.translation);
          }
          if (sentence.audio !== null && sentence.audio !== "") {
            // let file = require("../../../uploads/" + sentence.audio);
            sentence.file = "http://127.0.0.1:8010/" + sentence.audio;
          }
          sentence.isChecked = false;
        });
      }
      this.setState({
        page: nextProps.page + 1,
        sentences: sentences,
        totalElements: nextProps.totalElements,
        totalPages: nextProps.totalPages,
      });
    }

    if (
      nextProps.vietnameseHightLight !== undefined &&
      nextProps.vietnameseHightLight !== null &&
      nextProps.vietnameseHightLight !== this.props.vietnameseHightLight &&
      nextProps.englishHightLight !== undefined &&
      nextProps.englishHightLight !== null &&
      nextProps.englishHightLight !== this.props.englishHightLight
    ) {
      let vietnameseHightLight = [
        ...nextProps.vietnameseHightLight,
        this.state.translation,
      ];
      let englishHightLight = [
        ...nextProps.englishHightLight,
        this.state.sentence,
      ];
      this.setState({
        vietnameseHightLight,
        englishHightLight,
      });
    } else {
      if (
        nextProps.vietnameseHightLight !== undefined &&
        nextProps.vietnameseHightLight !== null &&
        nextProps.vietnameseHightLight !== this.props.vietnameseHightLight
      ) {
        let vietnameseHightLight = [
          ...nextProps.vietnameseHightLight,
          this.state.translation,
        ];
        let englishHightLight = [this.state.sentence];
        this.setState({
          vietnameseHightLight,
          englishHightLight,
        });
      } else {
        this.setState({
          vietnameseHightLight: [this.state.vietnamese],
        });
      }

      if (
        nextProps.englishHightLight !== undefined &&
        nextProps.englishHightLight !== null &&
        nextProps.englishHightLight !== this.props.englishHightLight
      ) {
        let englishHightLight = [
          ...nextProps.englishHightLight,
          this.state.sentence,
        ];
        let vietnameseHightLight = [this.state.translation];
        this.setState({
          vietnameseHightLight,
          englishHightLight,
        });
      } else {
        this.setState({
          englishHightLight: [this.state.sentence],
        });
      }
    }
  }

  onChangeInput = (e) => {
    let name = e.target.name;
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        if (name === "sentence") {
          let englishHightLight = [this.state.sentence.trim()];
          this.setState({
            englishHightLight: englishHightLight,
          });
        } else {
          let vietnameseHightLight = [this.state.translation.trim()];
          this.setState({
            vietnameseHightLight: vietnameseHightLight,
          });
        }
      }
    );
  };

  search = (e) => {
    this.setState(
      {
        page: 1,
      },
      () => {
        trackPromise(this.props.getPagination(this.state)).then(() => {
          let params = {
            name: this.state.sentence,
            translation: this.state.translation,
          };
          trackPromise(this.props.getHightLight(params));
        });
      }
    );
  };

  searchEnter = (e) => {
    if (e.key === "Enter") {
      this.setState(
        {
          page: 1,
        },
        () => {
          trackPromise(this.props.getPagination(this.state)).then(() => {
            let params = {
              name: this.state.sentence,
              translation: this.state.translation,
            };
            trackPromise(this.props.getHightLight(params));
          });
        }
      );
    }
  };

  onChangeSizePage = (e) => {
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        trackPromise(this.props.getPagination(this.state)).then(() => {
          let params = {
            name: this.state.sentence,
            translation: this.state.translation,
          };
          trackPromise(this.props.getHightLight(params));
        });
      }
    );
  };
  selectFile = (e) => {
    this.setState(
      {
        selectedFile: e.target.files[0],
      },
      () => {
        trackPromise(this.props.readFile(this.state)).then(() => {
          window.location.reload();
        });
      }
    );
  };
  onChangePage = (e, page) => {
    this.setState(
      {
        page: page,
        checkAutoPlaySentence: false,
      },
      () => {
        this.props.history.push(
          "/sentence?page=" + this.state.page + "&size=" + this.state.size
        );
        trackPromise(this.props.getPagination(this.state)).then(() => {
          let params = {
            name: this.state.sentence,
            translation: this.state.translation,
          };
          trackPromise(this.props.getHightLight(params));
        });
      }
    );
  };

  onChangeCheckboxAll = () => {
    this.state.checkAll = !this.state.checkAll;
    const listIdDelete = [];
    this.state.sentences.map((value) => {
      value.isChecked = this.state.checkAll;
      if (this.state.checkAll) {
        listIdDelete.push(value.id);
      }
    });

    //  SET STATUS CHECK ALL
    this.setState({
      checkAll: this.state.checkAll,
      listIdDelete: listIdDelete,
    });
  };

  onChangeCheckbox = (value) => {
    value.isChecked = !value.isChecked;
    let { sentences } = this.state;
    let countChecked = 0;
    const listIdDelete = [];
    sentences.map((sentence) => {
      if (sentence.sentence === value.sentence) {
        sentence.isChecked = value.isChecked;
      }

      if (sentence.isChecked === true) {
        countChecked++;
        listIdDelete.push(sentence.id);
      }
    });
    if (countChecked === sentences.length) {
      this.setState({
        checkAll: true,
      });
    } else {
      this.setState({
        checkAll: false,
      });
    }
    this.setState({
      sentences: sentences,
      listIdDelete: listIdDelete,
    });
  };

  reloadPage = () => {
    this.setState(
      {
        sentence: "",
        translation: "",
        sortBy: "id",
        sort: "DESC",
        sorted: false,
        sentences: [],
        totalElements: 0,
        totalPages: 0,
        idDelete: "",
        listIdDelete: [],
        showModal: false,
        showModalMany: false,
        checkAll: false,
        vietnameseHightLight: [],
        movieSelected: [],
        checkPlayOneAudio: false,
        checkAutoPlaySentence: false,
        selected: [{ value: "100%", label: "100%" }],
      },
      () => {
        this.getPageSizeParam();
        trackPromise(this.props.getPagination(this.state)).then(() => {
          let params = {
            name: this.state.sentence,
            translation: this.state.translation,
          };
          trackPromise(this.props.getHightLight(params));
        });
      }
    );
  };

  deleteModal = (id) => {
    this.setState({
      showModal: true,
      idDelete: id,
    });
  };

  handleCloseModal = () => this.setState({ showModal: false });

  deleteSentence = () => {
    const id = this.state.idDelete;
    this.props.deleteById(id).then(() => {
      this.handleCloseModal();
      trackPromise(this.props.getPagination(this.state)).then(() => { });
    });
  };

  deleteManyModal = () => {
    if (this.state.listIdDelete.length > 0) {
      this.setState({
        showModalMany: true,
      });
    } else {
      toast.warning("Please Select One Sentence");
    }
  };

  handleCloseModalMany = () => this.setState({ showModalMany: false });

  deleteManySentence = () => {
    const listIdDelete = this.state.listIdDelete;
    this.props.deleteList(listIdDelete).then(() => {
      this.handleCloseModalMany();
      trackPromise(this.props.getPagination(this.state));
    });
  };

  sortBy = (fieldName) => {
    let sorted = this.state.sorted;
    sorted = !sorted;
    if (this.state.sorted) {
      this.setState({
        sort: "ASC",
      });
    } else {
      this.setState({
        sort: "DESC",
      });
    }
    this.setState(
      {
        sortBy: fieldName,
        sorted: sorted,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };

  onChangeRepeatAudio = (audioId) => {
    let repeatAudio = document.getElementById(
      "audio-element-repeat-" + audioId
    );
    let audio = document.getElementById("audio-element-" + audioId);
    if (repeatAudio.value == "true") {
      audio.loop = true;
      audio.autoPlay = true;
      audio.play();
    } else {
      audio.loop = false;
      audio.autoPlay = true;
      audio.play();
    }
  };
  onChangeSelectOneAudioSpeed = (audioId) => {
    let speedAudio = document.getElementById("audio-element-speed-" + audioId);
    let audio = document.getElementById("audio-element-" + audioId);
    if (speedAudio.value === "50%") {
      audio.playbackRate = 0.5;
    } else if (speedAudio.value === "60%") {
      audio.playbackRate = 0.6;
    } else if (speedAudio.value === "70%") {
      audio.playbackRate = 0.7;
    } else if (speedAudio.value === "80%") {
      audio.playbackRate = 0.8;
    } else if (speedAudio.value === "90%") {
      audio.playbackRate = 0.9;
    } else if (speedAudio.value === "100%") {
      audio.playbackRate = 1;
    } else {
      audio.playbackRate = 1.5;
    }
  };

  onChangeSelectAudioSpeed = (speed) => {
    let audio = null;
    this.state.sentences.map((sentence) => {
      audio = document.getElementById("audio-element-" + sentence.id);
    });

    if (speed.value === "50%") {
      this.state.sentences.map((sentence) => {
        audio = document.getElementById("audio-element-" + sentence.id);
        audio.playbackRate = 0.5;
      });
      this.setState({
        selected: [{ value: "50%", label: "50%" }],
      });
    } else if (speed.value === "60%") {
      this.state.sentences.map((sentence) => {
        audio = document.getElementById("audio-element-" + sentence.id);
        audio.playbackRate = 0.6;
      });
      this.setState({
        selected: [{ value: "60%", label: "60%" }],
      });
    } else if (speed.value === "70%") {
      this.state.sentences.map((sentence) => {
        audio = document.getElementById("audio-element-" + sentence.id);
        audio.playbackRate = 0.7;
      });
      this.setState({
        selected: [{ value: "70%", label: "70%" }],
      });
    } else if (speed.value === "80%") {
      this.state.sentences.map((sentence) => {
        audio = document.getElementById("audio-element-" + sentence.id);
        audio.playbackRate = 0.8;
      });
      this.setState({
        selected: [{ value: "80%", label: "80%" }],
      });
    } else if (speed.value === "90%") {
      this.state.sentences.map((sentence) => {
        audio = document.getElementById("audio-element-" + sentence.id);
        audio.playbackRate = 0.9;
      });
      this.setState({
        selected: [{ value: "90%", label: "90%" }],
      });
    } else if (speed.value === "100%") {
      this.state.sentences.map((sentence) => {
        audio = document.getElementById("audio-element-" + sentence.id);
        audio.playbackRate = 1;
      });
      this.setState({
        selected: [{ value: "100%", label: "100%" }],
      });
    } else {
      this.state.sentences.map((sentence) => {
        audio = document.getElementById("audio-element-" + sentence.id);
        audio.playbackRate = 1.5;
      });
      this.setState({
        selected: [{ value: "150%", label: "150%" }],
      });
    }
  };
  onChangeSelectMovie = (movie) => {
    this.setState(
      {
        movieSelected: [movie],
        checkAutoPlaySentence: false,
        checkPlayOneAudio: false,
        page: 1,
      },
      () => {
        this.props.getPagination(this.state);
      }
    );
  };

  onChangeSelectGenres = (genre) => {
    this.setState(
      {
        genreSelected: [genre],
      },
      () => {
        trackPromise(this.props.getGenreDetail(this.state.genreSelected[0].value)).then(() => {
          console.log(this.props);
          const movies = this.props.genreDetail;
          const allMovie = [];
        
          movies.forEach((m) => {
            let movie = { value: "", label: "" };
            movie.value = m.id;
            movie.label = m.name;
            allMovie.push(movie);
          });
          this.setState({
            movies: allMovie,
          });
        });
      }
    );
  };

  playOneAudio = () => {
    this.setState({
      playOneAudio: true,
    });
  };
  autoPlaySentence = () => {
    this.setState(
      {
        checkAutoPlaySentence: !this.state.checkAutoPlaySentence,
      },
      () => {
        var currentAudio = 0;
        if (this.state.checkAutoPlaySentence) {
          var audio = null;
          for (var i = 0; i < this.state.sentences.length; i++) {
            audio = document.getElementById(
              "audio-element-" + this.state.sentences[i].id
            );
            if (audio.currentTime > 0) {
              currentAudio = i;
            }
          }

          for (var i = currentAudio; i < this.state.sentences.length; i++) {
            let audio = document.getElementById(
              "audio-element-" + this.state.sentences[i].id
            );

            audio.loop = false;
            if (i == 0) {
              audio.play();
            } else {
              let previousAudio = document.getElementById(
                "audio-element-" + this.state.sentences[i - 1].id
              );

              if (this.state.playOneAudio) {
                if (audio.currentTime > 0) {
                  audio.play();
                } else {
                  let previousAudio = document.getElementById(
                    "audio-element-" + this.state.sentences[i - 1].id
                  );

                  if (previousAudio.ended) {
                    audio.play();
                  } else {
                    previousAudio.addEventListener("ended", () => {
                      audio.play();
                    });
                  }
                }
              } else {
                if (previousAudio.ended) {
                  audio.play();
                } else {
                  previousAudio.addEventListener("ended", () => {
                    audio.play();
                  });
                }
              }
            }
          }
        } else {
          var audio = null;
          for (var i = 0; i < this.state.sentences.length; i++) {
            audio = document.getElementById(
              "audio-element-" + this.state.sentences[i].id
            );
            if (audio.currentTime > 0) {
              currentAudio = i;
            }
          }

          audio = document.getElementById(
            "audio-element-" + this.state.sentences[currentAudio].id
          );
          audio.pause();
        }
      }
    );
  };

  render() {
    return (
      <React.Fragment>
        <ToastContainer
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <Modal
          name="delete"
          show={this.state.showModal}
          animation={false}
          onHide={this.handleCloseModal}
        >
          <Modal.Header closeButton className="bg-warning white">
            <Modal.Title>MODAL DELETE</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete this sentence?</Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={this.deleteSentence}
            >
              OK
            </button>
            <button
              type="button"
              className="btn grey btn-outline-secondary "
              onClick={this.handleCloseModal}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>

        <Modal
          name="deleteAll"
          show={this.state.showModalMany}
          animation={false}
          onHide={this.handleCloseModalMany}
        >
          <Modal.Header closeButton className="bg-warning white">
            <Modal.Title>MODAL DELETE</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete these sentences?</Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={this.deleteManySentence}
            >
              OK
            </button>
            <button
              type="button"
              className="btn grey btn-outline-secondary "
              onClick={this.handleCloseModalMany}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>

        <Header />
        <MenuSideBar />
        {/* Panel search */}

        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />

          <div className="content-wrapper">
            <div className="content-header row">
              <div className="content-header-left col-md-4 col-12 mb-2">
                <h3 className="content-header-title mb-0">Sentence</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item active">Sentence</li>
                    </ol>
                  </div>
                </div>
              </div>
              <div className="content-header-right text-md-right col-md-8 col-12">
                <div className="form-group">
                  <Link
                    className="btn-icon btn fonticon-container"
                    type="button"
                    to="/sentence/create"
                  >
                    <i className="fa fa-plus"></i>
                    <label className="fonticon-classname">Add Sentence</label>
                  </Link>
                  <button
                    className="btn-icon btn fonticon-container"
                    type="button"
                    onClick={() => this.deleteManyModal()}
                  >
                    <i className="fa fa-times"></i>
                    <label className="fonticon-classname">Delete</label>
                  </button>
                  <button
                    className="btn-icon btn fonticon-container"
                    type="button"
                    onClick={this.reloadPage}
                  >
                    <i className="fa fa-refresh"></i>
                    <label className="fonticon-classname">Refresh</label>
                  </button>
                  {!this.state.checkAutoPlaySentence ? (
                    <button
                      className="btn-icon btn fonticon-container"
                      type="button"
                      onClick={this.autoPlaySentence}
                    >
                      <i className="fa fa-play"></i>
                      <label className="fonticon-classname">PLAY</label>
                    </button>
                  ) : null}

                  {this.state.checkAutoPlaySentence ? (
                    <button
                      className="btn-icon btn fonticon-container"
                      type="button"
                      onClick={this.autoPlaySentence}
                      id="autoPlayId"
                    >
                      <i className="fa fa-pause"></i>
                      <label className="fonticon-classname">PAUSE</label>
                    </button>
                  ) : null}
                </div>
              </div>
            </div>
            <div className="content-body">
              {/* Basic Elements start */}
              <section className="basic-elements">
                <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      <div className="card-content">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <TextField
                                id="sentenceId"
                                label="Sentence"
                                className="form-control"
                                color="secondary"
                                value={this.state.sentence}
                                name="sentence"
                                onChange={(e) => this.onChangeInput(e)}
                                onKeyDown={this.searchEnter}
                              />
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <TextField
                                id="translationId"
                                label="Translation"
                                className="form-control"
                                color="secondary"
                                name="translation"
                                value={this.state.translation}
                                onChange={(e) => this.onChangeInput(e)}
                                onKeyDown={this.searchEnter}
                              />
                            </div>

                            <div className="col-xl-3 col-lg-6 col-md-12 mb-1">
                              <Select
                                options={this.state.genres}
                                className="border-primary"
                                onChange={(e) => this.onChangeSelectGenres(e)}
                                value={this.state.genreSelected}
                              />
                            </div>
                            <div className="col-xl-1 col-lg-1 col-md-1 mb-1">
                              <a
                                className="btn btn-social-icon btn-outline-twitter btn-outline-cyan"
                                onClick={this.search}
                              >
                                <span className="fa fa-search"></span>
                              </a>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <Select
                                options={this.state.movies}
                                className="border-primary"
                                onChange={(e) => this.onChangeSelectMovie(e)}
                                value={this.state.movieSelected}
                              />
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <Select
                                options={this.state.select}
                                className="border-primary"
                                onChange={(e) =>
                                  this.onChangeSelectAudioSpeed(e)
                                }
                                value={this.state.selected}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              {/* Basic Inputs end */}

              <section id="decimal">
                <div className="row">
                  <div className="col-12">
                    <div className="card">
                      <div className="card-header">
                        <a href="/#" className="heading-elements-toggle">
                          <i className="fa fa-ellipsis-v font-medium-3" />
                        </a>
                        <div className="heading-elements">
                          <ul className="list-inline mb-0">
                            <li>
                              <a data-action="collapse">
                                <i className="feather icon-minus" />
                              </a>
                            </li>
                            <li>
                              <a data-action="reload" onClick={this.reloadPage}>
                                <i className="feather icon-rotate-cw" />
                              </a>
                            </li>
                            <li>
                              <a data-action="expand">
                                <i className="feather icon-maximize" />
                              </a>
                            </li>
                            <li>
                              <a data-action="close">
                                <i className="feather icon-x" />
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="card-content collapse show">
                        <div className="card-body card-dashboard">
                          <table className="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th style={{ width: "5%" }}>
                                  <Checkbox
                                    checked={this.state.checkAll}
                                    onChange={this.onChangeCheckboxAll}
                                    name="check"
                                    color="primary"
                                    value="checkAll"
                                  />
                                </th>
                                <th style={{ width: "5%" }}>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("id")}
                                  >
                                    ID
                                  </a>
                                </th>
                                <th>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("sentence")}
                                  >
                                    Sentence
                                  </a>
                                </th>
                                <th>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("translation")}
                                  >
                                    Translation
                                  </a>
                                </th>
                                <th style={{ width: "35%" }}>
                                  <a className="tableHeaderLink">Audio</a>
                                </th>
                                <th style={{ width: "10%" }}>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.sentences.map((value, index) => (
                                <tr key={index}>
                                  <td>
                                    <Checkbox
                                      checked={value.isChecked}
                                      color="primary"
                                      onChange={() =>
                                        this.onChangeCheckbox(value)
                                      }
                                    />
                                  </td>
                                  <td className="styleTD">
                                    {index +
                                      (this.state.page - 1) * this.state.size +
                                      1}
                                  </td>
                                  <td className="styleTD">
                                    <Highlighter
                                      highlightStyle={{
                                        color: "red",
                                        fontWeight: "bold",
                                      }}
                                      searchWords={this.state.englishHightLight}
                                      autoEscape={true}
                                      textToHighlight={value.sentence}
                                    />
                                  </td>
                                  <td className="styleTD">
                                    {value.translations.map(
                                      (translation, index) => {
                                        if (value.translations.length == 1) {
                                          return <Highlighter
                                            key={index}
                                            highlightStyle={{
                                              color: "red",
                                              fontWeight: "bold",
                                            }}
                                            searchWords={
                                              this.state.vietnameseHightLight
                                            }
                                            autoEscape={true}
                                            textToHighlight={translation}
                                          />;
                                        } else {
                                          if (
                                            index ===
                                            value.translations.length - 1
                                          ) {
                                            return <div>
                                              <Highlighter
                                                highlightStyle={{
                                                  color: "red",
                                                  fontWeight: "bold",
                                                }}
                                                searchWords={
                                                  this.state.vietnameseHightLight
                                                }
                                                autoEscape={true}
                                                textToHighlight={translation}
                                              />
                                            </div>;
                                          } else {
                                            return <div><Highlighter
                                              highlightStyle={{
                                                color: "red",
                                                fontWeight: "bold",
                                              }}
                                              searchWords={
                                                this.state.vietnameseHightLight
                                              }
                                              autoEscape={true}
                                              textToHighlight={translation}
                                            /><hr /></div>;
                                          }
                                        }
                                      }
                                    )}

                                  </td>

                                  <td>
                                    <ReactAudioPlayer
                                      ref="audio"
                                      src={value.file ? value.file : ""}
                                      controls
                                      id={"audio-element-" + value.id}
                                      loop={true}
                                      autoPlay={false}
                                      className="audio"
                                      onPlay={this.playOneAudio}
                                    />
                                    <select
                                      name="repeat"
                                      className="custom-select custom-select-md custom-select-sm form-control form-control-sm repeatAudioSelect"
                                      onChange={() =>
                                        this.onChangeRepeatAudio(value.id)
                                      }
                                      id={"audio-element-repeat-" + value.id}
                                    >
                                      <option value="true">TRUE</option>
                                      <option value="false">FALSE</option>
                                    </select>
                                    <select
                                      name="speed"
                                      className="custom-select custom-select-md custom-select-sm form-control form-control-sm repeatAudioSelect"
                                      onChange={() =>
                                        this.onChangeSelectOneAudioSpeed(
                                          value.id
                                        )
                                      }
                                      id={"audio-element-speed-" + value.id}
                                    >
                                      <option defaultValue="100%">100%</option>
                                      <option value="50%">50%</option>
                                      <option value="60%">60%</option>
                                      <option value="70%">70%</option>
                                      <option value="80%">80%</option>
                                      <option value="90%">90%</option>
                                      <option value="150%">150%</option>
                                    </select>
                                  </td>
                                  <td>
                                    <Link
                                      className="btn-icon btn primary editButton"
                                      type="button"
                                      to={
                                        `/sentence/update/${value.id}?page=` +
                                        this.state.page +
                                        "&size=" +
                                        this.state.size
                                      }
                                    >
                                      <i className="fa fa-pencil-square"></i>
                                    </Link>
                                    <button
                                      className="btn-icon btn danger deleteButton"
                                      type="button"
                                      onClick={() => this.deleteModal(value.id)}
                                    >
                                      <i className="fa fa-trash"></i>
                                    </button>
                                  </td>
                                </tr>
                              ))}
                            </tbody>
                          </table>

                          <div className="row">
                            <div className="offset-5 col-2 text-right">
                              <label className="control-label labelPagination">
                                Show
                              </label>
                            </div>
                            <div className="col-1">
                              <select
                                name="size"
                                className="custom-select custom-select-sm form-control form-control-sm"
                                value={this.state.size}
                                onChange={this.onChangeSizePage}
                              >
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                              </select>
                            </div>
                            <div className="col-4">
                              <Pagination
                                name="page"
                                count={this.state.totalPages}
                                color="primary"
                                showFirstButton
                                showLastButton
                                onChange={this.onChangePage}
                                page={this.state.page}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
        {/* Modal */}

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  sentences: state.sentences.sentences,
  vietnameseHightLight: state.phrases.phrases.vietnamesePhrasesSet,
  englishHightLight: state.phrases.phrases.englishPhrasesSet,
  totalPages: state.sentences.totalPages,
  totalElements: state.sentences.totalElements,
  page: state.sentences.page,
  deleteSuccess: state.sentences.deleteSuccess,
  movies: state.movies.allMovie,
  genres: state.genres.genres,
  genreDetail: state.genres.genreDetail,
});

export default connect(mapStateToProps, {
  getAllGenre,
  getGenreDetail,
  getAllMovie,
  getPagination,
  deleteById,
  deleteList,
  getHightLight,
  readFile,
})(Sentence);

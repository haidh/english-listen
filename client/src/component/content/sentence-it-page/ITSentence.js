import React, { Component } from "react";
import "../../../style.css";
import { connect } from "react-redux";
import {
  getPagination,
  deleteById,
  deleteList,
} from "../../../actions/ITSentenceAction";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { TextField, Checkbox } from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";
import { Link } from "react-router-dom";
import { trackPromise } from "react-promise-tracker";
import Header from "../../layout/Header";
import { Modal } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";
import Highlighter from "react-highlight-words";
import { getHightLight } from "../../../actions/PhraseAction";

class ITSentence extends Component {
  state = {
    sentence: "",
    translation: "",
    page: 1,
    size: 10,
    sortBy: "id",
    sort: "DESC",
    sorted: false,
    sentences: [],
    totalElements: 0,
    totalPages: 0,
    idDelete: "",
    listIdDelete: [],
    showModal: false,
    showModalMany: false,
    checkAll: false,
    vietnameseHightLight: [],
    englishHightLight: [],
  };

  componentDidMount() {
    trackPromise(this.props.getPagination(this.state));
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
    if (
      JSON.stringify(nextProps.sentences) !==
      JSON.stringify(this.props.sentences)
    ) {
      const sentences = nextProps.sentences;
      if (sentences.length > 0) {
        sentences.map((sentence) => {
          sentence.isChecked = false;
        });
      }
      this.setState({
        page: nextProps.page + 1,
        sentences: sentences,
        totalElements: nextProps.totalElements,
        totalPages: nextProps.totalPages,
      });
      return this.state;
    }

    if (
      nextProps.vietnameseHightLight !== undefined &&
      nextProps.vietnameseHightLight !== null &&
      nextProps.vietnameseHightLight !== this.props.vietnameseHightLight
    ) {
      let vietnameseHightLight = [
        ...nextProps.vietnameseHightLight,
        this.state.translation,
      ];
      let englishHightLight = [this.state.sentence];
      this.setState({
        vietnameseHightLight,
        englishHightLight,
      });
    } else {
      this.setState({
        vietnameseHightLight: [this.state.vietnamese],
      });
    }

    if (
      nextProps.englishHightLight !== undefined &&
      nextProps.englishHightLight !== null &&
      nextProps.englishHightLight !== this.props.englishHightLight
    ) {
      let englishHightLight = [
        ...nextProps.englishHightLight,
        this.state.sentence,
      ];
      let vietnameseHightLight = [this.state.translation];
      this.setState({
        vietnameseHightLight,
        englishHightLight,
      });
    } else {
      this.setState({
        englishHightLight: [this.state.sentence],
      });
    }
  }

  search = () => {
    trackPromise(this.props.getPagination(this.state)).then(() => {
      let params = {
        name: this.state.sentence,
        translation: this.state.translation,
      };
      trackPromise(this.props.getHightLight(params));
    });
  };

  onChangeSizePage = (e) => {
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };

  onChangePage = (e, page) => {
    this.setState(
      {
        page: page,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };

  onChangeCheckboxAll = () => {
    this.state.checkAll = !this.state.checkAll;
    const listIdDelete = [];
    this.state.sentences.map((value) => {
      value.isChecked = this.state.checkAll;
      if (this.state.checkAll) {
        listIdDelete.push(value.id);
      }
    });

    //  SET STATUS CHECK ALL
    this.setState({
      checkAll: this.state.checkAll,
      listIdDelete: listIdDelete,
    });
  };

  onChangeCheckbox = (value) => {
    value.isChecked = !value.isChecked;
    let { sentences } = this.state;
    let countChecked = 0;
    const listIdDelete = [];
    sentences.map((sentence) => {
      if (sentence.sentence === value.sentence) {
        sentence.isChecked = value.isChecked;
      }

      if (sentence.isChecked === true) {
        countChecked++;
        listIdDelete.push(sentence.id);
      }
    });
    if (countChecked === sentences.length) {
      this.setState({
        checkAll: true,
      });
    } else {
      this.setState({
        checkAll: false,
      });
    }
    this.setState({
      sentences: sentences,
      listIdDelete: listIdDelete,
    });
  };

  reloadPage = () => {
    this.setState(
      {
        sentence: "",
        description: "",
        page: 1,
        size: 10,
        sortBy: "id",
        sort: "DESC",
        sorted: false,
        movies: [],
        totalElements: 0,
        totalPages: 0,
        idDelete: "",
        listIdDelete: [],
        showModal: false,
        showModalMany: false,
        checkAll: false,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };

  deleteModal = (id) => {
    this.setState({
      showModal: true,
      idDelete: id,
    });
  };

  handleCloseModal = () => this.setState({ showModal: false });

  deleteITSentence = () => {
    const id = this.state.idDelete;
    this.props.deleteById(id).then(() => {
      this.handleCloseModal();
      trackPromise(this.props.getPagination(this.state)).then(() => {});
    });
  };

  deleteManyModal = () => {
    if (this.state.listIdDelete.length > 0) {
      this.setState({
        showModalMany: true,
      });
    } else {
      toast.warning("Please Select One Sentence");
    }
  };

  handleCloseModalMany = () => this.setState({ showModalMany: false });

  deleteManyITSentence  = () => {
    const listIdDelete = this.state.listIdDelete;
    this.props.deleteList(listIdDelete).then(() => {
      this.handleCloseModalMany();
      trackPromise(this.props.getPagination(this.state));
    });
  };

  sortBy = (fieldName) => {
    let sorted = this.state.sorted;
    sorted = !sorted;
    if (this.state.sorted) {
      this.setState({
        sort: "ASC",
      });
    } else {
      this.setState({
        sort: "DESC",
      });
    }
    this.setState(
      {
        sortBy: fieldName,
        sorted: sorted,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };

  onChangeInput = (e) => {
    let name = e.target.name;
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        if (name === "sentence") {
          let englishHightLight = [this.state.sentence];
          this.setState({
            englishHightLight: englishHightLight,
          });
        } else {
          let vietnameseHightLight = [this.state.translation];
          this.setState({
            vietnameseHightLight: vietnameseHightLight,
          });
        }
      }
    );
  };

  searchEnter = (e) => {
    if (e.key === "Enter") {
      trackPromise(this.props.getPagination(this.state)).then(() => {
        let params = {
          name: this.state.sentence,
          translation: this.state.translation,
        };
        trackPromise(this.props.getHightLight(params));
      });
    }
  };
  render() {
    return (
      <React.Fragment>
        <ToastContainer
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <Modal
          name="delete"
          show={this.state.showModal}
          animation={false}
          onHide={this.handleCloseModal}
        >
          <Modal.Header closeButton className="bg-warning white">
            <Modal.Title>MODAL DELETE</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete this sentence?</Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={this.deleteITSentence}
            >
              OK
            </button>
            <button
              type="button"
              className="btn grey btn-outline-secondary "
              onClick={this.handleCloseModal}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>

        <Modal
          name="deleteAll"
          show={this.state.showModalMany}
          animation={false}
          onHide={this.handleCloseModalMany}
        >
          <Modal.Header closeButton className="bg-warning white">
            <Modal.Title>MODAL DELETE</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete these sentences?</Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={this.deleteManyITSentence }
            >
              OK
            </button>
            <button
              type="button"
              className="btn grey btn-outline-secondary "
              onClick={this.handleCloseModalMany}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>

        <Header />
        <MenuSideBar />
        {/* Panel search */}

        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />

          <div className="content-wrapper">
            <div className="content-header row">
              <div className="content-header-left col-md-6 col-12 mb-2">
                <h3 className="content-header-title mb-0">IT Sentence</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item active">IT Sentence</li>
                    </ol>
                  </div>
                </div>
              </div>
              <div className="content-header-right text-md-right col-md-6 col-12">
                <div className="form-group">
                  <Link
                    className="btn-icon btn fonticon-container"
                    type="button"
                    to="/it/create"
                  >
                    <i className="fa fa-plus"></i>
                    <label className="fonticon-classname">Add Sentence</label>
                  </Link>
                  <button
                    className="btn-icon btn fonticon-container"
                    type="button"
                    onClick={() => this.deleteManyModal()}
                  >
                    <i className="fa fa-times"></i>
                    <label className="fonticon-classname">Delete</label>
                  </button>
                  <button
                    className="btn-icon btn fonticon-container"
                    type="button"
                    onClick={this.reloadPage}
                  >
                    <i className="fa fa-refresh"></i>
                    <label className="fonticon-classname">Refresh</label>
                  </button>
                </div>
              </div>
            </div>
            <div className="content-body">
              {/* Basic Elements start */}
              <section className="basic-elements">
                <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      <div className="card-content">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <TextField
                                id="sentenceId"
                                label="Sentence"
                                className="form-control"
                                color="secondary"
                                value={this.state.sentence}
                                name="sentence"
                                onChange={(e) => this.onChangeInput(e)}
                                onKeyDown={this.searchEnter}
                              />
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <TextField
                                id="translationId"
                                label="Translation"
                                className="form-control"
                                color="secondary"
                                name="translation"
                                value={this.state.translation}
                                onChange={(e) => this.onChangeInput(e)}
                                onKeyDown={this.searchEnter}
                              />
                            </div>
                            <div className="col-xl-1 col-lg-1 col-md-1 mb-1">
                              <a
                                className="btn btn-social-icon btn-outline-twitter btn-outline-cyan"
                                onClick={this.search}
                              >
                                <span className="fa fa-search"></span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              {/* Basic Inputs end */}

              <section id="decimal">
                <div className="row">
                  <div className="col-12">
                    <div className="card">
                      <div className="card-header">
                        <a href="/#" className="heading-elements-toggle">
                          <i className="fa fa-ellipsis-v font-medium-3" />
                        </a>
                        <div className="heading-elements">
                          <ul className="list-inline mb-0">
                            <li>
                              <a data-action="collapse">
                                <i className="feather icon-minus" />
                              </a>
                            </li>
                            <li>
                              <a data-action="reload" onClick={this.reloadPage}>
                                <i className="feather icon-rotate-cw" />
                              </a>
                            </li>
                            <li>
                              <a data-action="expand">
                                <i className="feather icon-maximize" />
                              </a>
                            </li>
                            <li>
                              <a data-action="close">
                                <i className="feather icon-x" />
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="card-content collapse show">
                        <div className="card-body card-dashboard">
                          <table className="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th style={{ width: "5%" }}>
                                  <Checkbox
                                    checked={this.state.checkAll}
                                    onChange={this.onChangeCheckboxAll}
                                    name="check"
                                    color="primary"
                                    value="checkAll"
                                  />
                                </th>
                                <th style={{ width: "5%" }}>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("id")}
                                  >
                                    ID
                                  </a>
                                </th>
                                <th>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("sentence")}
                                  >
                                    Sentence
                                  </a>
                                </th>
                                <th>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("translation")}
                                  >
                                    Translation
                                  </a>
                                </th>

                                <th style={{ width: "10%" }}>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.sentences.map((value, index) => (
                                <tr key={index}>
                                  <td>
                                    <Checkbox
                                      checked={value.isChecked}
                                      color="primary"
                                      onChange={() =>
                                        this.onChangeCheckbox(value)
                                      }
                                    />
                                  </td>
                                  <td className="styleTD">
                                    {index +
                                      (this.state.page - 1) * this.state.size +
                                      1}
                                  </td>
                                  <td className="styleTD">
                                    <Highlighter
                                      highlightStyle={{
                                        color: "red",
                                        fontWeight: "bold",
                                      }}
                                      searchWords={this.state.englishHightLight}
                                      autoEscape={true}
                                      textToHighlight={value.sentence}
                                    />
                                  </td>
                                  <td className="styleTD">
                                    <Highlighter
                                      highlightStyle={{
                                        color: "red",
                                        fontWeight: "bold",
                                      }}
                                      searchWords={
                                        this.state.vietnameseHightLight
                                      }
                                      autoEscape={true}
                                      textToHighlight={value.translation}
                                    />
                                  </td>
                                  <td>
                                    <Link
                                      className="btn-icon btn primary editButton"
                                      type="button"
                                      to={`/it/update/${value.id}`}
                                    >
                                      <i className="fa fa-pencil-square"></i>
                                    </Link>
                                    <button
                                      className="btn-icon btn danger deleteButton"
                                      type="button"
                                      onClick={() => this.deleteModal(value.id)}
                                    >
                                      <i className="fa fa-trash"></i>
                                    </button>
                                  </td>
                                </tr>
                              ))}
                            </tbody>
                          </table>

                          <div className="row">
                            <div className="offset-5 col-2 text-right">
                              <label className="control-label labelPagination">
                                Show
                              </label>
                            </div>
                            <div className="col-1">
                              <select
                                name="size"
                                className="custom-select custom-select-sm form-control form-control-sm"
                                value={this.state.size}
                                onChange={this.onChangeSizePage}
                              >
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                              </select>
                            </div>
                            <div className="col-4">
                              <Pagination
                                name="page"
                                count={this.state.totalPages}
                                color="primary"
                                showFirstButton
                                showLastButton
                                onChange={this.onChangePage}
                                page={this.state.page}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
        {/* Modal */}

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  sentences: state.itSentences.itSentencesPagination,
  vietnameseHightLight: state.phrases.phrases.vietnamesePhrasesSet,
  englishHightLight: state.phrases.phrases.englishPhrasesSet,
  totalPages: state.itSentences.totalPages,
  totalElements: state.itSentences.totalElements,
  page: state.itSentences.page,
});

export default connect(mapStateToProps, {
  getPagination,
  deleteById,
  deleteList,
  getHightLight,
})(ITSentence);

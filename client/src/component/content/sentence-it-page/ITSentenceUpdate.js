import React, { Component } from "react";
import "../../../style.css";
import Header from "../../layout/Header";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { update, getDetail } from "../../../actions/ITSentenceAction";
import { trackPromise } from "react-promise-tracker";
import { ToastContainer } from "react-toastify";
import classnames from "classnames";
import Select from "react-select";

class ITSentenceUpdate extends Component {
  state = {
    sentence: { value: "", isInvalid: false, message: "" },
    translation: { value: "", isInvalid: false, message: "" },
    isInvalidForm: true,
    errors: {},
  };

  componentDidMount() {
    const id = this.props.match.params.id;

    trackPromise(this.props.getDetail(id)).then(() => {
      console.log(this.props);
      this.setState({
        sentence: { value: this.props.sentence.sentence },
        translation: { value: this.props.sentence.translation },
      });
    });
  }
  componentWillReceiveProps(nextProps) {
    if (
      JSON.stringify(nextProps.errors) !== JSON.stringify(this.props.errors)
    ) {
      this.setState({
        errors: nextProps.errors,
      });
    }
  }

  reload = () => {
    this.setState({
      sentence: { value: "", isInvalid: false, message: "" },
      translation: { value: "", isInvalid: false, message: "" },
      isInvalidForm: true,
    });
  };

  onChangeInput = (e) => {
    this.setState({
      [e.target.name]: {
        ...this.state[e.target.name],
        value: e.target.value,
      },
    });
  };

  update = (e) => {
    const id = this.props.match.params.id;
    e.preventDefault();
    let params = {
      id: id,
      sentence: this.state.sentence.value,
      translation: this.state.translation.value,
    };
    trackPromise(this.props.update(params, this.props.history)).then(() => {
      this.setState({
        sentence: { value: "", isInvalid: false, message: "" },
        translation: { value: "", isInvalid: false, message: "" },
        errors: this.props.errors,
      });
    });
  };

  render() {
    const { sentence, translation, errors } = this.state;
    return (
      <React.Fragment>
        <Header />
        <MenuSideBar />
        {/* Panel search */}
        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />
          <div className="content-wrapper">
            <div className="content-header row">
              <ToastContainer
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />
              <div className="content-header-left col-md-6 col-12 mb-2">
                <h3 className="content-header-title mb-0">Sentence</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item">
                        <Link to="/sentence">Sentence</Link>
                      </li>
                      <li className="breadcrumb-item active">
                        Create IT Sentence
                      </li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
            <div className="content-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header">
                      <h4
                        className="card-title"
                        id="horz-layout-colored-controls"
                      >
                        Create IT Sentence
                      </h4>
                      <a className="heading-elements-toggle">
                        <i className="fa fa-ellipsis-v font-medium-3" />
                      </a>
                      <div className="heading-elements">
                        <ul className="list-inline mb-0">
                          <li>
                            <a data-action="collapse">
                              <i className="feather icon-minus" />
                            </a>
                          </li>
                          <li>
                            <a data-action="reload" onClick={this.reload}>
                              <i className="feather icon-rotate-cw" />
                            </a>
                          </li>
                          <li>
                            <a data-action="expand">
                              <i className="feather icon-maximize" />
                            </a>
                          </li>
                          <li>
                            <a data-action="close">
                              <i className="feather icon-x" />
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="card-content collpase show">
                      <div className="card-body">
                        <form
                          className="form form-horizontal"
                          onSubmit={this.update}
                        >
                          <div className="form-body">
                            <div className="row">
                              <div className="col-md-12">
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Sentence
                                    <span style={{ color: "red" }}>
                                      &nbsp; *
                                    </span>
                                  </label>
                                  <div className="col-md-8 ">
                                    <textarea
                                      id="sentenceId"
                                      rows="4"
                                      className={classnames(
                                        "form-control border-primary",
                                        {
                                          "is-invalid": errors.sentence,
                                        }
                                      )}
                                      name="sentence"
                                      placeholder="Sentence"
                                      value={sentence.value}
                                      onChange={(e) => this.onChangeInput(e)}
                                      maxLength="255"
                                    ></textarea>

                                    {errors.sentence && (
                                      <div className="help-block">
                                        <ul role="alert">
                                          <li>{errors.sentence}</li>
                                        </ul>
                                      </div>
                                    )}
                                  </div>
                                </div>

                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Translation
                                    <span style={{ color: "red" }}>
                                      &nbsp; *
                                    </span>
                                  </label>

                                  <div className="col-md-8">
                                    <textarea
                                      id="translationId"
                                      rows="4"
                                      className={classnames(
                                        "form-control border-primary",
                                        {
                                          "is-invalid": errors.translation,
                                        }
                                      )}
                                      name="translation"
                                      placeholder="Translation"
                                      value={translation.value}
                                      onChange={(e) => this.onChangeInput(e)}
                                      maxLength="255"
                                    ></textarea>
                                    {errors.translation && (
                                      <div className="help-block">
                                        <ul role="alert">
                                          <li>{errors.translation}</li>
                                        </ul>
                                      </div>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="form-actions center">
                            <button
                              type="submit"
                              className="btn btn-primary mr-1"
                            >
                              <i className="fa fa-check-square-o" /> Save
                            </button>
                            <Link
                              type="button"
                              className="btn btn-warning mr-1"
                              to="/it"
                            >
                              <i className="feather icon-x" /> Cancel
                            </Link>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  errors: state.errors,
  sentence: state.itSentences.itSentenceDetail,
});

export default connect(mapStateToProps, {
  update,
  getDetail,
})(ITSentenceUpdate);

import React, { Component } from "react";
import "../../../style.css";
import Header from "../../layout/Header";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { Link } from "react-router-dom";
import CheckboxTree from "react-checkbox-tree";
import "react-checkbox-tree/lib/react-checkbox-tree.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  getOperation,
  getOperationByCondition,
} from "../../../actions/OperationAction";
import {
  createPermission,
  getPermissionDetail,
} from "../../../actions/PermissionAction";
import { trackPromise } from "react-promise-tracker";
import { ToastContainer } from "react-toastify";
import classnames from "classnames";
import { validateForm } from "../../validate/ValidateForm";

class PermissionCreate extends Component {
  state = {
    name: { value: "", isValid: true, message: "" },
    description: { value: "", isValid: true, message: "" },
    checkedOperation: [],
    expandedOperation: [],
    operationDB: [],
    permissionDetail: {},
    operations: [],
    isInvalidForm: true,
  };

  componentDidMount() {
    this.refresh();
    //  If Create Permission => Get Operation
    trackPromise(this.props.getOperation(this.state));
  }

  componentWillReceiveProps(nextProps) {
    const permissionId = this.props.match.params.id;
    //  Get All Operation in DB
    const operationDB = nextProps.operationDB;
    const nodes = [];
    const existNode = [];
    operationDB.map((operation) => {
      for (var i = 0; i < nodes.length; i++) {
        existNode.push(nodes[i].label);
      }
      const parent = operation.operationKey.split("_");

      if (existNode.indexOf(parent[0]) == -1) {
        const parentNode = {
          label: "",
          value: "",
          children: [],
        };
        parentNode.label = parent[0];
        parentNode.value = parent[0];
        const childNode = {};
        childNode.label = operation.name;
        childNode.value = operation.operationKey;
        childNode.object = operation;
        parentNode.children.push(childNode);
        nodes.push(parentNode);
      } else {
        for (var i = 0; i < nodes.length; i++) {
          if (nodes[i].value == parent[0]) {
            const childNode = {};
            childNode.label = operation.name;
            childNode.value = operation.operationKey;
            childNode.object = operation;
            nodes[i].children.push(childNode);
          }
        }
      }
    });
    const expandedOperation = [];
    nodes.map((operationParent) => {
      expandedOperation.push(operationParent.value);
    });

    this.setState({
      operationDB: nodes,
      expandedOperation: expandedOperation,
    });

    //  Check PermissionDetail Not Null & Permission Update - NOT create
    if (Object.keys(nextProps.permissionDetail).length !== 0 && permissionId) {
      const operations = nextProps.permissionDetail.operations;
      const checkedOperation = [];
      operations.map((operation) => {
        checkedOperation.push(operation.operationKey);
      });
      //  Set Permission Name , Description & Operation Checked
      this.setState({
        name: { value: nextProps.permissionDetail.name },
        description: { value: nextProps.permissionDetail.description },
        checkedOperation: checkedOperation,
        operations: operations,
      });
    }
  }

  checkOperations = (checkedOperation) => {
    this.setState({
      checkedOperation: checkedOperation,
    });

    //  Operation to create or update
    const operations = [];
    this.state.operationDB.map((operation) => {
      operation.children.map((childOperation) => {
        checkedOperation.map((checked) => {
          if (checked == childOperation.value) {
            operations.push(childOperation.object);
          }
        });
      });
    });

    this.setState({
      operations: operations,
    });
  };

  refresh = () => {
    this.setState({
      name: { value: "", isValid: true, message: "" },
      description: { value: "", isValid: true, message: "" },
      checkedOperation: [],
      expandedOperation: [],
      operationDB: [],
      permissionDetail: {},
      operations: [],
      isInvalidForm: true,
    });
  };

  onChangeInput = (e) => {
    const fieldName = e.target.name;
    this.setState(
      {
        [e.target.name]: {
          ...this.state[e.target.name],
          value: e.target.value,
        },
      },
      () => {
        const field = this.state[fieldName];
        const fieldValidate = validateForm(fieldName, field);
        if (fieldValidate.isInvalid) {
          this.setState({
            [fieldName]: {
              ...this.state[fieldName],
              isInvalid: fieldValidate.isInvalid,
              message: fieldValidate.message,
            },
            isInvalidForm: true,
          });
        } else {
          this.setState({
            [fieldName]: {
              ...this.state[fieldName],
              isInvalid: false,
              message: "",
            },
            isInvalidForm: false,
          });
        }
      }
    );
  };

  create = (e) => {
    e.preventDefault();
    const { name, description } = this.state;
    const newPermission = {
      name: name.value,
      description: description.value,
      operations: this.state.operations,
    };
    this.props.createPermission(newPermission, this.props.history);
  };

  render() {
    console.log(this.state);
    const { name, description } = this.state;
    const nameGroup = classnames("form-control border-primary", {
      "is-invalid": name.isInvalid,
    });

    return (
      <React.Fragment>
        <Header />
        <MenuSideBar />
        {/* Panel search */}

        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />
          <div className="content-wrapper">
            <div className="content-header row">
              <ToastContainer
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />
              <div className="content-header-left col-md-6 col-12 mb-2">
                <h3 className="content-header-title mb-0">Permission</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item">
                        <Link to="/permission">Permission</Link>
                      </li>
                      <li className="breadcrumb-item active">
                        Create Permission
                      </li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
            <div className="content-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header">
                      <h4
                        className="card-title"
                        id="horz-layout-colored-controls"
                      >
                        Create Permission
                      </h4>
                      <a className="heading-elements-toggle">
                        <i className="fa fa-ellipsis-v font-medium-3" />
                      </a>
                      <div className="heading-elements">
                        <ul className="list-inline mb-0">
                          <li>
                            <a data-action="collapse">
                              <i className="feather icon-minus" />
                            </a>
                          </li>
                          <li>
                            <a data-action="reload" onClick={this.reload}>
                              <i className="feather icon-rotate-cw" />
                            </a>
                          </li>
                          <li>
                            <a data-action="expand">
                              <i className="feather icon-maximize" />
                            </a>
                          </li>
                          <li>
                            <a data-action="close">
                              <i className="feather icon-x" />
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="card-content collpase show">
                      <div className="card-body">
                        <form
                          className="form form-horizontal"
                          onSubmit={this.create}
                        >
                          <div className="form-body">
                            <div className="row">
                              <div className="col-md-6">
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Permission Name
                                    <span style={{ color: "red" }}>
                                      &nbsp; *
                                    </span>
                                  </label>
                                  <div className="col-md-8">
                                    <input
                                      type="text"
                                      id="userinput1"
                                      className={nameGroup}
                                      placeholder="Permission Name"
                                      name="name"
                                      value={name.value}
                                      onChange={this.onChangeInput}
                                    />
                                    {this.state.isInvalidForm &&
                                      name.isInvalid && (
                                        <div className="help-block">
                                          <ul role="alert">
                                            <li>{name.message}</li>
                                          </ul>
                                        </div>
                                      )}
                                  </div>
                                </div>
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Permission Description
                                  </label>
                                  <div className="col-md-8">
                                    <input
                                      type="text"
                                      id="userinput3"
                                      className="form-control border-primary"
                                      placeholder="Permission Description"
                                      name="description"
                                      value={description.value}
                                      onChange={this.onChangeInput}
                                    />
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-6">
                                <CheckboxTree
                                  nodes={this.state.operationDB}
                                  checked={this.state.checkedOperation}
                                  expanded={this.state.expandedOperation}
                                  onCheck={(checkedOperation) =>
                                    this.checkOperations(checkedOperation)
                                  }
                                  onExpand={(expandedOperation) =>
                                    this.setState({ expandedOperation })
                                  }
                                />
                              </div>
                            </div>
                          </div>
                          <div className="form-actions center">
                            <button
                              type="submit"
                              className="btn btn-primary mr-1"
                              disabled={this.state.isInvalidForm}
                            >
                              <i className="fa fa-check-square-o" /> Save
                            </button>
                            <Link
                              type="button"
                              className="btn btn-warning mr-1"
                              to="/permission"
                            >
                              <i className="feather icon-x" /> Cancel
                            </Link>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  //  List Operation
  operationDB: state.operations.operation,
  permissionDetail: state.permissions.permissionDetail,
});

PermissionCreate.propTypes = {
  getOperation: PropTypes.func.isRequired,
  operationDB: PropTypes.array.isRequired,
  createPermission: PropTypes.func.isRequired,
  getPermissionDetail: PropTypes.func.isRequired,
  permissionDetail: PropTypes.object.isRequired,
};
export default connect(mapStateToProps, {
  getOperation,
  getOperationByCondition,
  createPermission,
  getPermissionDetail,
})(PermissionCreate);

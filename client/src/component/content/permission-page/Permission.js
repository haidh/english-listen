import React, { Component } from "react";
import "../../../style.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  getPermission,
  deletePermission,
  deleteListPermission,
} from "../../../actions/PermissionAction";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { TextField, Checkbox } from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";
import { Link } from "react-router-dom";
import { trackPromise } from "react-promise-tracker";
import Header from "../../layout/Header";
import { Modal } from "react-bootstrap";

import { ToastContainer, toast } from "react-toastify";
class Permission extends Component {
  state = {
    name: "",
    description: "",
    page: 1,
    size: 10,
    sortBy: "id",
    sort: "ASC",
    sorted: false,
    permission: [],
    totalElements: 0,
    totalPages: 0,
    permissionIdDelete: "",
    showModal: false,
    showModalMany: false,
    checkAll: false,
    listPermissionIdDelete: [],
  };

  componentDidMount() {
    trackPromise(this.props.getPermission(this.state));
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      const permission = nextProps.permission;
      if (permission.length > 0) {
        permission.map((per) => {
          per.isChecked = false;
        });
      }
      this.setState({
        page: nextProps.page + 1,
        permission: permission,
        totalElements: nextProps.totalElements,
        totalPages: nextProps.totalPages,
      });
    }
  }

  searchPermission = () => {
    //  Load spinner when call action search
    trackPromise(this.props.getPermission(this.state));
  };

  onChangeInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onChangeSizePage = (e) => {
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        trackPromise(this.props.getPermission(this.state));
      }
    );
  };

  onChangePage = (e, page) => {
    this.setState(
      {
        page: page,
      },
      () => {
        trackPromise(this.props.getPermission(this.state));
      }
    );
  };

  onChangeCheckboxAll = () => {
    this.state.checkAll = !this.state.checkAll;

    const listPermissionIdDelete = [];
    //  SET STATTUS CHECKED TO CHILDREN
    this.state.permission.map((value) => {
      value.isChecked = this.state.checkAll;
      if (this.state.checkAll) {
        listPermissionIdDelete.push(value.id);
      }
    });

    //  SET STATUS CHECK ALL
    this.setState({
      checkAll: this.state.checkAll,
      listPermissionIdDelete: listPermissionIdDelete,
    });
  };

  onChangeCheckbox = (value) => {
    value.isChecked = !value.isChecked;
    let permission = this.state.permission;
    let countChecked = 0;
    const listPermissionIdDelete = [];
    permission.map((per) => {
      if (per.name === value.name) {
        per.isChecked = value.isChecked;
      }

      if (per.isChecked === true) {
        countChecked++;
        listPermissionIdDelete.push(per.id);
      }
    });
    if (countChecked === permission.length) {
      this.setState({
        checkAll: true,
      });
    } else {
      this.setState({
        checkAll: false,
      });
    }
    this.setState({
      permission: permission,
      listPermissionIdDelete: listPermissionIdDelete,
    });
  };

  reloadPage = () => {
    this.setState(
      {
        name: "",
        description: "",
        page: 1,
        size: 10,
        sortBy: "id",
        sort: "ASC",
        sorted: false,
        permission: [],
        totalElements: 0,
        totalPages: 0,
        permissionIdDelete: "",
        showModal: false,
        showModalMany: false,
        checkAll: false,
        listPermissionIdDelete: [],
      },
      () => {
        trackPromise(this.props.getPermission(this.state));
      }
    );
  };

  deleteModal = (permissionId) => {
    this.setState({
      showModal: true,
      permissionIdDelete: permissionId,
    });
  };

  handleCloseModal = () => this.setState({ showModal: false });

  deletePermission = () => {
    const id = this.state.permissionIdDelete;
    this.props.deletePermission(id).then(() => {
      this.handleCloseModal();
      trackPromise(this.props.getPermission(this.state));
    });
  };

  deleteManyModal = () => {
    console.log(this.state);
    if (this.state.listPermissionIdDelete.length > 0) {
      this.setState({
        showModalMany: true,
      });
    } else {
      toast.warning("Please Select One Permission");
    }
  };

  handleCloseModalMany = () => this.setState({ showModalMany: false });

  deleteManyPermission = () => {
    const listPermissionIdDelete = this.state.listPermissionIdDelete;
    console.log(listPermissionIdDelete);
    this.props.deleteListPermission(listPermissionIdDelete).then(() => {
      this.handleCloseModalMany();
      trackPromise(this.props.getPermission(this.state));
    });
  };

  sortBy = (fieldName) => {
    let sorted = this.state.sorted;
    sorted = !sorted;
    if (this.state.sorted) {
      this.setState({
        sort: "ASC",
      });
    } else {
      this.setState({
        sort: "DESC",
      });
    }
    this.setState(
      {
        sortBy: fieldName,
        sorted: sorted,
      },
      () => {
        trackPromise(this.props.getPermission(this.state));
      }
    );
  };

  render() {
    return (
      <React.Fragment>
        <ToastContainer
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <Modal
          name="delete"
          show={this.state.showModal}
          animation={false}
          onHide={this.handleCloseModal}
        >
          <Modal.Header closeButton className="bg-warning white">
            <Modal.Title>MODAL DELETE</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete this permission?</Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={this.deletePermission}
            >
              OK
            </button>
            <button
              type="button"
              className="btn grey btn-outline-secondary "
              onClick={this.handleCloseModal}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>

        <Modal
          name="deleteAll"
          show={this.state.showModalMany}
          animation={false}
          onHide={this.handleCloseModalMany}
        >
          <Modal.Header closeButton className="bg-warning white">
            <Modal.Title>MODAL DELETE</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete these permissions?</Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={this.deleteManyPermission}
            >
              OK
            </button>
            <button
              type="button"
              className="btn grey btn-outline-secondary "
              onClick={this.handleCloseModalMany}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>

        <Header />
        <MenuSideBar />
        {/* Panel search */}

        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />

          <div className="content-wrapper">
            <div className="content-header row">
              <div className="content-header-left col-md-6 col-12 mb-2">
                <h3 className="content-header-title mb-0">Permission</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item active">Permission</li>
                    </ol>
                  </div>
                </div>
              </div>
              <div className="content-header-right text-md-right col-md-6 col-12">
                <div className="form-group">
                  <Link
                    className="btn-icon btn fonticon-container"
                    type="button"
                    to="/permission/create"
                  >
                    <i className="fa fa-plus"></i>
                    <label className="fonticon-classname">Add Permission</label>
                  </Link>
                  <button
                    className="btn-icon btn fonticon-container"
                    type="button"
                    onClick={() => this.deleteManyModal()}
                  >
                    <i className="fa fa-times"></i>
                    <label className="fonticon-classname">Delete</label>
                  </button>
                  <button
                    className="btn-icon btn fonticon-container"
                    type="button"
                    onClick={this.reloadPage}
                  >
                    <i className="fa fa-refresh"></i>
                    <label className="fonticon-classname">Refresh</label>
                  </button>
                </div>
              </div>
            </div>
            <div className="content-body">
              {/* Basic Elements start */}
              <section className="basic-elements">
                <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      <div className="card-content">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <TextField
                                id="nameId"
                                label="Permission Name"
                                className="form-control"
                                color="secondary"
                                value={this.state.name}
                                name="name"
                                onChange={(e) => this.onChangeInput(e)}
                              />
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <TextField
                                id="descriptionId"
                                label="Description"
                                className="form-control"
                                color="secondary"
                                name="description"
                                value={this.state.description}
                                onChange={(e) => this.onChangeInput(e)}
                              />
                            </div>
                            <div className="col-xl-1 col-lg-1 col-md-1 mb-1">
                              <a
                                href="#"
                                className="btn btn-social-icon btn-outline-twitter btn-outline-cyan"
                                onClick={this.searchPermission}
                              >
                                <span className="fa fa-search"></span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              {/* Basic Inputs end */}

              <section id="decimal">
                <div className="row">
                  <div className="col-12">
                    <div className="card">
                      <div className="card-header">
                        <a href="/#" className="heading-elements-toggle">
                          <i className="fa fa-ellipsis-v font-medium-3" />
                        </a>
                        <div className="heading-elements">
                          <ul className="list-inline mb-0">
                            <li>
                              <a data-action="collapse">
                                <i className="feather icon-minus" />
                              </a>
                            </li>
                            <li>
                              <a data-action="reload" onClick={this.reloadPage}>
                                <i className="feather icon-rotate-cw" />
                              </a>
                            </li>
                            <li>
                              <a data-action="expand">
                                <i className="feather icon-maximize" />
                              </a>
                            </li>
                            <li>
                              <a data-action="close">
                                <i className="feather icon-x" />
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="card-content collapse show">
                        <div className="card-body card-dashboard">
                          <table className="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th style={{ width: "5%" }}>
                                  <Checkbox
                                    checked={this.state.checkAll}
                                    onChange={this.onChangeCheckboxAll}
                                    name="check"
                                    color="primary"
                                    value="checkAll"
                                  />
                                </th>
                                <th style={{ width: "5%" }}>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("id")}
                                  >
                                    ID
                                  </a>
                                </th>
                                <th style={{ width: "35%" }}>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("name")}
                                  >
                                    Permission Name
                                  </a>
                                </th>
                                <th style={{ width: "35%" }}>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("description")}
                                  >
                                    Description
                                  </a>
                                </th>
                                <th style={{ width: "15%" }}>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.permission.map((value, index) => (
                                <tr key={index}>
                                  <td>
                                    <Checkbox
                                      checked={value.isChecked}
                                      color="primary"
                                      onChange={() =>
                                        this.onChangeCheckbox(value)
                                      }
                                    />
                                  </td>
                                  <td className="styleTD">
                                    {index +
                                      (this.state.page - 1) * this.state.size +
                                      1}
                                  </td>
                                  <td className="styleTD">{value.name}</td>
                                  <td className="styleTD">
                                    {value.description}
                                  </td>
                                  <td>
                                    <Link
                                      className="btn-icon btn warning editButton"
                                      type="button"
                                      to={`/permission/view/${value.id}`}
                                    >
                                      <i className="fa fa-eye"></i>
                                    </Link>
                                    <Link
                                      className="btn-icon btn primary editButton"
                                      type="button"
                                      to={`/permission/update/${value.id}`}
                                    >
                                      <i className="fa fa-pencil-square"></i>
                                    </Link>
                                    <button
                                      className="btn-icon btn danger deleteButton"
                                      type="button"
                                      onClick={() => this.deleteModal(value.id)}
                                    >
                                      <i className="fa fa-trash"></i>
                                    </button>
                                  </td>
                                </tr>
                              ))}
                            </tbody>
                          </table>

                          <div className="row">
                            <div className="offset-5 col-2 text-right">
                              <label className="control-label labelPagination">
                                Show
                              </label>
                            </div>
                            <div className="col-1">
                              <select
                                name="size"
                                className="custom-select custom-select-sm form-control form-control-sm"
                                value={this.state.size}
                                onChange={this.onChangeSizePage}
                              >
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                              </select>
                            </div>
                            <div className="col-4">
                              <Pagination
                                name="page"
                                count={this.state.totalPages}
                                color="primary"
                                showFirstButton
                                showLastButton
                                onChange={this.onChangePage}
                                page={this.state.page}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
        {/* Modal */}

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  permission: state.permissions.permission,
  totalPages: state.permissions.totalPages,
  totalElements: state.permissions.totalElements,
  page: state.permissions.page,
  deleteSuccess: state.permissions.deleteSuccess,
  
});

Permission.propTypes = {
  permission: PropTypes.array.isRequired,
  totalPages: PropTypes.number.isRequired,
  totalElements: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  deleteSuccess: PropTypes.string.isRequired,
  getPermission: PropTypes.func.isRequired,
  deletePermission: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  getPermission,
  deletePermission,
  deleteListPermission,
})(Permission);

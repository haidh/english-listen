import React, { Component } from "react";
import "../../../style.css";
import Header from "../../layout/Header";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { Link } from "react-router-dom";
import CheckboxTree from "react-checkbox-tree";
import "react-checkbox-tree/lib/react-checkbox-tree.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getAllPermission } from "../../../actions/PermissionAction";
import { updateRole, getRoleDetail } from "../../../actions/RoleAction";
import { trackPromise } from "react-promise-tracker";
import { ToastContainer } from "react-toastify";
import classnames from "classnames";
import { validateForm } from "../../validate/ValidateForm";

class RoleUpdate extends Component {
  state = {
    name: { value: "", isValid: true, message: "" },
    description: { value: "", isValid: true, message: "" },
    roleDetail: {},
    permissionTree: [],
    permissionsCheckbox: [],
    checkedPermission: [],
    expandedPermission: [],
    permissionsDetail: {},
    permissions: [],
    isInvalidForm: false,
  };

  componentDidMount = () => {
    const id = this.props.match.params.id;
    //  Get Role Detail & Get All Permission
    trackPromise(
      this.props.getRoleDetail(id).then(() => {
        trackPromise(this.props.getAllPermission(this.state));
      })
    );
  };

  componentWillReceiveProps = (nextProps) => {
    const id = this.props.match.params.id;
    //  Custom Permission => Permission Checkbox
    this.getPermissionCheckbox(nextProps);
    //  Check PermissionDetail Not Null & Permission Update - NOT create
    if (Object.keys(nextProps.roleDetail).length !== 0 && id) {
      const permissions = nextProps.roleDetail.permissions;
      const checkedPermission = [];
      permissions.map((permission) => {
        checkedPermission.push(permission.name);
      });

      //  Set Permission Name , Description & Operation Checked
      this.setState({
        name: { value: nextProps.roleDetail.name },
        description: { value: nextProps.roleDetail.description },
        checkedPermission: checkedPermission,
        permissions: permissions,
        permissionTree: nextProps.permissionTree,
      });
    }
  };

  getPermissionCheckbox = (nextProps) => {
    const permissionTree = nextProps.permissionTree;
    const nodes = [];
    permissionTree.map((permission) => {
      const parentNode = {
        label: "",
        value: "",
      };
      parentNode.label = permission.name;
      parentNode.value = permission.name;
      nodes.push(parentNode);
    });
    this.setState({
      permissionsCheckbox: nodes,
      permissionTree: nextProps.permissionTree,
    });
  };

  checkedPermissions = (checkedPermission) => {
    this.setState({
      checkedPermission: checkedPermission,
    });

    //  Permissions to create or update
    const permissions = [];
    this.state.permissionTree.map((permission) => {
      checkedPermission.map((checked) => {
        if (permission.name == checked) {
          permissions.push(permission);
        }
      });
    });

    this.setState({
      permissions: permissions,
    });
  };

  reload = () => {
    this.setState({
      name: { value: "", isValid: true, message: "" },
      description: { value: "", isValid: true, message: "" },
      roleDetail: {},
      permissionTree: [],
      permissionsCheckbox: [],
      checkedPermission: [],
      expandedPermission: [],
      permissionsDetail: {},
      permissions: [],
      isInvalidForm: true,
    });
  };

  onChangeInput = (e) => {
    const fieldName = e.target.name;
    this.setState(
      {
        [e.target.name]: {
          ...this.state[e.target.name],
          value: e.target.value,
        },
      },
      () => {
        const field = this.state[fieldName];
        const fieldValidate = validateForm(fieldName, field);
        if (fieldValidate.isInvalid) {
          this.setState({
            [fieldName]: {
              ...this.state[fieldName],
              isInvalid: fieldValidate.isInvalid,
              message: fieldValidate.message,
            },
            isInvalidForm: true,
          });
        } else {
          this.setState({
            [fieldName]: {
              ...this.state[fieldName],
              isInvalid: false,
              message: "",
            },
            isInvalidForm: false,
          });
        }
      }
    );
  };

  update = (e) => {
    e.preventDefault();
    const { name, description } = this.state;
    const id = this.props.match.params.id;

    const updateRole = {
      id: id,
      name: name.value,
      description: description.value,
      permissions: this.state.permissions,
    };
    this.props.updateRole(updateRole, this.props.history);
  };

  render() {
    const { name, description } = this.state;
    const nameGroup = classnames("form-control border-primary", {
      "is-invalid": name.isInvalid,
    });

    return (
      <React.Fragment>
        <Header />
        <MenuSideBar />
        {/* Panel search */}

        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />
          <div className="content-wrapper">
            <div className="content-header row">
              <ToastContainer
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />
              <div className="content-header-left col-md-6 col-12 mb-2">
                <h3 className="content-header-title mb-0">Role</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item">
                        <Link to="/permission">Role</Link>
                      </li>
                      <li className="breadcrumb-item active">Update Role</li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
            <div className="content-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header">
                      <h4
                        className="card-title"
                        id="horz-layout-colored-controls"
                      >
                        Update Role
                      </h4>
                      <a className="heading-elements-toggle">
                        <i className="fa fa-ellipsis-v font-medium-3" />
                      </a>
                      <div className="heading-elements">
                        <ul className="list-inline mb-0">
                          <li>
                            <a data-action="collapse">
                              <i className="feather icon-minus" />
                            </a>
                          </li>
                          <li>
                            <a data-action="reload">
                              <i className="feather icon-rotate-cw" />
                            </a>
                          </li>
                          <li>
                            <a data-action="expand">
                              <i className="feather icon-maximize" />
                            </a>
                          </li>
                          <li>
                            <a data-action="close">
                              <i className="feather icon-x" />
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="card-content collpase show">
                      <div className="card-body">
                        <form
                          className="form form-horizontal"
                          onSubmit={this.update}
                        >
                          <div className="form-body">
                            <div className="row">
                              <div className="col-md-6">
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Role Name
                                    <span style={{ color: "red" }}>
                                      &nbsp; *
                                    </span>
                                  </label>
                                  <div className="col-md-8 ">
                                    <input
                                      type="text"
                                      id="name"
                                      className={nameGroup}
                                      placeholder="Role Name"
                                      name="name"
                                      value={name.value}
                                      onChange={(e) => this.onChangeInput(e)}
                                      maxLength="255"
                                    />
                                    {this.state.isInvalidForm &&
                                      name.isInvalid && (
                                        <div className="help-block">
                                          <ul role="alert">
                                            <li>{name.message}</li>
                                          </ul>
                                        </div>
                                      )}
                                  </div>
                                </div>
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Role Description
                                  </label>
                                  <div className="col-md-8">
                                    <input
                                      type="text"
                                      id="description"
                                      className="form-control border-primary"
                                      placeholder="Role Description"
                                      name="description"
                                      value={description.value}
                                      onChange={this.onChangeInput}
                                      maxLength="255"
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="col-md-6">
                                <CheckboxTree
                                  nodes={this.state.permissionsCheckbox}
                                  checked={this.state.checkedPermission}
                                  expanded={this.state.permissionExpanded}
                                  noCascade
                                  onCheck={(permissionChecked) =>
                                    this.checkedPermissions(permissionChecked)
                                  }
                                  onExpand={(permissionExpanded) =>
                                    this.setState({ permissionExpanded })
                                  }
                                />
                              </div>
                            </div>
                          </div>
                          <div className="form-actions center">
                            <button
                              type="submit"
                              className="btn btn-primary mr-1"
                              disabled={this.state.isInvalidForm}
                            >
                              <i className="fa fa-check-square-o" /> Save
                            </button>
                            <Link
                              type="button"
                              className="btn btn-warning mr-1"
                              to="/role"
                            >
                              <i className="feather icon-x" /> Cancel
                            </Link>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  //  List Operation
  permissionTree: state.permissions.allPermission,
  roleDetail: state.roles.roleDetail,
});

RoleUpdate.propTypes = {
  permissionTree: PropTypes.array.isRequired,
  roleDetail: PropTypes.object.isRequired,
  updateRole: PropTypes.func.isRequired,
  getRoleDetail: PropTypes.func.isRequired,
};
export default connect(mapStateToProps, {
  getAllPermission,
  updateRole,
  getRoleDetail,
})(RoleUpdate);

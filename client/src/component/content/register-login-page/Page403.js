import React, { Component } from "react";
import Header from "../../layout/Header";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";

export default class Page403 extends Component {
  render() {
    return (
      <React.Fragment>
      <Header/>
      <MenuSideBar/>
      <div className="app-content content">
        <div className="content-overlay" />
        <div className="content-wrapper">
          <div className="content-header row"></div>
          <div className="content-body">
            <section className="flexbox-container">
              <div className="col-12 d-flex align-items-center justify-content-center">
                <div className="col-lg-4 col-md-8 col-10 p-0">
                  <div className="card-header bg-transparent border-0">
                    <h1 className="error-code text-center mb-2">403</h1>
                    <h1 className="text-uppercase text-center">
                      Access Denied/Forbidden !
                    </h1>
                  </div>
                 
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <Footer />
      </React.Fragment>
    );
  }
}

import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { toast, ToastContainer } from "react-toastify";
import { Link } from "react-router-dom";
import { registerUser } from "../../../actions/UserAction";

class Resgiter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      fullName: "",
      password: "",
      confirmPassword: "",
      email: "",
      errors: {},
    };
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
      });
    } else {
      toast.success("Register Success", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }

  onSubmit = (e) => {
    e.preventDefault();
    const newUser = {
      username: this.state.username,
      fullName: this.state.fullName,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
      email: this.state.email,
    };

    this.props.registerUser(newUser, this.props.history);
  };

  render() {
    const { errors } = this.state;
    return (
      <div>
        <ToastContainer />
        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />
          <div className="content-wrapper">
            <div className="content-header row"></div>
            <div className="content-body">
              <section className="row flexbox-container">
                <div className="col-12 d-flex align-items-center justify-content-center">
                  <div className="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                    <div className="card border-grey border-lighten-3 px-1 py-1 m-0">
                      <div className="card-header border-0 pb-0">
                        <div className="card-title text-center">
                          <img
                            src="app-assets/images/logo/stack-logo-dark.png"
                            alt="branding logo"
                          />
                        </div>
                        <h6 className="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                          <span>Easily Using</span>
                        </h6>
                      </div>
                      <div className="card-content">
                        <div className="text-center">
                          <Link
                            to="/"
                            className="btn btn-social-icon mr-1 mb-1 btn-outline-facebook"
                          >
                            <span className="fa fa-facebook" />
                          </Link>
                          <Link
                            to="/"
                            className="btn btn-social-icon mr-1 mb-1 btn-outline-twitter"
                          >
                            <span className="fa fa-twitter" />
                          </Link>
                          <Link
                            to="/"
                            className="btn btn-social-icon mr-1 mb-1 btn-outline-linkedin"
                          >
                            <span className="fa fa-linkedin font-medium-4" />
                          </Link>
                          <Link
                            to="/"
                            className="btn btn-social-icon mr-1 mb-1 btn-outline-github"
                          >
                            <span className="fa fa-github font-medium-4" />
                          </Link>
                        </div>
                        <p className="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
                          <span>OR Using Email</span>
                        </p>
                        <div className="card-body">
                          <form
                            className="form-horizontal"
                            action="index.html"
                            noValidate
                            onSubmit={this.onSubmit}
                          >
                            <fieldset className="form-group position-relative has-icon-left">
                              <input
                                type="text"
                                className="form-control"
                                id="username"
                                name="username"
                                placeholder="User Name"
                                value={this.state.username}
                                onChange={this.onChange}
                              />
                              <div className="form-control-position">
                                <i className="feather icon-user" />
                              </div>
                            </fieldset>
                            {errors.username ? (
                              <p style={{ color: "red" }}>{errors.username}</p>
                            ) : null}
                            <fieldset className="form-group position-relative has-icon-left">
                              <input
                                type="text"
                                className="form-control"
                                id="fullName"
                                name="fullName"
                                placeholder="Full Name"
                                value={this.state.fullName}
                                onChange={this.onChange}
                              />
                              <div className="form-control-position">
                                <i className="feather icon-user" />
                              </div>
                            </fieldset>
                            {errors.fullName ? (
                              <p style={{ color: "red" }}>{errors.fullName}</p>
                            ) : null}
                            <fieldset className="form-group position-relative has-icon-left">
                              <input
                                type="email"
                                className="form-control"
                                id="email"
                                name="email"
                                placeholder="Your Email Address"
                                required
                                value={this.state.email}
                                onChange={this.onChange}
                              />
                              <div className="form-control-position">
                                <i className="feather icon-mail" />
                              </div>
                            </fieldset>
                            {errors.email ? (
                              <p style={{ color: "red" }}>{errors.email}</p>
                            ) : null}
                            <fieldset className="form-group position-relative has-icon-left">
                              <input
                                type="password"
                                className="form-control"
                                id="password"
                                name="password"
                                placeholder="Enter Password"
                                required
                                value={this.state.password}
                                onChange={this.onChange}
                              />
                              <div className="form-control-position">
                                <i className="fa fa-key" />
                              </div>
                            </fieldset>
                            {errors.password ? (
                              <p style={{ color: "red" }}>{errors.password}</p>
                            ) : null}
                            <fieldset className="form-group position-relative has-icon-left">
                              <input
                                type="password"
                                className="form-control"
                                id="confirmPassword"
                                name="confirmPassword"
                                placeholder="Enter Confirm Password"
                                value={this.state.confirmPassword}
                                onChange={this.onChange}
                                required
                              />
                              {errors.confirmPassword ? (
                                <p style={{ color: "red" }}>
                                  {errors.confirmPassword}
                                </p>
                              ) : null}
                              <div className="form-control-position">
                                <i className="fa fa-key" />
                              </div>
                            </fieldset>
                            <div className="form-group row">
                              <div className="col-sm-6 col-12 text-center text-sm-left pr-0">
                                <fieldset>
                                  <input
                                    type="checkbox"
                                    id="remember-me"
                                    className="chk-remember"
                                  />
                                  <label htmlFor="remember-me">
                                    &nbsp; Remember Me
                                  </label>
                                </fieldset>
                              </div>
                              <div className="col-sm-6 col-12 float-sm-left text-center text-sm-right">
                                <a
                                  href="recover-password.html"
                                  className="card-link"
                                >
                                  Forgot Password?
                                </a>
                              </div>
                            </div>
                            <button
                              type="submit"
                              className="btn btn-outline-primary btn-block"
                            >
                              <i className="feather icon-user" /> Register
                            </button>
                          </form>
                          <Link
                            to="/login"
                            className="btn btn-outline-danger btn-block mt-2"
                          >
                            <i className="feather icon-unlock" /> Login
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
        {/* END: Content*/}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  errors: state.errors,
});

Resgiter.propTypes = {
  user: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  registerUser: PropTypes.func.isRequired,
};
export default connect(mapStateToProps, { registerUser })(Resgiter);

import React, { Component } from "react";
import "../../../style.css";
import Header from "../../layout/Header";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getAllSeason } from "../../../actions/SeasonAction";
import { getAllEpisode } from "../../../actions/EpisodeAction";
import { create } from "../../../actions/MovieAction";
import { trackPromise } from "react-promise-tracker";
import { ToastContainer } from "react-toastify";
import classnames from "classnames";
import { validateForm } from "../../validate/ValidateForm";
import Select from "react-select";

class MovieCreate extends Component {
  state = {
    name: { value: "", isInvalid: false, message: "" },
    description: { value: "", isInvalid: false, message: "" },
    seasonNumber: { value: "", isInvalid: false, message: "" },
    seasons: [],
    seasonSelect: [],
    seasonSelected: [],
    episodes: [],
    episodeSelect: [],
    episodeSelected: [],
    seasonEpisode: [],
    isInvalidForm: true,
  };

  componentDidMount() {
    //  If Create Role => Get All Permission
    trackPromise(this.props.getAllSeason(this.state)).then(
      trackPromise(this.props.getAllEpisode(this.state))
    );
  }

  componentWillReceiveProps(nextProps) {
    const { seasons, episodes } = nextProps;

    const nodes = [];
    seasons.map((season) => {
      const parentNode = { ...season, label: season.name, value: season.name };
      nodes.push(parentNode);
    });

    const episodeNodes = [];
    episodes.map((episode) => {
      const parentNode = {
        ...episode,
        label: episode.name,
        value: episode.name,
      };
      episodeNodes.push(parentNode);
    });

    this.setState({
      seasonSelect: nodes,
      seasons: nextProps.seasons,
      episodeSelect: episodeNodes,
      episodes: nextProps.episodes,
    });
  }

  checkedPermissions = (checkedPermission) => {
    this.setState({
      checkedPermission: checkedPermission,
    });

    //  Permissions to create
    const permissions = [];

    this.state.permissionTree.map((permission) => {
      checkedPermission.map((checked) => {
        if (permission.name === checked) {
          permissions.push(permission);
        }
      });
    });

    this.setState({
      permissions: permissions,
    });
  };

  reload = () => {
    this.setState({
      name: { value: "", isInvalid: false, message: "" },
      description: { value: "", isInvalid: false, message: "" },
      seasonNumber: { value: "", isInvalid: false, message: "" },
      seasons: [],
      seasonSelectShow: [],
      seasonSelect: [],
      seasonSelected: [],
      episodes: [],
      episodeSelect: [],
      episodeSelected: [],
      seasonEpisode: [],
      isInvalidForm: true,
    });
  };

  onChangeInput = (e) => {
    const fieldName = e.target.name;
    this.setState(
      {
        [e.target.name]: {
          ...this.state[e.target.name],
          value: e.target.value,
        },
      },
      () => {
        const field = this.state[fieldName];
        const fieldValidate = validateForm(fieldName, field);
        if (fieldValidate.isInvalid) {
          this.setState(
            {
              [fieldName]: {
                ...fieldValidate,
              },
            },
            () => {
              if (
                this.state.name.isInvalid ||
                this.state.description.isInvalid
              ) {
                this.setState({
                  isInvalidForm: true,
                });
              } else {
                this.setState({
                  isInvalidForm: false,
                });
              }
            }
          );
        } else {
          this.setState(
            {
              [fieldName]: {
                ...fieldValidate,
              },
            },
            () => {
              if (
                this.state.name.isInvalid ||
                this.state.description.isInvalid
              ) {
                this.setState({
                  isInvalidForm: true,
                });
              } else {
                this.setState({
                  isInvalidForm: false,
                });
              }
            }
          );
        }
      }
    );
  };

  create = (e) => {
    e.preventDefault();
    const movie = {
      name: this.state.name.value,
      description: this.state.description.value,
      seasons: this.state.seasonEpisode,
    };
    this.props.create(movie, this.props.history);
  };

  onChangeSelectSeason = (value) => {
    if (value === null) {
      this.setState({
        seasonEpisode: [],
      });
    }
    this.setState(
      {
        seasonSelected: value,
      },
      () => {
        if (
          this.state.seasonSelected !== null &&
          this.state.seasonSelected.length > 0 &&
          this.state.episodeSelected !== null &&
          this.state.episodeSelected.length == 0
        ) {
          this.setState({ isInvalidForm: true });
        } else {
          this.setState({ isInvalidForm: false });
        }
      }
    );
  };
  onChangeSelectEpisode = (value, e) => {
    const seasonEpisode = [...this.state.seasonSelected];
    seasonEpisode.map((season) => {
      if (season.id === e.name) {
        season.episodes = value;
      }
    });
    this.setState(
      {
        seasonEpisode: seasonEpisode,
        episodeSelected: value,
      },
      () => {
        console.log(this.state.episodeSelected);
        if (
          (this.state.seasonSelected.length > 0 &&
            this.state.episodeSelected == null) ||
          (this.state.seasonSelected.length > 0 &&
            this.state.episodeSelected !== null &&
            this.state.episodeSelected.length == 0)
        ) {
          this.setState({ isInvalidForm: true });
        } else {
          this.setState({ isInvalidForm: false });
        }
      }
    );
  };

  render() {
    const {
      name,
      description,
      seasonSelect,
      episodeSelect,
      seasonSelected,
      episodeSelected,
    } = this.state;
    const nameGroup = classnames("form-control border-primary", {
      "is-invalid": name.isInvalid,
    });

    const episodeGroup = classnames("border-primary", {
      "is-invalid": episodeSelected !== null ? true : false,
    });

    return (
      <React.Fragment>
        <Header />
        <MenuSideBar />
        {/* Panel search */}

        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />
          <div className="content-wrapper">
            <div className="content-header row">
              <ToastContainer
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />
              <div className="content-header-left col-md-6 col-12 mb-2">
                <h3 className="content-header-title mb-0">Movie</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item">
                        <Link to="/movie">Movie</Link>
                      </li>
                      <li className="breadcrumb-item active">Create Movie</li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
            <div className="content-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header">
                      <h4
                        className="card-title"
                        id="horz-layout-colored-controls"
                      >
                        Create Movie
                      </h4>
                      <a className="heading-elements-toggle">
                        <i className="fa fa-ellipsis-v font-medium-3" />
                      </a>
                      <div className="heading-elements">
                        <ul className="list-inline mb-0">
                          <li>
                            <a data-action="collapse">
                              <i className="feather icon-minus" />
                            </a>
                          </li>
                          <li>
                            <a data-action="reload">
                              <i className="feather icon-rotate-cw" />
                            </a>
                          </li>
                          <li>
                            <a data-action="expand">
                              <i className="feather icon-maximize" />
                            </a>
                          </li>
                          <li>
                            <a data-action="close">
                              <i className="feather icon-x" />
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="card-content collpase show">
                      <div className="card-body">
                        <form
                          className="form form-horizontal"
                          onSubmit={this.create}
                        >
                          <div className="form-body">
                            <div className="row">
                              <div className="col-md-6">
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Movie Name
                                    <span style={{ color: "red" }}>
                                      &nbsp; *
                                    </span>
                                  </label>
                                  <div className="col-md-8 ">
                                    <input
                                      type="text"
                                      id="name"
                                      className={nameGroup}
                                      placeholder="Movie Name"
                                      name="name"
                                      value={name.value}
                                      onChange={(e) => this.onChangeInput(e)}
                                      maxLength="255"
                                    />

                                    {name.isInvalid && (
                                      <div className="help-block">
                                        <ul role="alert">
                                          <li>{name.message}</li>
                                        </ul>
                                      </div>
                                    )}
                                  </div>
                                </div>

                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Movie Season
                                  </label>
                                  <div className="col-md-8">
                                    <Select
                                      options={seasonSelect}
                                      isMulti
                                      className="border-primary"
                                      onChange={this.onChangeSelectSeason}
                                      value={this.state.seasonSelectShow}
                                    />
                                  </div>
                                </div>
                              </div>

                              <div className="col-md-6">
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Movie Description
                                  </label>
                                  <div className="col-md-8">
                                    <input
                                      type="text"
                                      id="description"
                                      className="form-control border-primary"
                                      placeholder="Movie Description"
                                      name="description"
                                      value={description.value}
                                      onChange={this.onChangeInput}
                                      maxLength="255"
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          {seasonSelected !== null &&
                            seasonSelected.map((season) => {
                              return (
                                <div className="row" key={season.id}>
                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-md-4 control-label"></label>
                                      <div className="col-md-8 ">
                                        <input
                                          type="text"
                                          className="form-control border-primary"
                                          placeholder={season.value}
                                          name={season.value}
                                          value={season.value}
                                          onChange={(e) =>
                                            this.onChangeSeasonInput(e)
                                          }
                                          disabled
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-md-4 control-label"></label>
                                      <div className="col-md-8 ">
                                        <Select
                                          name={season.id}
                                          options={episodeSelect}
                                          isMulti
                                          className={episodeGroup}
                                          onChange={(value, e) =>
                                            this.onChangeSelectEpisode(value, e)
                                          }
                                        />

                                        {seasonSelected !== null &&
                                          seasonSelected.length > 0 &&
                                          (episodeSelected == null ||
                                            (episodeSelected != null &&
                                              episodeSelected.length == 0)) && (
                                            <div className="help-block">
                                              <ul role="alert">
                                                <li>Episode is Required</li>
                                              </ul>
                                            </div>
                                          )}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              );
                            })}

                          <div className="form-actions center">
                            <button
                              type="submit"
                              className="btn btn-primary mr-1"
                              disabled={this.state.isInvalidForm}
                            >
                              <i className="fa fa-check-square-o" /> Save
                            </button>
                            <Link
                              type="button"
                              className="btn btn-warning mr-1"
                              to="/movie"
                            >
                              <i className="feather icon-x" /> Cancel
                            </Link>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  //  List Operation
  seasons: state.seasons.seasons,
  episodes: state.episodes.episodes,
});

export default connect(mapStateToProps, {
  getAllSeason,
  getAllEpisode,
  create,
})(MovieCreate);

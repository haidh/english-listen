import React, { Component } from "react";
import "../../../style.css";
import Header from "../../layout/Header";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { create } from "../../../actions/PhraseAction";
import { ToastContainer } from "react-toastify";
import classnames from "classnames";
import { validateForm } from "../../validate/ValidateForm";

class PhraseCreate extends Component {
  state = {
    name: { value: "", isValid: true, message: "" },
    value: { value: "", isValid: true, message: "" },
    description: { value: "", isValid: true, message: "" },
    translations: [],
    count: 0,
    isInvalidForm: true,
  };

  reload = () => {
    this.setState({
      name: { value: "", isValid: true, message: "" },
      description: { value: "", isValid: true, message: "" },
      translations: [],
      count: 0,
      isInvalidForm: true,
    });
  };

  onChangeInputName = (e) => {
    this.setState({
      [e.target.name]: {
        ...this.state[e.target.name],
        value: e.target.value,
      },
    });
  };

  onChangeInputDescription = (e) => {
    this.setState({
      [e.target.name]: {
        ...this.state[e.target.name],
        value: e.target.value,
      },
    });
  };

  onChangeInput = (e) => {
    let { translations } = this.state;
    translations.map((transition) => {
      if (transition.name === e.target.name) {
        transition.value = e.target.value;
      }
    });
    this.setState({
      translations: translations,
    });
  };

  create = (e) => {
    e.preventDefault();
    let { translations } = this.state;
    let translationParams = [];
    translations.map((translation) => {
      let value = { name: "" };
      value.name = translation.value;
      translationParams.push(value);
    });

    const params = {
      name: this.state.name.value,
      description: this.state.description.value,
      vietnamesePhrases: translationParams,
    };

    this.props.create(params, this.props.history).then(() => {
      this.setState({
        name: { value: "", isValid: true, message: "" },
        description: { value: "", isValid: true, message: "" },
        transitions: [],
      })
    });
  };

  addTranslation = () => {
    let { count } = this.state;
    let translationName = "tranlation" + count;
    let translation = {
      name: translationName,
      value: "",
      isValid: true,
      message: "",
    };

    let translations = this.state.translations;

    translations.push(translation);
    count += 1;
    this.setState(
      {
        translations: translations,
        count: count,
      },
      () => { }
    );
  };

  deleteTranslation = (translationName) => {
    let { count } = this.state;
    let translations = this.state.translations;
    let translationsCopy = [];
    translations.map((translation, index) => {
      if (translation.name === translationName) {
        translations.splice(index, 1);
        count--;
      }
    });
    translations.map((translation, index) => {
      let name = "translation" + index;
      let translationCopy = {
        name: name,
        value: translation.value,
        isValid: true,
        message: "",
      };
      translationsCopy.push(translationCopy);
    });

    this.setState({
      translations: translationsCopy,
      count: count,
    });
  };
  render() {
    const { name,description } = this.state;
    const nameGroup = classnames("form-control border-primary", {
      "is-invalid": name.isInvalid,
    });

    return (
      <React.Fragment>
        <Header />
        <MenuSideBar />
        {/* Panel search */}

        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />
          <div className="content-wrapper">
            <div className="content-header row">
              <ToastContainer
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />
              <div className="content-header-left col-md-6 col-12 mb-2">
                <h3 className="content-header-title mb-0">Phrase</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item">
                        <Link to="/phrase">Phrase</Link>
                      </li>
                      <li className="breadcrumb-item active">Create Phrase</li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
            <div className="content-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header">
                      <h4
                        className="card-title"
                        id="horz-layout-colored-controls"
                      >
                        Create Phrase
                      </h4>
                      <a className="heading-elements-toggle">
                        <i className="fa fa-ellipsis-v font-medium-3" />
                      </a>
                      <div className="heading-elements">
                        <ul className="list-inline mb-0">
                          <li>
                            <a data-action="collapse">
                              <i className="feather icon-minus" />
                            </a>
                          </li>
                          <li>
                            <a data-action="reload" onClick={this.reload}>
                              <i className="feather icon-rotate-cw" />
                            </a>
                          </li>
                          <li>
                            <a data-action="expand">
                              <i className="feather icon-maximize" />
                            </a>
                          </li>
                          <li>
                            <a data-action="close">
                              <i className="feather icon-x" />
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="card-content collpase show">
                      <div className="card-body">
                        <form
                          className="form form-horizontal"
                          onSubmit={this.create}
                        >
                          <div className="form-body">
                            <div className="row">
                              <div className="col-md-12">
                                <div className="form-group row">
                                  <label className="col-md-3 control-label">
                                    Phrase
                                    <span style={{ color: "red" }}>
                                      &nbsp; *
                                    </span>
                                  </label>
                                  <div className="col-md-5">
                                    <input
                                      type="text"
                                      id="name"
                                      className={nameGroup}
                                      placeholder="Phrase"
                                      name="name"
                                      value={name.value}
                                      onChange={(e) =>
                                        this.onChangeInputName(e)
                                      }
                                      maxLength="255"
                                    />
                                    {this.state.isInvalidForm &&
                                      name.isInvalid && (
                                        <div className="help-block">
                                          <ul role="alert">
                                            <li>{name.message}</li>
                                          </ul>
                                        </div>
                                      )}
                                  </div>
                                  <div className="col-md-1">
                                    <button
                                      className="btn fonticon-container"
                                      type="button"
                                      onClick={() => this.addTranslation()}
                                    >
                                      <i className="fa fa-plus"></i>
                                    </button>
                                  </div>
                                </div>
                              </div>
                              <div className="col-md-12">
                                <div className="form-group row">
                                  <label className="col-md-3 control-label">
                                    Description
                                  </label>
                                  <div className="col-md-5">
                                    <textarea
                                      id="description"
                                      rows="4"
                                      className="form-control border-primary"
                                      name="description"
                                      placeholder="Description"
                                      value={description.value}
                                      onChange={(e) => this.onChangeInputDescription(e)}
                                      maxLength="1000"
                                    ></textarea>
                                  </div>
                                </div>
                              </div>
                              {this.state.translations.map((value, index) => {
                                return (
                                  <div className="col-md-12" key={index}>
                                    <div className="form-group row">
                                      <label className="col-md-3 control-label">
                                        Translation {index + 1}
                                      </label>
                                      <div className="col-md-5">
                                        <input
                                          type="text"
                                          id="translation"
                                          className="form-control border-primary"
                                          placeholder="Translation"
                                          name={value.name}
                                          value={value.value}
                                          onChange={this.onChangeInput}
                                          maxLength="255"
                                        />
                                      </div>
                                      <div className="col-md-1">
                                        <button
                                          className="btn fonticon-container"
                                          type="button"
                                          name={index}
                                          onClick={() =>
                                            this.deleteTranslation(value.name)
                                          }
                                        >
                                          <i
                                            className="feather icon-x"
                                            name={index}
                                          ></i>
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                );
                              })}
                            </div>
                          </div>
                          <div className="form-actions center">
                            <button
                              type="submit"
                              className="btn btn-primary mr-1"
                            >
                              <i className="fa fa-check-square-o" /> Save
                            </button>
                            <Link
                              type="button"
                              className="btn btn-warning mr-1"
                              to="/phrase"
                            >
                              <i className="feather icon-x" /> Cancel
                            </Link>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {
  create,
})(PhraseCreate);

import React, { Component } from "react";
import "../../../style.css";
import { connect } from "react-redux";
import {
  getPagination,
  deleteById,
  deleteList,
} from "../../../actions/PhraseAction";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { TextField, Checkbox } from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";
import { Link } from "react-router-dom";
import { trackPromise } from "react-promise-tracker";
import Header from "../../layout/Header";
import { Modal } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";
import Highlighter from "react-highlight-words";

class Phrase extends Component {
  state = {
    englishPhrase: "",
    vietnamesePhrase: "",
    page: 1,
    size: 10,
    sortBy: "id",
    sort: "DESC",
    sorted: false,
    phrases: [],
    totalElements: 0,
    totalPages: 0,
    idDelete: "",
    listIdDelete: [],
    showModal: false,
    showModalMany: false,
    checkAll: false,
  };

  componentDidMount() {
    trackPromise(this.props.getPagination(this.state));
  }

  componentWillReceiveProps(nextProps) {
    if (
      JSON.stringify(nextProps.phrases) !== JSON.stringify(this.props.phrases)
    ) {
      const phrases = nextProps.phrases;
     
      if (phrases.length > 0) {
        phrases.map((phrase) => {
          phrase.isChecked = false;
        });
      }
      this.setState({
        page: nextProps.page + 1,
        phrases: phrases,
        totalElements: nextProps.totalElements,
        totalPages: nextProps.totalPages,
      });
      return this.state;
    }
  }

  search = () => {
    trackPromise(this.props.getPagination(this.state));
  };

  onChangeSizePage = (e) => {
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };

  onChangePage = (e, page) => {
    this.setState(
      {
        page: page,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };

  onChangeCheckboxAll = () => {
    this.state.checkAll = !this.state.checkAll;
    const listIdDelete = [];
    this.state.phrases.map((value) => {
      value.isChecked = this.state.checkAll;
      if (this.state.checkAll) {
        listIdDelete.push(value.id);
      }
    });

    //  SET STATUS CHECK ALL
    this.setState({
      checkAll: this.state.checkAll,
      listIdDelete: listIdDelete,
    });
  };

  onChangeCheckbox = (value) => {
    value.isChecked = !value.isChecked;
    let { phrases } = this.state;
    let countChecked = 0;
    const listIdDelete = [];
    phrases.map((phrase) => {
      if (phrase.name === value.name) {
        phrase.isChecked = value.isChecked;
      }

      if (phrase.isChecked === true) {
        countChecked++;
        listIdDelete.push(phrase.id);
      }
    });
    if (countChecked === phrases.length) {
      this.setState({
        checkAll: true,
      });
    } else {
      this.setState({
        checkAll: false,
      });
    }
    this.setState({
      phrases: phrases,
      listIdDelete: listIdDelete,
    });
  };

  reloadPage = () => {
    this.setState(
      {
        englishPhrase: "",
		vietnamesePhrase: "",
		page: 1,
		size: 10,
		sortBy: "id",
		sort: "DESC",
		sorted: false,
		phrases: [],
		totalElements: 0,
		totalPages: 0,
		idDelete: "",
		listIdDelete: [],
		showModal: false,
		showModalMany: false,
		checkAll: false,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };

  deleteModal = (id) => {
    this.setState({
      showModal: true,
      idDelete: id,
    });
  };

  handleCloseModal = () => this.setState({ showModal: false });

  deletePhrase = () => {
    const id = this.state.idDelete;
    this.props.deleteById(id).then(() => {
      this.handleCloseModal();
      trackPromise(this.props.getPagination(this.state)).then(() => { });
    });
  };

  deleteManyModal = () => {
    if (this.state.listIdDelete.length > 0) {
      this.setState({
        showModalMany: true,
      });
    } else {
      toast.warning("Please Select One Phrase");
    }
  };

  handleCloseModalMany = () => this.setState({ showModalMany: false });

  deleteManyPhrase = () => {
    const listIdDelete = this.state.listIdDelete;
    this.props.deleteList(listIdDelete).then(() => {
      this.handleCloseModalMany();
      trackPromise(this.props.getPagination(this.state));
    });
  };

  sortBy = (fieldName) => {
    let sorted = this.state.sorted;
    sorted = !sorted;
    if (this.state.sorted) {
      this.setState({
        sort: "ASC",
      });
    } else {
      this.setState({
        sort: "DESC",
      });
    }
    this.setState(
      {
        sortBy: fieldName,
        sorted: sorted,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };

  onChangeInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  searchEnter = (e) => {
    if (e.key === "Enter") {
      trackPromise(this.props.getPagination(this.state));
    }
  };
  render() {
    return (
      <React.Fragment>
        <ToastContainer
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <Modal
          name="delete"
          show={this.state.showModal}
          animation={false}
          onHide={this.handleCloseModal}
        >
          <Modal.Header closeButton className="bg-warning white">
            <Modal.Title>MODAL DELETE</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete this phrase?</Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={this.deletePhrase}
            >
              OK
            </button>
            <button
              type="button"
              className="btn grey btn-outline-secondary "
              onClick={this.handleCloseModal}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>

        <Modal
          name="deleteAll"
          show={this.state.showModalMany}
          animation={false}
          onHide={this.handleCloseModalMany}
        >
          <Modal.Header closeButton className="bg-warning white">
            <Modal.Title>MODAL DELETE</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete these phrase?</Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={this.deleteManyPhrase}
            >
              OK
            </button>
            <button
              type="button"
              className="btn grey btn-outline-secondary "
              onClick={this.handleCloseModalMany}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>

        <Header />
        <MenuSideBar />
        {/* Panel search */}

        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />

          <div className="content-wrapper">
            <div className="content-header row">
              <div className="content-header-left col-md-6 col-12 mb-2">
                <h3 className="content-header-title mb-0">Phrase</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item active">Phrase</li>
                    </ol>
                  </div>
                </div>
              </div>
              <div className="content-header-right text-md-right col-md-6 col-12">
                <div className="form-group">
                  <Link
                    className="btn-icon btn fonticon-container"
                    type="button"
                    to="/phrase/create"
                  >
                    <i className="fa fa-plus"></i>
                    <label className="fonticon-classname">Add Phrase</label>
                  </Link>
                  <button
                    className="btn-icon btn fonticon-container"
                    type="button"
                    onClick={() => this.deleteManyModal()}
                  >
                    <i className="fa fa-times"></i>
                    <label className="fonticon-classname">Delete</label>
                  </button>
                  <button
                    className="btn-icon btn fonticon-container"
                    type="button"
                    onClick={this.reloadPage}
                  >
                    <i className="fa fa-refresh"></i>
                    <label className="fonticon-classname">Refresh</label>
                  </button>
                </div>
              </div>
            </div>
            <div className="content-body">
              {/* Basic Elements start */}
              <section className="basic-elements">
                <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      <div className="card-content">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <TextField
                                id="sentenceId"
                                label="English Phrase"
                                className="form-control"
                                color="secondary"
                                value={this.state.englishPhrase}
                                name="englishPhrase"
                                onChange={(e) => this.onChangeInput(e)}
                                onKeyDown={this.searchEnter}
                              />
                            </div>

                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <TextField
                                id="sentenceId"
                                label="Vietnamese Phrase"
                                className="form-control"
                                color="secondary"
                                value={this.state.vietnamesePhrase}
                                name="vietnamesePhrase"
                                onChange={(e) => this.onChangeInput(e)}
                                onKeyDown={this.searchEnter}
                              />
                            </div>

                            <div className="col-xl-1 col-lg-1 col-md-1 mb-1">
                              <a
                                className="btn btn-social-icon btn-outline-twitter btn-outline-cyan"
                                onClick={this.search}
                              >
                                <span className="fa fa-search"></span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              {/* Basic Inputs end */}

              <section id="decimal">
                <div className="row">
                  <div className="col-12">
                    <div className="card">
                      <div className="card-header">
                        <a href="/#" className="heading-elements-toggle">
                          <i className="fa fa-ellipsis-v font-medium-3" />
                        </a>
                        <div className="heading-elements">
                          <ul className="list-inline mb-0">
                            <li>
                              <a data-action="collapse">
                                <i className="feather icon-minus" />
                              </a>
                            </li>
                            <li>
                              <a data-action="reload" onClick={this.reloadPage}>
                                <i className="feather icon-rotate-cw" />
                              </a>
                            </li>
                            <li>
                              <a data-action="expand">
                                <i className="feather icon-maximize" />
                              </a>
                            </li>
                            <li>
                              <a data-action="close">
                                <i className="feather icon-x" />
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="card-content collapse show">
                        <div className="card-body card-dashboard">
                          <table className="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th style={{ width: "5%" }}>
                                  <Checkbox
                                    checked={this.state.checkAll}
                                    onChange={this.onChangeCheckboxAll}
                                    name="check"
                                    color="primary"
                                    value="checkAll"
                                  />
                                </th>
                                <th style={{ width: "5%" }}>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("id")}
                                  >
                                    ID
                                  </a>
                                </th>
                                <th style={{width: "20%"}}>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("sentence")}
                                  >
                                    English Phrase
                                  </a>
                                </th>
                                <th style={{width: "25%"}}>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("translation")}
                                  >
                                    Vietnamese Phrase
                                  </a>
                                </th>
                                <th style={{width: "35%"}}>
                                    Description
                                </th>
                                <th style={{ width: "15%" }}>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.phrases.map((value, index) => (
                                <tr key={index}>
                                  <td>
                                    <Checkbox
                                      checked={value.isChecked}
                                      color="primary"
                                      onChange={() =>
                                        this.onChangeCheckbox(value)
                                      }
                                    />
                                  </td>
                                  <td className="styleTD">
                                    {index +
                                      (this.state.page - 1) * this.state.size +
                                      1}
                                  </td>
                                  <td className="styleTD"><p style={{fontSize:18}}>{value.name}</p></td>
                                  <td className="styleTD">
                                    {value.vietnamesePhrases.map(
                                      (vietnamesePhrase, index) => {
                                        if (
                                          index ===
                                          value.vietnamesePhrases.length - 1
                                        ) {
                                          return <div><div style={{marginBottom: 20}}><p style={{fontSize:18}}>{vietnamesePhrase.name}</p></div></div>;
                                        } else {
                                          return <div><div style={{marginBottom: 20}}><p style={{fontSize:18}}>{vietnamesePhrase.name}</p></div><hr/></div>;
                                        }
                                      }
                                    )}
                                  </td>
                                  <td style={{fontSize:17,color:"red"}}>{value.description}</td>
                                  <td>
                                    <Link
                                      className="btn-icon btn primary editButton"
                                      type="button"
                                      to={`/phrase/update/${value.id}`}
                                    >
                                      <i className="fa fa-pencil-square"></i>
                                    </Link>
                                    <button
                                      className="btn-icon btn danger deleteButton"
                                      type="button"
                                      onClick={() => this.deleteModal(value.id)}
                                    >
                                      <i className="fa fa-trash"></i>
                                    </button>
                                  </td>
                                </tr>
                              ))}
                            </tbody>
                          </table>

                          <div className="row">
                            <div className="offset-5 col-2 text-right">
                              <label className="control-label labelPagination">
                                Show
                              </label>
                            </div>
                            <div className="col-1">
                              <select
                                name="size"
                                className="custom-select custom-select-sm form-control form-control-sm"
                                value={this.state.size}
                                onChange={this.onChangeSizePage}
                              >
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                              </select>
                            </div>
                            <div className="col-4">
                              <Pagination
                                name="page"
                                count={this.state.totalPages}
                                color="primary"
                                showFirstButton
                                showLastButton
                                onChange={this.onChangePage}
                                page={this.state.page}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
        {/* Modal */}

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  phrases: state.phrases.phrasesPagination,
  totalPages: state.phrases.totalPages,
  totalElements: state.phrases.totalElements,
  page: state.phrases.page,
});

export default connect(mapStateToProps, {
  getPagination,
  deleteById,
  deleteList,
})(Phrase);

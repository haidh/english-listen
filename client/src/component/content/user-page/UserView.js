import React, { Component } from "react";
import "../../../style.css";
import Header from "../../layout/Header";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { Link } from "react-router-dom";
import CheckboxTree from "react-checkbox-tree";
import "react-checkbox-tree/lib/react-checkbox-tree.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { updateUser, getUserDetails } from "../../../actions/UserAction";
import { getAllRole } from "../../../actions/RoleAction";
import { trackPromise } from "react-promise-tracker";
import { ToastContainer } from "react-toastify";
import classnames from "classnames";
import { validateForm } from "../../validate/ValidateForm";

class UserView extends Component {
  state = {
    username: { value: "", isValid: true, message: "" },
    email: { value: "", isValid: true, message: "" },
    fullName: { value: "", isValid: true, message: "" },
    phone: { value: "", isValid: true, message: "" },
    userDetail: {},
    roleTree: [],
    rolesCheckbox: [],
    checkedRole: [],
    expandedRole: [],
    rolesDetail: {},
    roles: [],
    isInvalidForm: false,
  };

  componentDidMount = () => {
    const id = this.props.match.params.id;
    //  Get User Detail & Get All Role
    trackPromise(
      this.props.getUserDetails(id).then(() => {
        trackPromise(this.props.getAllRole(this.state));
      })
    );
  };

  componentWillReceiveProps = (nextProps) => {
    const id = this.props.match.params.id;
    //  Custom role => role Checkbox
    this.getRoleCheckbox(nextProps);
    //  Check userDetail Not Null & role Update - NOT create
    console.log(nextProps.userDetail);
    if (Object.keys(nextProps.userDetail).length !== 0 && id) {
      const roles = nextProps.userDetail.roles;
      const checkedRole = [];
      roles.map((role) => {
        checkedRole.push(role.name);
      });

      //  Set role Name , Description & Operation Checked

      this.setState({
        username: { value: nextProps.userDetail.username },
        fullName: { value: nextProps.userDetail.fullName },
        email: { value: nextProps.userDetail.email },
        phone: { value: nextProps.userDetail.phone },
        checkedRole: checkedRole,
        roles: roles,
        roleTree: nextProps.roleTree,
      });
    }
  };

  getRoleCheckbox = (nextProps) => {
    const roleTree = nextProps.roleTree;
    const nodes = [];
    roleTree.map((role) => {
      const parentNode = {
        label: "",
        value: "",
      };
      parentNode.label = role.name;
      parentNode.value = role.name;
      nodes.push(parentNode);
    });
    this.setState({
      rolesCheckbox: nodes,
      roleTree: nextProps.roleTree,
    });
  };

  checkedRoles = (checkedRole) => {
    this.setState({
      checkedRole: checkedRole,
    });

    const roles = [];
    this.state.roleTree.map((role) => {
      checkedRole.map((checked) => {
        if (role.name == checked) {
          roles.push(role);
        }
      });
    });

    this.setState({
      roles: roles,
    });
  };

  reload = () => {
    this.setState({
      username: { value: "", isValid: true, message: "" },
      email: { value: "", isValid: true, message: "" },
      fullName: { value: "", isValid: true, message: "" },
      phone: { value: "", isValid: true, message: "" },
      userDetail: {},
      roleTree: [],
      rolesCheckbox: [],
      checkedRole: [],
      expandedRole: [],
      rolesDetail: {},
      roles: [],
      isInvalidForm: false,
    });
  };

  onChangeInput = (e) => {
    const fieldName = e.target.name;
    this.setState(
      {
        [e.target.name]: {
          ...this.state[e.target.name],
          value: e.target.value,
        },
      },
      () => {
        const field = this.state[fieldName];
        const fieldValidate = validateForm(fieldName, field);
        if (fieldValidate.isInvalid) {
          this.setState({
            [fieldName]: {
              ...this.state[fieldName],
              isInvalid: fieldValidate.isInvalid,
              message: fieldValidate.message,
            },
            isInvalidForm: true,
          });
        } else {
          this.setState({
            [fieldName]: {
              ...this.state[fieldName],
              isInvalid: false,
              message: "",
            },
            isInvalidForm: false,
          });
        }
      }
    );
  };

  update = (e) => {
    e.preventDefault();
    const { username, fullName, email, phone } = this.state;
    const id = this.props.match.params.id;

    const updateUser = {
      id: id,
      username: username.value,
      fullName: fullName.value,
      email: email.value,
      phone: phone.value,
      roles: this.state.roles,
    };
    this.props.updateUser(updateUser, this.props.history);
  };

  render() {
    console.log(this.state);
    const { username, email, fullName, phone } = this.state;
    const fullNameGroup = classnames("form-control border-primary", {
      "is-invalid": fullName.isInvalid,
    });
    const phoneGroup = classnames("form-control border-primary", {
      "is-invalid": phone.isInvalid,
    });

    return (
      <React.Fragment>
        <Header />
        <MenuSideBar />
        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />
          <div className="content-wrapper">
            <div className="content-header row">
              <ToastContainer
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />
              <div className="content-header-left col-md-6 col-12 mb-2">
                <h3 className="content-header-title mb-0">User</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item">
                        <Link to="/permission">User</Link>
                      </li>
                      <li className="breadcrumb-item active">Update User</li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
            <div className="content-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header">
                      <h4
                        className="card-title"
                        id="horz-layout-colored-controls"
                      >
                        Update User
                      </h4>
                      <a className="heading-elements-toggle">
                        <i className="fa fa-ellipsis-v font-medium-3" />
                      </a>
                      <div className="heading-elements">
                        <ul className="list-inline mb-0">
                          <li>
                            <a data-action="collapse">
                              <i className="feather icon-minus" />
                            </a>
                          </li>
                          <li>
                            <a data-action="reload">
                              <i className="feather icon-rotate-cw" />
                            </a>
                          </li>
                          <li>
                            <a data-action="expand">
                              <i className="feather icon-maximize" />
                            </a>
                          </li>
                          <li>
                            <a data-action="close">
                              <i className="feather icon-x" />
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="card-content collpase show">
                      <div className="card-body">
                        <form
                          className="form form-horizontal"
                          onSubmit={this.update}
                        >
                          <div className="form-body">
                            <div className="row">
                              <div className="col-md-6">
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Username
                                    <span style={{ color: "red" }}>
                                      &nbsp; *
                                    </span>
                                  </label>
                                  <div className="col-md-8 ">
                                    <input
                                      type="text"
                                      id="usernameId"
                                      className="form-control border-primary"
                                      placeholder="User Name"
                                      name="username"
                                      value={username.value}
                                      onChange={(e) => this.onChangeInput(e)}
                                      maxLength="255"
                                      disabled
                                    />
                                  </div>
                                </div>
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Email
                                  </label>
                                  <div className="col-md-8">
                                    <input
                                      type="text"
                                      id="emailId"
                                      className="form-control border-primary"
                                      placeholder="Email"
                                      name="email"
                                      value={email.value}
                                      onChange={(e) => this.onChangeInput(e)}
                                      maxLength="255"
                                      disabled
                                    />
                                  </div>
                                </div>
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Full Name
                                    <span style={{ color: "red" }}>
                                      &nbsp; *
                                    </span>
                                  </label>
                                  <div className="col-md-8 ">
                                    <input
                                      type="text"
                                      id="fullNameId"
                                      className={fullNameGroup}
                                      placeholder="Full Name"
                                      name="fullName"
                                      value={fullName.value}
                                      onChange={(e) => this.onChangeInput(e)}
                                      maxLength="255"
                                      disabled
                                    />
                                    {this.state.isInvalidForm &&
                                      fullName.isInvalid && (
                                        <div className="help-block">
                                          <ul role="alert">
                                            <li>{fullName.message}</li>
                                          </ul>
                                        </div>
                                      )}
                                  </div>
                                </div>

                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Phone
                                    <span style={{ color: "red" }}>
                                      &nbsp; *
                                    </span>
                                  </label>
                                  <div className="col-md-8 ">
                                    <input
                                      type="number"
                                      id="phoneId"
                                      className={phoneGroup}
                                      placeholder="Phone"
                                      name="phone"
                                      value={phone.value}
                                      onChange={(e) => this.onChangeInput(e)}
                                      maxLength="255"
                                      disabled
                                    />
                                    {this.state.isInvalidForm &&
                                      phone.isInvalid && (
                                        <div className="help-block">
                                          <ul role="alert">
                                            <li>{phone.message}</li>
                                          </ul>
                                        </div>
                                      )}
                                  </div>
                                </div>
                              </div>
                              <div className="col-md-6">
                                <CheckboxTree
                                  nodes={this.state.rolesCheckbox}
                                  checked={this.state.checkedRole}
                                  expanded={this.state.expandedRole}
                                  noCascade
                                  onCheck={(checkedRole) =>
                                    this.checkedRoles(checkedRole)
                                  }
                                  disabled
                                />
                              </div>
                            </div>
                          </div>
                          <div className="form-actions center">
                            <Link
                              type="button"
                              className="btn btn-warning mr-1"
                              to="/user"
                            >
                              <i className="feather icon-x" /> Cancel
                            </Link>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  roleTree: state.roles.allRole,
  userDetail: state.user.userDetail,
});

UserView.propTypes = {
  roleTree: PropTypes.array.isRequired,
  userDetail: PropTypes.object.isRequired,
  updateUser: PropTypes.func.isRequired,
  getUserDetails: PropTypes.func.isRequired,
  getAllRole: PropTypes.func.isRequired,
};
export default connect(mapStateToProps, {
  updateUser,
  getUserDetails,
  getAllRole,
})(UserView);

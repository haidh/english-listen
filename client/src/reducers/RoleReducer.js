import {
  GET_ROLE,
  GET_ROLE_DETAIL,
  DELETE_ROLE,
  DELETE_LIST_ROLE,
  GET_ALL_ROLE,
} from "../action_types/types";

const initialState = {
  roles: [],
  allRole: [],
  totalPages: 0,
  totalElements: 0,
  page: 0,
  roleDetail: {},
  deleteSuccess: "",
  deleteListSuccess: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ROLE:
      return {
        ...state,
        roles: action.payload.roles,
        totalPages: action.payload.totalPages,
        totalElements: action.payload.totalElements,
        page: action.payload.page,
      };
    case GET_ALL_ROLE:
      return {
        ...state,
        allRole: action.payload,
      };
    case GET_ROLE_DETAIL:
      return {
        ...state,
        roleDetail: action.payload,
      };
    case DELETE_ROLE:
      return {
        ...state,
        deleteSuccess: action.payload,
      };
    case DELETE_LIST_ROLE:
      return {
        ...state,
        deleteSuccess: action.payload,
      };
    default:
      return state;
  }
}

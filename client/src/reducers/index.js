import { combineReducers } from "redux";
import UserReducer from "./UserReducer";
import ErrorReducer from "./ErrorReducer";
import PermissionReducer from "./PermissionReducer";
import OperationReducer from "./OperationReducer";
import RoleReducer from "./RoleReducer";
import MovieReducer from "./MovieReducer";
import SeasonReducer from "./SeasonReducer";
import EpisodeReducer from "./EpisodeReducer";
import SentenceReducer from "./SentenceReducer";
import PhraseReducer from "./PhraseReducer";
import ITSentenceReducer from "./ITSentenceReducer";
import GenreReducer from "./GenreReducer";

export default combineReducers({
  user: UserReducer,
  errors: ErrorReducer,
  permissions: PermissionReducer,
  operations: OperationReducer,
  roles: RoleReducer,
  movies: MovieReducer,
  seasons: SeasonReducer,
  episodes: EpisodeReducer,
  sentences: SentenceReducer,
  phrases: PhraseReducer,
  itSentences: ITSentenceReducer,
  genres: GenreReducer,
});

import { GET_SEASON_ALL, GET_SEASON_BY_MOVIE } from "../action_types/types";

const initialState = {
  seasons: [],
  seasonByMovie: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_SEASON_ALL:
      return {
        ...state,
        seasons: action.payload,
      };
    case GET_SEASON_BY_MOVIE:
      return {
        ...state,
        seasonByMovie: action.payload,
      };
    default:
      return state;
  }
}

import {
  GET_IT_SENTENCE_PAGINATION,
  GET_IT_SENTENCE_DETAIL,
} from "../action_types/types";

const initialState = {
  itSentences: [],
  itSentencesPagination: [],
  itSentenceDetail: {},
  totalPages: 0,
  totalElements: 0,
  page: 0,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_IT_SENTENCE_PAGINATION:
      return {
        ...state,
        itSentencesPagination: action.payload.itSentences,
        totalPages: action.payload.totalPages,
        totalElements: action.payload.totalElements,
        page: action.payload.page,
      };
    case GET_IT_SENTENCE_DETAIL:
      return {
        ...state,
        itSentenceDetail: action.payload,
      };
    // case GET_PHRASES:
    //   return {
    //     ...state,
    //     phrases: action.payload,
    //   };
    default:
      return state;
  }
}

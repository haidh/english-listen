import {
  GET_OPERATION,
  GET_OPERATION_BY_CONDITION,
} from "../action_types/types";

const initialState = {
  operation: [],
  operationByCondition: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_OPERATION:
      return {
        ...state,
        operation: action.payload,
      };
    case GET_OPERATION_BY_CONDITION:
      return {
        ...state,
        operationByCondition: action.payload,
      };
    default:
      return state;
  }
}

import { GET_SENTENCE_PAGINATION, GET_SENTENCE_DETAIL } from "../action_types/types";

const initialState = {
  sentences: [],
  sentenceDetail: {},
  totalPages: 0,
  totalElements: 0,
  page: 0,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_SENTENCE_PAGINATION:
      return {
        ...state,
        sentences: action.payload.sentences,
        totalPages: action.payload.totalPages,
        totalElements: action.payload.totalElements,
        page: action.payload.page,
      };
    case GET_SENTENCE_DETAIL:
      return {
        ...state,
        sentenceDetail: action.payload,
      };
    default:
      return state;
  }
}

import {
  GET_ALL_GENRE,
  GET_GENRE_DETAIL
} from "../action_types/types";

const initialState = {
  genres: [],
  genreDetail: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ALL_GENRE:
      return {
        ...state,
        genres: action.payload,
      };
    case GET_GENRE_DETAIL:
      return {
        ...state,
        genreDetail: action.payload,
      };
    default:
      return state;
  }
}

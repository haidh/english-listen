import { GET_PHRASES, GET_PHRASE_PAGINATION, GET_PHRASE_DETAIL } from "../action_types/types";

const initialState = {
  phrases: [],
  phrasesPagination: [],
  phraseDetail: {},
  totalPages: 0,
  totalElements: 0,
  page: 0,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_PHRASE_PAGINATION:
      return {
        ...state,
        phrasesPagination: action.payload.phrases,
        totalPages: action.payload.totalPages,
        totalElements: action.payload.totalElements,
        page: action.payload.page,
      };
    case GET_PHRASE_DETAIL:
      return {
        ...state,
        phraseDetail: action.payload,
      };
    case GET_PHRASES:
      return {
        ...state,
        phrases: action.payload,
      };
    default:
      return state;
  }
}

import {
  REGISTER_USER,
  LOGIN_USER,
  GET_USER_PAGINATION,
  GET_NOTIFICATION,
  GET_USER_DETAIL,
} from "../action_types/types";

const initialState = {
  data: {},
  validToken: false,
  users: [],
  totalPages: 0,
  totalElements: 0,
  page: 0,
  userDetail: {},
  deleteSuccess: "",
  deleteListSuccess: "",
  notification: "",
};

const booleanActionPayload = (payload) => {
  if (payload) {
    return true;
  } else {
    return false;
  }
};
export default function (state = initialState, action) {
  switch (action.type) {
    case REGISTER_USER:
      return action.payload;
    case LOGIN_USER:
      return {
        ...state,
        validToken: booleanActionPayload(action.payload),
        data: action.payload,
      };
    case GET_NOTIFICATION:
      return { ...state, notification: action.payload };
    case GET_USER_PAGINATION:
      return {
        ...state,
        users: action.payload.users,
        totalPages: action.payload.totalPages,
        totalElements: action.payload.totalElements,
        page: action.payload.page,
      };
    case GET_USER_DETAIL:
      return {
        ...state,
        userDetail: action.payload,
      };
    default:
      return state;
  }
}

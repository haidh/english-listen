import {
  GET_EPISODE_ALL,
  GET_EPISODE_BY_MOVIE_SEASON,
} from "../action_types/types";

const initialState = {
  episodes: [],
  episodeByMovieSeason: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_EPISODE_ALL:
      return {
        ...state,
        episodes: action.payload,
      };
    case GET_EPISODE_BY_MOVIE_SEASON:
      return {
        ...state,
        episodeByMovieSeason: action.payload,
      };
    default:
      return state;
  }
}
